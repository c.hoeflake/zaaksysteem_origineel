package Zaaksysteem::Test::Betrokkene::Object::NatuurlijkPersoon;
use Moose;
extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;
use Zaaksysteem::Betrokkene::Object::NatuurlijkPersoon;
use BTTW::Tools::RandomData qw(:uuid);

sub test_np_create {

    my $model = Zaaksysteem::Betrokkene::Object::NatuurlijkPersoon->new();

    my %args = (
        'np-authenticated'   => 0,
        'np-authenticatedby' => 'twofactor',
        'np-voornamen'       => 'Test',
        'np-geslachtsnaam'   => 'Burgerman',
        'np-huisnummer'      => 90,
        'np-landcode'        => '6030',
        'np-postcode'        => '1114AD',
        'np-straatnaam'      => 'H.J.E Wenckebachweg',
        'np-woonplaats'      => 'Amsterdam',
        'npc-email'          => 'test@example.com',
        'npc-mobiel'         => '+31612345678',
        'npc-telefoonnummer' => '+31205355255',
    );


    my %result;
    my $override = override(
        'Zaaksysteem::Betrokkene::Object::NatuurlijkPersoon::_create_extern'
            => sub {
            my ($self, $disptach_args, $unknown, $args) = @_;
            %result = %$args;
            return 42;
        }
    );

    my %want = (
        gm => {
            geslachtsnaam   => 'Burgerman',
            authenticated   => 0,
            voorletters     => '',
            authenticatedby => 'twofactor',
            voornamen       => 'Test',
        },
        adres => {
            straatnaam    => 'H.J.E Wenckebachweg',
            huisnummer    => 90,
            postcode      => '1114AD',
            woonplaats    => 'Amsterdam',
            landcode      => 6030,
            functie_adres => 'W',
        },
        gmc => {
            email          => 'test@example.com',
            mobiel         => '+31612345678',
            telefoonnummer => '+31205355255',
        },
    );

    $model->create(undef, \%args);
    cmp_deeply(\%result, \%want, "created np via _extern");

    my $uuid = generate_uuid_v4();
    $args{uuid}               = $uuid;
    $want{gm}{uuid}           = $uuid;
    $args{'np-authenticated'} = 1;
    $want{gm}{authenticated}  = 1;

    $model->create(undef, \%args);
    cmp_deeply(\%result, \%want, "created np via _extern with UUID $uuid");


}

1;


__END__

=head1 NAME

Zaaksysteem::Test::Betrokkene::Object::NatuurlijkPersoon - Testing betrokkene NP

=head1 DESCRIPTION

Test the untestable

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::Betrokkene::Object::NatuurlijkPersoon

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
