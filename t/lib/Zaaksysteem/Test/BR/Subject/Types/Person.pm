package Zaaksysteem::Test::BR::Subject::Types::Person;

use Zaaksysteem::Test;
use Mock::Quick;

=head1 NAME

Zaaksysteem::Test::Backend::Email - Test ZS::Backend::Email

=head1 DESCRIPTION

Test email backend code

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::BR::Subject::Types::Person

=head2 test_run_post_triggers

=cut

sub test_run_post_triggers {
    my $self    = shift;

    my @create;
    my @item;

    my $person = mock_moose_class({
        roles => [qw[Zaaksysteem::BR::Subject::Queue::Person]]
    });

    my $mock_result_source = mock_one(
        schema => {
            resultset => sub {
                ok shift eq 'Queue', 'expected table dereferenced';
                return mock_one(
                    create_item => sub {
                        @create = @_;
                        return 'item';
                    },
                    queue_item => sub {
                        @item = @_;
                    }
                );
            }
        }
    );

    my $mock_died = mock_one(
        id => 44,
        datum_overlijden => '2016-08-08',
        result_source => $mock_result_source
    );

    my $mock_alive = mock_one(
        id => 55,
        datum_overlijden => undef,
        result_source => $mock_result_source
    );

    $person->run_post_triggers($mock_alive);

    is_deeply \@create, [], 'run_post_triggers on alive person creates no queue item';
    is_deeply \@item, [], 'run_post_triggers on alive person adds no queue item to queue';

    $person->run_post_triggers($mock_died);

    is_deeply \@create, [
        'update_cases_of_deceased',
        {
            label => 'Update cases of deceased persons',
            metadata => {
                target => 'backend',
                disable_acl => 1,
            },
            data => {
                natuurlijk_persoon_id => 44,
            }
        },
    ], 'run_post_triggers on dead person creates queue item';

    is_deeply \@item, [ 'item' ], 'run_post_triggers on dead person adds queue item to queue';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
