package Zaaksysteem::Test::Tools::X509;

use Zaaksysteem::Test;
use File::Spec::Functions qw(catfile);
use IO::All;

use Zaaksysteem::Tools::X509 qw(
    split_chain
);

sub test_split_chain {

    my $first = q{-----BEGIN CERTIFICATE-----
this is a chain
yada
-----END CERTIFICATE-----};

    my $second = q{-----BEGIN CERTIFICATE-----
chainy
bro
-----END CERTIFICATE-----};

    my $third = q{-----BEGIN CERTIFICATE-----
dick chainy
nomoar chainy
-----END CERTIFICATE-----};

    my $filename = catfile(qw(t data x509 three-chains.txt));
    my @cert = split_chain($filename);
    cmp_deeply(\@cert, [$first, $second, $third], "Three chains found in $filename");

    {
        open(my $fh, '<', $filename);
        my @cert = split_chain($fh);
        cmp_deeply(
            \@cert,
            [$first, $second, $third],
            ".. and native filehandle also works"
        );
    }

    {
        my $fh   = _io_tmp($first, $second, $third);
        my @cert = split_chain($fh);
        cmp_deeply(
            \@cert,
            [$first, $second, $third],
            ".. and also IO::All"
        );
    }

    {
        my $fh = _io_tmp("-----BEGIN CERTIFICATE-----\r", "foo\r", "-----END CERTIFICATE-----\r");
        my @cert = split_chain($fh);
        cmp_deeply(
            \@cert,
            [q{-----BEGIN CERTIFICATE-----
foo
-----END CERTIFICATE-----}],
            "Windowsze",
        );
    }

    throws_ok(
        sub {
            my $fh = _io_tmp($first, "-----BEGIN CERTIFICATE-----\nfoo");
            split_chain($fh);

        },
        qr/No END found after 1 chains/,
        "Invalid chain found, only BEGIN"
    );

    throws_ok(
        sub {
            my $fh = _io_tmp("foo\n-----END CERTIFICATE-----\n");
            split_chain($fh);

        },
        qr/Invalid END certificate found/,
        "Invalid chain found, only END"
    );

    throws_ok(
        sub {
            my $fh = _io_tmp(
                "-----BEGIN CERTIFICATE-----\n",
                "foo\n",
                "-----BEGIN CERTIFICATE-----\n"
            );
            split_chain($fh);

        },
        qr/Invalid BEGIN certificate found/,
        "Invalid chain found, two BEGIN blocks"
    );
}

sub _io_tmp {
    my $fh = io('?');
    foreach (@_) {
        $fh->print($_);
        $fh->print("\n");
    }
    $fh->seek(0, 0);
    return $fh;
}
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
