package Zaaksysteem::Test::Zaken::Concept;

use Moose;
extends "Zaaksysteem::Test::Moose";

=head1 NAME

Zaaksysteem::Test::Zaken::Concept - Test Zaaksysteem::Zaken::Concept

=head1 DESCRIPTION

Test the concept case model

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Zaken::Concept

=cut

use Zaaksysteem::Test;
use BTTW::Tools::RandomData qw(:uuid);
use Data::Random::NL qw(:all);
use DateTime;
use JSON::XS;

use Zaaksysteem::Zaken::Concept;

sub test_pod {
    pod_coverage_ok('Zaaksysteem::Zaken::Concept');
}

sub _get_model {

    my $model = Zaaksysteem::Zaken::Concept->new(
        concept_rs => mock_one(),
        @_,
    );
    isa_ok($model, "Zaaksysteem::Zaken::Concept");
    return $model;

}

sub test_find_concept_case {

    my @args;
    my $rs = mock_one(
        search_rs => sub {
            @args = @_;
            return mock_one(),
        }
    );
    my $model   = _get_model(concept_rs => $rs);

    my $concept = $model->find_concept_case(1, "String");
    isnt($concept, undef, "Found concept case");
    cmp_deeply(\@args, [{zaaktype_id => 1, betrokkene => "String"}], "Search args");

}

sub test_get_concept_case_data {
    my $model = _get_model();

    my $override = override(
        'Zaaksysteem::Zaken::Concept::find_concept_case' => sub {
            return mock_one(json_string => '{"foo":"bar"}'),
    });

    my $rv = $model->get_concept_case_data(1, "String");
    cmp_deeply($rv, { foo => 'bar'} , "Found concept case data");
}

sub test_delete_concept_case {

    my @args;
    my $rs = mock_one(
        search_rs => sub {
            @args = @_;
            return mock_one(delete => sub { die "Called delete" }),
        }
    );
    my $model   = _get_model(concept_rs => $rs);
    throws_ok(
        sub {
            $model->delete_concept_case(1, "Two");
        },
        qr/Called delete/,
        "We called delete"
    );

    cmp_deeply(\@args, [{zaaktype_id => 1, betrokkene => "Two"}], "Delete args");
}

sub test_submit_concept_case {
    my @args;
    my $rs = mock_one(
        update_or_create => sub {
            @args = @_;
            return 1;
        }
    );
    my $model   = _get_model(concept_rs => $rs, json => JSON::XS->new->canonical(1));

    my $now = DateTime->now()->subtract(days => 2);
    my $override = override('DateTime::now' => sub { return $now });

    ok(
        $model->submit_concept_case(
            { zaaktype_id => 42, ztc_aanvrager_id => 'twee', my => 'data' }
        ),
        "Submitting concept case"
    );
    cmp_deeply(
        $args[0],
        {
            zaaktype_id     => 42,
            betrokkene      => 'twee',
            json_string     => '{"my":"data","zaaktype_id":42,"ztc_aanvrager_id":"twee"}',
            afronden        => 0,
            create_unixtime => $now->epoch,
        },
        "submit args"
    );
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
