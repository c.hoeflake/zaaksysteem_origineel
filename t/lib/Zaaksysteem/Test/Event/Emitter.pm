package Zaaksysteem::Test::Event::Emitter;
use Moose;
extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;
use Zaaksysteem::Event::Emitter;

my %event = (
    event_name => 'DocumentCreated',
);

sub test_event_plugins {


    my @plugins = Zaaksysteem::Event::Emitter->plugins();
    is(@plugins, 4, "Found two plugins");


    my $handler = Zaaksysteem::Event::Emitter->new_from_event(
        \%event,
        schema  => mock_strict(),
        subject => mock_strict(),
        case    => mock_strict(),
        queue   => mock_strict(),
    );

    isa_ok($handler, "Zaaksysteem::Event::DocumentCreated");
}




__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
