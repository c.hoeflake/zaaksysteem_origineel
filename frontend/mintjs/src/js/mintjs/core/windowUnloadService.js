/*global angular,_,$*/
(function ( ) {
	
	angular.module('MintJS.core')
		.factory('windowUnloadService', [ '$rootScope', '$window', 'translationService', function ( $rootScope, $window, translationService ) {
			
			var windowUnloadService = {},
				callbacks = [],
				safeApply = window.zsFetch('nl.mintlab.utils.safeApply');
			
			function attemptExit ( e ) {
				var i = 0,
					l = callbacks.length,
					msg;
				
				for(; i < l; ++i) {
					msg = callbacks[i](e);
					if(msg) {
						return msg;
					}
				}
				
				return undefined;
			}
			
			windowUnloadService.register = function ( callback ) {
				callbacks.push(callback);
			};
			
			windowUnloadService.unregister = function ( callback ) {
				_.pull(callbacks, callback);
			};
			
			windowUnloadService.UNSAVED_CHANGES = translationService.get('U heeft onopgeslagen wijzigingen. Deze gaan verloren als u wegnavigeert van deze pagina.');
			
			// Doesn't work, using jquery for now
			// $window.onbeforeunload = function ( e ) {
			// return attemptExit(e);
			// };
			
			$($window).bind('beforeunload', function ( e) {
				return safeApply($rootScope, function ( ) {
					return attemptExit(e);
				});
			});
			
			
			
			return windowUnloadService;
			
		}]);
	
})();
