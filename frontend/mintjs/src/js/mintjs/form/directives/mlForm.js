/*global angular,_*/
(function ( ) {
	
	angular.module('MintJS.form')
		.directive('mlForm', [ '$parse', '$http', '$interpolate', '$q', function ( $parse, $http, $interpolate, $q ) {
			
			return {
				scope: true,
				controller: [ '$scope', '$attrs', function ( $scope, $attrs ) {
					var ctrl = this,
						unwatchers = [],
						lastSaved = NaN,
						submitting,
						controls = {};
						
					ctrl.values = $scope.$new();
					
					function setPromises ( promises ) {
						_.each(unwatchers, function ( unwatcher ) {
							unwatcher();
						});
						
						unwatchers.length = 0;
						
						_.each(promises, function ( p ) {
							unwatchers.push(ctrl.values.$watch(p.watch, function ( /*nw, old*/ ) {
								if(!p.when || $parse(p.when)(ctrl.values)) {
									$parse(p.then)(ctrl.values);
								}
							}));
						});
					}
					
					function getFormControl ( ) {
						return $scope[ctrl.name];
					}
					
					function onValueChange ( ) {
						attemptSave();
					}
					
					function attemptSave ( ) {
						if(ctrl.options.autosave) {
							if(ctrl.options.autosave !== 'valid' || ctrl.isValid()) {
								ctrl.submit();
							}
						}
					}
					
					ctrl.setConfig = function ( config ) {
						ctrl.name = config.name;
						ctrl.options = config.options || {};
						
						ctrl.fieldsets = config.fieldsets || [];
						
						if(config.fields) {
							ctrl.fieldsets.unshift({
								name: 'all',
								fields: config.fields
							});
						}
						
						ctrl.actions = config.actions;
						setPromises(config.promises);
					};
					
					ctrl.isValid = function ( ) {
						return getFormControl().$valid;
					};
					
					ctrl.isChanged = function ( ) {
						return _.some(controls, function ( control ){
							return control.isChanged();
						});
					};
					
					ctrl.addControl = function ( control ) {
						controls[control.name] = control;
						control.commitListeners.push(onValueChange);
					};
					
					ctrl.removeControl = function ( control ) {
						_.pull(control.commitListeners, onValueChange);
						delete controls[control.name];
					};
					
					ctrl.getControl = function ( name ) {
						return controls[name];
					};
					
					ctrl.submit = function ( ) {
						
						var values = {},
							resolvedValues = {},
							url,
							deferred = $q.defer(),
							val,
							name;
						
						_.each(controls, function ( fieldCtrl ) {
							name = fieldCtrl.name;
							val = ctrl.values[name];
							values[name] = val;
							resolvedValues[name] = fieldCtrl.resolveValue(val);
						});
						
						url = ctrl.options.url ? $interpolate(ctrl.options.url) : null;
						
						submitting = true;
						
						$scope.$emit('zs.form.submit.attempt', values, resolvedValues);
						
						if(url) {
							$http({
								method: ctrl.options.method || 'POST',
								url: url
							})
								.success(function ( response ) {
									deferred.resolve(response);
									
									lastSaved = new Date().getTime();
									
									$scope.$emit('zs.form.submit.success', response);
								})
								.error(function ( response ) {
									deferred.reject(response);
									$scope.$emit('zs.form.submit.error', response);
								})
								['finally'](function ( ) {
									submitting = false;
								});
						} else {
							$scope.$emit('zs.form.submit.success');
							deferred.resolve();
						}
						
						return deferred.promise;
					};
					
					ctrl.reset = function ( ) {
						_.each(controls, function ( control ) {
							control.resetValue();
						});
					};
					
					ctrl.isSubmitting = function ( ) {
						return submitting;
					};
					
					ctrl.getLastSaved = function ( ) {
						return lastSaved;
					};
					
					ctrl.isActionDisabled = function ( action ) {
						return _.isEmpty(action.disabled) || $parse(action.disabled)($scope);
					};
					
					ctrl.isActionVisible = function ( action ) {
						return _.isEmpty(action.when) || $parse(action.when)($scope);	
					};
					
					ctrl.hasVisibleActions = function ( ) {
						return _.some(ctrl.actions, function ( action ) {
							return ctrl.isActionVisible(action);
						});
					};
					
					function parseConfig ( ) {
						var config = $scope.$eval($attrs.zsForm) || {};
						ctrl.setConfig(config);
					}

					if ($attrs.zsForm === undefined) {
						parseConfig();
					} else {
						$attrs.$observe('zsForm', function ( ) {
							parseConfig();
						});
					}
					
					return ctrl;
				}],
				controllerAs: 'form'
			};
			
		}]);
	
})();
