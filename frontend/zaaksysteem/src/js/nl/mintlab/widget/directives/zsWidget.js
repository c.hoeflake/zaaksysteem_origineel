// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.widget').directive('zsWidget', [
    function () {
      return {
        templateUrl: '/html/widget/widget.html',
        transclude: true,
        scope: true,
        link: function (scope, element, attrs) {
          function setTitle() {
            scope.widgetTitle = attrs.zsWidgetTitle;
          }

          if (attrs.zsWidgetTitle === undefined) {
            setTitle();
          } else {
            attrs.$observe('zsWidgetTitle', setTitle);
          }
        },
      };
    },
  ]);
})();
