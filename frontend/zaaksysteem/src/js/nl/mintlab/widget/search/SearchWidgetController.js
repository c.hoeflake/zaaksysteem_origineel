// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.widget.search')
    .controller('nl.mintlab.widget.search.SearchWidgetController', [
      '$scope',
      '$window',
      '$http',
      '$q',
      '$parse',
      'systemMessageService',
      'searchService',
      'formService',
      'translationService',
      'zqlService',
      'zqlEscapeFilter',
      function (
        $scope,
        $window,
        $http,
        $q,
        $parse,
        systemMessageService,
        searchService,
        formService,
        translationService,
        zqlService,
        zqlEscapeFilter
      ) {
        var generateUid = window.zsFetch('nl.mintlab.utils.generateUid'),
          groupAll = {
            label: 'Alles',
            value: null,
            count: NaN,
          },
          crudUrl = null,
          groupingUrl = null,
          sortBy = null,
          sortOrder = null,
          httpCancel,
          resultOptions = [],
          activeColumns = [],
          showLocation = false,
          itemActions = [];

        $scope.options = { objectType: null };

        $scope.config = null;

        $scope.filtersLocked = true;
        $scope.groups = [groupAll];

        $scope.openGroup = null;
        $scope.loading = false;

        $scope.search = searchService.create();

        $scope.chartOptions = [];

        $scope.loading = {
          crud: false,
          groups: false,
          linechart: false,
          bulk: false,
        };

        function setConfig(config) {
          var deferred = $q.defer(),
            hasChanged = !angular.equals(config.filters, $scope.filters),
            unregister;

          $scope.config = config;

          if ($scope.hasChart()) {
            $scope.chartOptions = config.chart.options;
            $scope.options.chartProfile = $scope.chartOptions[0];
          }

          $scope.groupings = config.grouping || [];

          _.each(config, function (value, key) {
            $scope[key] = value;
          });

          setActions();
          setCollapsed();

          $scope.activeGrouping = $scope.groupings[0];

          $scope.$broadcast('search.config.change');

          if (hasChanged) {
            unregister = $scope.$on('form.config.change', function () {
              deferred.resolve();
              unregister();
            });
          } else {
            deferred.resolve();
          }

          return deferred.promise;
        }

        function isPublic() {
          return !!$scope.public;
        }

        function getConfig() {
          var objectType = $scope.options.objectType
              ? $scope.options.objectType.object_type
              : null,
            deferred = $q.defer();

          searchService
            .getConfig(objectType, {
              baseUrl: isPublic()
                ? '/api/public_search/' + $scope.interfaceId + '/config'
                : '/api/search/config',
            })
            .then(function (config) {
              setConfig(config).then(function () {
                deferred.resolve(config);
              });
            })
            ['catch'](function () {
              deferred.reject(
                systemMessageService.emitLoadError(
                  'de configuratie voor dit objecttype'
                )
              );
            });

          return deferred.promise;
        }

        function getUrlBase() {
          return isPublic()
            ? '/api/public_search/' + $scope.interfaceId
            : '/api/object';
        }

        function setSearchForm() {
          var form = formService.get('searchWidgetForm'),
            key,
            values,
            scope,
            fieldsets,
            fields,
            field,
            fieldset,
            hasChangedValue,
            i,
            l,
            j,
            m;

          if (!form || !$scope.activeSearch) {
            return;
          }

          scope = form.scope;
          fieldsets = scope.zsForm.fieldsets;

          values = $scope.activeSearch.values.values;

          scope.resetForm();

          for (key in values) {
            form.setValue(key, angular.copy(values[key]));
          }

          for (i = 0, l = fieldsets.length; i < l; ++i) {
            fieldset = fieldsets[i];
            fields = fieldset.fields;
            hasChangedValue = false;
            for (j = 0, m = fields.length; j < m; ++j) {
              field = fields[j];
              if (!scope.isDefaultValue(field)) {
                hasChangedValue = true;
                break;
              }
            }
            fieldset.collapsed = !(
              _.indexOf(['fulltext'], fieldset.name) !== -1 || hasChangedValue
            );
          }

          form.scope.submitForm();
        }

        function escapeForZql(val) {
          return zqlEscapeFilter(val);
        }

        function reloadGroups(force) {
          var grouping = $scope.getZqlGrouping(),
            where = encodeURIComponent($scope.getZqlWhere()),
            matching = encodeURIComponent($scope.getZqlMatching()),
            url,
            deferred = $q.defer();

          if (grouping) {
            url =
              getUrlBase() +
              '/search/?zql=SELECT DISTINCT ' +
              grouping +
              ' FROM ' +
              $scope.options.objectType.object_type +
              (matching ? ' MATCHING ' + matching : '') +
              (where ? ' WHERE (' + where + ')' : '');
          }

          if (url) {
            if (url === groupingUrl && !force) {
              return;
            }

            $scope.loading.groups = true;

            $http({
              method: 'GET',
              url: url,
              params: {
                zapi_no_pager: 1,
              },
            })
              .success(function (response) {
                var group,
                  groups = [],
                  grouping = $scope.activeGrouping,
                  groupsById = {};

                _.each(response.result, function (gr) {
                  var label = gr[grouping.resolve];

                  if (grouping.translations) {
                    label = grouping.translations[label];
                  }

                  if (!label) {
                    label = translationService.get('Geen');
                  }

                  group = {
                    value: gr[grouping.by],
                    label: label,
                    count: gr.count,
                  };

                  if (groupsById[group.value]) {
                    group = groupsById[group.value];
                    group.count += gr.count;
                  } else {
                    groupsById[group.value] = group;
                    groups.push(group);
                  }
                });

                groups = _.reject(groups, { value: null });

                $scope.groups = groups;

                setCrudUrl();

                deferred.resolve();
              })
              .error(function (/*response*/) {
                $scope.$emit('systemMessage', {
                  type: 'error',
                  content: translationService.get(
                    'Er ging iets fout bij het ophalen van de groeperingen.'
                  ),
                });

                deferred.reject(arguments[0]);
              })
              ['finally'](function () {
                $scope.loading.groups = false;
              });
          } else {
            $scope.groups = [groupAll];
            $scope.loading.groups = false;
            setCrudUrl();
            deferred.resolve();
          }

          groupingUrl = url;

          return deferred.promise;
        }

        function setCrudUrl() {
          var zql = encodeURIComponent($scope.getZql());

          crudUrl = zql ? getUrlBase() + '/search/?zql=' + zql : '';
        }

        function getColumnFromAttr(attr) {
          var column = searchService.getTypeColumn(
            attr.object.column_name,
            attr.public_label || attr.label,
            attr.object.value_type
          );

          setColumnProperties(column);

          return column;
        }

        function setColumnProperties(column) {
          var template = $scope.columns.templates[column.id];

          if (!template) {
            // try it without the attribute prefix
            template =
              $scope.columns.templates[column.id.replace('attribute.', '')];
          }

          if (template) {
            for (var key in template) {
              if (key !== '$$hashKey') {
                column[key] = template[key];
              }
            }
          }

          // by default, all columns are unsortable unless true
          if (column.sort === undefined) {
            column.sort = false;
          }
        }

        function setActions() {
          var actionGroup = _.find($scope.actions, { id: 'actions' });

          if (actionGroup) {
            itemActions = actionGroup.data.children;
          } else {
            itemActions = $scope.actions || [];
          }
        }

        function setCollapsed() {
          var fieldsets = $scope.filters.fieldsets;
          _.each(fieldsets, function (fieldset) {
            fieldset.collapsed = true;
          });
        }

        // FIXME: for the love of god PLEASE do
        // not convert numbers to strings
        function isValueTrue(val) {
          return typeof val === 'string' ? val === '1' : val;
        }

        $scope.getDate = function ($values) {
          var dateTypes = _.reject($values, { name: 'date' }),
            prop = {},
            range = _.find($values, { name: 'date' }).value,
            count = _.reduce(
              dateTypes,
              function (sum, dateType) {
                return isValueTrue(dateType.value) ? sum + 1 : sum;
              },
              0
            ),
            some = count > 1,
            none = !count;

          if (!range) {
            return null;
          }

          if (some || none) {
            prop.name = 'date';
            prop.type = 'property-group';
            prop.relation = some ? 'AND' : 'OR';
            prop.value = [];
            _.each(dateTypes, function (dateType) {
              if (none || isValueTrue(dateType.value)) {
                prop.value.push({
                  name: dateType.name,
                  type: 'date-range',
                  value: range,
                });
              }
            });
          } else {
            prop.name = _.find(dateTypes, function (dateType) {
              return isValueTrue(dateType.value);
            }).name;
            prop.type = 'date-range';
            prop.value = range;
          }

          return prop;
        };

        $scope.getAddress = function ($values) {
          var prop,
            obj = $values[0],
            values = obj.unresolved || [],
            properties,
            name;

          if (!values.length) {
            return null;
          }

          name = obj.name;

          properties = _.map(values, function (val, index) {
            var prop = {};

            prop.name =
              name +
              (val.type === 'address'
                ? '.nummeraanduiding'
                : '.openbareruimte');
            prop.type = 'spot_enlighter';
            prop.value = obj.value[index];

            return prop;
          });

          prop = {
            name: 'address',
            type: 'property-group',
            relation: 'OR',
            value: properties,
          };

          return prop;
        };

        $scope.getMatchingStatement = function ($values) {
          var obj = $values[0],
            value = obj.value;

          if (!value) {
            return null;
          }

          return {
            name: obj.name,
            type: obj.type,
            value: obj.value,
            operator: '~',
          };
        };

        $scope.getApiBaseUrl = getUrlBase;
        $scope.isPublic = isPublic;

        $scope.getZql = function () {
          var where = $scope.getZqlWhere(),
            matching = $scope.getZqlMatching(),
            columns = $scope.getZqlColumns(),
            zql,
            isGrouped =
              $scope.activeGrouping &&
              $scope.activeGrouping.by !== null &&
              $scope.openGroup,
            column,
            as = '';

          if (!columns || !$scope.options.objectType) {
            return '';
          }

          zql =
            'SELECT ' +
            $scope.getZqlColumns() +
            ' FROM ' +
            $scope.options.objectType.object_type;

          if (matching) {
            zql += ' MATCHING ' + matching;
          }

          if (where || isGrouped) {
            zql += ' WHERE ';
            if (where) {
              zql += where;
            }
            if (isGrouped) {
              zql +=
                (where ? ' AND ' : '') +
                $scope.activeGrouping.by +
                ' = ' +
                escapeForZql($scope.openGroup);
            }
            zql += '';
          }

          if (sortBy && sortOrder) {
            column = _.find($scope.getActiveColumns(), { id: sortBy });
            if (column && column.sort && column.sort.type === 'numeric') {
              as = ' NUMERIC';
            }
            zql +=
              as +
              ' ORDER BY ' +
              sortBy +
              (sortOrder === 'asc' ? ' ASC' : ' DESC');
          }

          return zql;
        };

        $scope.getZqlWhere = function () {
          var form = formService.get('searchWidgetForm'),
            where = '',
            values,
            unresolvedValues = {},
            fieldsets,
            fields,
            properties = [],
            conditions = [],
            orgUnits = [];

          if (!form) {
            return '';
          }

          values = form.scope.getValues(true);

          unresolvedValues = form.scope.getValues(false);

          fields = angular.copy(form.scope.zsForm.fields);
          fieldsets = form.scope.zsForm.fieldsets;

          _.each(fieldsets, function (fieldset) {
            var fieldsetValues = [],
              prop;

            if (fieldset.resolve) {
              _.each(fieldset.fields, function (field) {
                _.remove(fields, { name: field.name });
                fieldsetValues.push({
                  name: field.name,
                  type: field.type,
                  value: values[field.name],
                  unresolved: unresolvedValues[field.name],
                });
              });

              prop = $parse(fieldset.resolve)($scope, {
                $values: fieldsetValues,
              });

              if (prop) {
                properties.push(prop);
              }
            }
          });

          _.each(fields, function (field) {
            var val = values[field.name],
              vals;

            if (
              !field.ignore &&
              val !== undefined &&
              val !== null &&
              !(_.isArray(val) && val.length === 0)
            ) {
              switch (field.type) {
                case 'org-unit':
                  _.each(val, function (item) {
                    vals = [];

                    if (item.orgUnit.org_unit_id) {
                      vals.push({
                        name: field.name.split('$')[0],
                        value: item.orgUnit.org_unit_id,
                        type: field.type,
                      });
                    }

                    if (item.orgUnit.role_id) {
                      vals.push({
                        name: field.name.split('$')[1],
                        value: item.orgUnit.role_id,
                        type: field.type,
                      });
                    }

                    if (vals.length) {
                      orgUnits.push({
                        name: field.name,
                        type: 'property-group',
                        relation: 'AND',
                        value: vals,
                      });
                    }
                  });
                  break;

                default:
                  properties.push({
                    name: field.name,
                    value: val,
                    type: field.type,
                    operator:
                      field.data && field.data.operator
                        ? field.data.operator
                        : undefined,
                  });
                  break;
              }
            }
          });

          properties = _.reject(properties, function (prop) {
            return prop.name === 'fulltext';
          });

          if (properties.length) {
            conditions = zqlService.getConditions(properties);
            if (conditions.length) {
              where = conditions.join(' AND ');
            }
          }

          if (orgUnits.length) {
            if (where) {
              where += ' AND ';
            }
            where +=
              '(' + zqlService.getConditions(orgUnits).join(' OR ') + ')';
          }

          return where;
        };

        $scope.getZqlMatching = function () {
          var form = formService.get('searchWidgetForm'),
            matching = '',
            values,
            field;

          if (!form) {
            return '';
          }

          values = form.scope.getValues(true);

          field = _.find(form.scope.zsForm.fields, function (field) {
            return field.name === 'fulltext';
          });

          if (field && values[field.name]) {
            matching = escapeForZql(values[field.name]);
          }

          return matching;
        };

        $scope.getZqlColumns = function (params) {
          var search = $scope.activeSearch,
            columns = [],
            without = (params && params.without) || [],
            visibleOnly = params && !!params.visibleOnly;

          if (!search) {
            return '';
          }

          _.each($scope.getActiveColumns(), function (col) {
            if (col.data && col.data.columns) {
              columns = columns.concat(col.data.columns);
            } else {
              columns.push(col.id);
            }
          });

          columns = _.difference(columns, without);

          if (!visibleOnly) {
            _.each($scope.columns.always, function (colId) {
              columns.push(colId);
            });

            if ($scope.isLocationVisible() && $scope.map) {
              _.each($scope.map.columns, function (colId) {
                columns.push(colId);
              });
            }
          }

          columns = _.uniq(columns);

          return columns.join(', ');
        };

        $scope.getZqlGrouping = function () {
          var grouping;

          if ($scope.activeGrouping && $scope.activeGrouping.by) {
            grouping = [$scope.activeGrouping.by];
            if ($scope.activeGrouping.by !== $scope.activeGrouping.resolve) {
              grouping.push($scope.activeGrouping.resolve);
            }
            grouping = grouping.join(', ');
          }

          return grouping;
        };

        $scope.getResultOptions = function () {
          return resultOptions;
        };

        $scope.handleLockClick = function () {
          $scope.filtersLocked = !$scope.filtersLocked;
        };

        $scope.setSearch = function (search) {
          $scope.options.objectType = search.values.objectType;

          return getConfig().then(function () {
            $scope.activeSearch = search;

            if (_.isEmpty(search.values.columns)) {
              search.values.columns = $scope.getDefaultColumns();
            }

            $scope.updateColumns(search.values.columns);

            $scope.updateAttributes(search);

            if (_.isEmpty(search.values.options.sort)) {
              search.values.options.sort = $scope.getDefaultSort();
            }

            if (search.values.options.showLocation === undefined) {
              search.values.options.showLocation = false;
            }

            $scope.setActiveColumns(angular.copy(search.values.columns));

            sortBy = search.values.options.sort.by || $scope.sort.by;
            sortOrder = search.values.options.sort.order || $scope.sort.order;
            showLocation = search.values.options.showLocation;

            setSearchForm();
            if (!search.values.zql) {
              search.values.zql = $scope.getZql();
            }

            $scope.$broadcast('activeSearch.change', $scope.activeSearch);
          });
        };

        $scope.getCrudUrl = function () {
          return crudUrl;
        };

        $scope.setOpenGroup = function (id) {
          $scope.openGroup = id;
          setCrudUrl();
        };

        $scope.getActions = function () {
          return $scope.actions;
        };

        $scope.getItemActions = function () {
          return itemActions;
        };

        $scope.getItemLink = function () {
          return $scope.link;
        };

        $scope.getItemResolve = function () {
          return $scope.resolve;
        };

        $scope.getItemStyle = function () {
          return $scope.style;
        };

        $scope.getDefaultSort = function () {
          return $scope.sort;
        };

        $scope.getSearchColumns = function ($query) {
          var deferred = $q.defer(),
            columns = [];

          if (httpCancel) {
            httpCancel.resolve();
          }

          httpCancel = $q.defer();

          $http({
            method: 'GET',
            url: '/objectsearch/attributes',
            params: {
              query: $query,
              all: $query ? 0 : 1,
            },
            timeout: httpCancel.promise,
          })
            .success(function (response) {
              var query = $query.toLowerCase();

              columns = _.map(response.json.entries, function (attr) {
                return _.assign({}, getColumnFromAttr(attr), {
                  description:
                    attr.public_label && attr.public_label !== attr.label
                      ? attr.label
                      : '',
                });
              });

              _.each($scope.columns.extra, function (columnId) {
                var column = {
                    id: columnId,
                    resolve: 'values["' + columnId + '"]',
                  },
                  label,
                  id;

                setColumnProperties(column);

                label = column.label.toLowerCase();
                id = column.id.toLowerCase();

                if (
                  !query ||
                  label.indexOf(query) !== -1 ||
                  id.indexOf(query) !== -1
                ) {
                  columns.push(column);
                }
              });

              columns = _.sortByAll(
                columns,
                function (col) {
                  var isSelected = !!_.find($scope.getActiveColumns(), {
                    id: col.id,
                  });
                  return isSelected ? 0 : 1;
                },
                function (col) {
                  return col.label.toLowerCase();
                }
              );
            })
            ['finally'](function () {
              deferred.resolve(columns);
            });

          return deferred.promise;
        };

        $scope.setActiveSort = function (sort) {
          sortBy = sort.by;
          sortOrder = sort.order;
        };

        $scope.getActiveSort = function () {
          return {
            by: sortBy || null,
            order: sortOrder || null,
          };
        };

        $scope.setActiveColumns = function (columns) {
          activeColumns = columns;
        };

        $scope.getActiveColumns = function () {
          return activeColumns;
        };

        $scope.setActiveValues = function (vals) {
          var form = formService.get('searchWidgetForm');

          if (form) {
            for (var key in vals) {
              form.setValue(key, angular.copy(vals[key]));
            }
          }
        };

        $scope.getActiveValues = function () {
          var form = formService.get('searchWidgetForm'),
            values = angular.copy(form.getValues(false));

          return values;
        };

        $scope.getDefaultColumns = function () {
          var columns = $scope.columns['default'];

          return _.map(columns, function (colId) {
            var column = {
              id: colId,
              resolve: 'values["' + colId + '"]',
            };
            setColumnProperties(column);
            return column;
          });
        };

        $scope.revertChanges = function () {
          var form = formService.get('searchWidgetForm'),
            search = $scope.activeSearch,
            values = search.values.values;

          form.reset();

          for (var key in values) {
            form.setValue(key, angular.copy(values[key]));
          }

          $scope.setActiveColumns(angular.copy(search.values.columns));

          sortBy = search.values.options.sort.by;
          sortOrder = search.values.options.sort.order;
          showLocation = search.values.options.showLocation;

          if (
            !angular.equals($scope.options.objectType, search.values.objectType)
          ) {
            $scope.options.objectType = search.values.objectType;
            getConfig().then(function () {
              setCrudUrl();
            });
          } else {
            setCrudUrl();
          }
        };

        $scope.toggleLocationVisible = function () {
          $scope.setLocationVisibility(!showLocation);
        };

        $scope.setLocationVisibility = function (show) {
          showLocation = show;
          setCrudUrl();
        };

        $scope.isLocationVisible = function () {
          return showLocation;
        };

        $scope.setCrudLoading = function (loading) {
          $scope.loading.crud = loading;
        };

        $scope.isLoading = function () {
          var isLoading = _.some($scope.loading);
          return isLoading;
        };

        $scope.isChanged = function () {
          if (!$scope.activeSearch) {
            return false;
          }

          var sort = $scope.getActiveSort(),
            showLocation = $scope.isLocationVisible(),
            zql = $scope.getZql(),
            obj;

          obj = {
            values: {
              zql: zql,
              options: {
                sort: sort,
                showLocation: showLocation,
              },
              objectType: $scope.options.objectType,
            },
          };

          return searchService.isChanged($scope.activeSearch, obj);
        };

        $scope.getActiveSearchClone = function () {
          var from = angular.copy($scope.activeSearch);

          from.values.zql = $scope.getZql();
          from.values.values = $scope.getActiveValues();
          from.values.columns = $scope.getActiveColumns();
          from.values.options.sort = $scope.getActiveSort();
          from.values.options.showLocation = $scope.isLocationVisible();
          from.values.objectType = $scope.getObjectType();

          return from;
        };

        $scope.handleObjectTypeChange = function (/*$object*/) {
          getConfig().then(function () {
            $scope.setActiveColumns($scope.getDefaultColumns());
          });
        };

        $scope.hasObjectType = function () {
          return !!$scope.options.objectType;
        };

        $scope.getObjectType = function () {
          return $scope.options.objectType;
        };

        $scope.setActiveObjectType = function (objectType) {
          $scope.options.objectType = objectType;
        };

        $scope.hasChart = function () {
          return $scope.config && !_.isEmpty($scope.config.chart);
        };

        $scope.hasMap = function () {
          return $scope.config && !_.isEmpty($scope.config.map);
        };

        $scope.updateColumns = function (columns) {
          var templates = $scope.columns.templates || {};

          _.each(columns, function (column) {
            var tpl = templates[column.id];
            if (tpl) {
              angular.extend(column, tpl);
            }
          });
        };

        $scope.updateAttributes = function (search) {
          var attrs = search.values.values.attributes
              ? search.values.values.attributes.attributes
              : [],
            updated = [];

          $q.all(
            attrs.map(function (attr) {
              return $http({
                url: '/objectsearch/attributes?all=0&query=' + attr.label,
              }).success(function (response) {
                var column = _.find(response.json.entries, { id: attr.id });
                if (column) {
                  updated = updated.concat(column);
                }
              });
            })
          ).then(function () {
            updated.forEach(function (attr) {
              var toUpdate = _.find(attrs, { id: attr.id });
              if (toUpdate) {
                _.assign(toUpdate, attr, {
                  //regenerate uid to override track by caching
                  _id: generateUid(),
                });
              }
            });

            setSearchForm();
          });
        };

        $scope.refresh = function () {
          reloadGroups(true).then(function () {
            $scope.$broadcast('search.reload');
          });
        };

        $scope.$watch('activeGrouping', function () {
          reloadGroups();
        });

        $scope.$on('form.ready', function (event) {
          if (event.targetScope.getName() === 'searchWidgetForm') {
            setSearchForm();
          }
        });

        $scope.$on('form.submit.attempt', function () {
          reloadGroups();
        });

        $scope.$on('crud.sort.change', function ($event, by, order) {
          sortBy = by;
          sortOrder = order;
          setCrudUrl();
        });

        $scope.$on('crud.column.add', function ($event, column) {
          var columns = $scope.getActiveColumns(),
            last = _.findLastIndex(columns, function (col) {
              return !col.locked;
            });

          columns.splice(last - 1, 0, column);
          setCrudUrl();
        });

        $scope.$on('crud.column.remove', function ($event, column) {
          _.remove($scope.getActiveColumns(), function (col) {
            return col.id === column.id;
          });
          setCrudUrl();
        });

        $scope.$on('crud.column.sort.update', function (event, column, before) {
          var columns = $scope.getActiveColumns().concat(),
            index = _.findIndex(columns, { id: column.id }),
            last = _.findLastIndex(columns, function (col) {
              return !col.locked;
            }),
            to = before ? _.findIndex(columns, { id: before.id }) : last;

          if (index === to) {
            return;
          }

          column = _.find(columns, { id: column.id });
          columns = _.pull(columns, column);

          if (before) {
            to = _.findIndex(columns, { id: before.id });
            columns.splice(to, 0, column);
          } else {
            columns.splice(last, 0, column);
          }

          $scope.setActiveColumns(columns);
        });

        $scope.init = function () {
          if (!isPublic()) {
            // todo: find a generic solution for this

            $http({
              method: 'GET',
              url: '/api/case/results',
              params: {
                zapi_no_pager: 1,
              },
            })
              .success(function (response) {
                resultOptions = response.result;
              })
              .error(function (/*response*/) {
                $scope.$emit('systemMessage', {
                  type: 'error',
                  content: translationService.get(
                    'Er ging iets fout bij het ophalen van de mogelijkheden voor resultaat'
                  ),
                });
              });
          }
        };
      },
    ]);
})();
