// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem').directive('zsArrayRequired', [
    function () {
      return {
        require: ['ngModel'],
        compile: function () {
          return function link(scope, element, attrs, controllers) {
            var ngModel = controllers[0];

            var validator = function (value) {
              var val = value && angular.isArray(value) ? value : [],
                isValid = val.length > 0,
                required = scope.$eval(attrs.zsArrayRequired);

              if (required && !isValid) {
                ngModel.$setValidity('required', false);
                return;
              } else {
                ngModel.$setValidity('required', true);
                return value;
              }
            };

            ngModel.$formatters.push(validator);
            ngModel.$parsers.push(validator);

            attrs.$observe('zsArrayRequired', function () {
              validator(ngModel.$viewValue);
            });
          };
        },
      };
    },
  ]);
})();
