// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem').directive('zsEzraMap', [
    '$http',
    '$q',
    function ($http, $q) {
      var center = $http
        .get('/api/v1/map/ol_settings', { cache: true })
        .then(function (response) {
          var coords = response.data.result.instance.map_center || '';
          coords = coords.replace(',', ' ');
          return coords;
        })
        .catch(function () {
          return '';
        });

      return {
        scope: {
          getCoordinates: '&',
        },
        controller: [
          '$element',
          function ($element) {
            var ctrl = this,
              markers = [],
              mapEl = $($element[0].querySelector('.ezra_map')),
              onSelect = [],
              onLoad = $q.defer(),
              isLoaded = false;

            function mapFunc() {
              return mapEl.ezra_map.apply(mapEl, arguments);
            }

            function clearMarkers() {
              if (markers.length) {
                mapFunc('clearMarkers');
              }
            }

            function resolveMapLoad() {
              isLoaded = true;
              onLoad.resolve();
            }

            function addMarkers() {
              if (!isLoaded) {
                return;
              }
              _.each(markers, function (marker) {
                mapFunc('setMarker', marker);
              });
            }

            ctrl.addMarker = function (marker) {
              clearMarkers();
              markers.push(marker);
              addMarkers();
            };

            ctrl.removeMarker = function (marker) {
              clearMarkers();
              _.pull(markers, marker);
              addMarkers();
            };

            ctrl.setMarkers = function (newMarkers) {
              clearMarkers();
              markers = newMarkers.concat();
              addMarkers();
            };

            ctrl.fromLatLngToGps = function (lat, lng) {
              return mapFunc('_get_epsg_points', lat, lng);
            };

            ctrl.onLoad = onLoad.promise;

            ctrl.isLoaded = function () {
              return isLoaded;
            };

            ctrl.onSelect = onSelect;

            mapEl.bind('ezra_map_click', function (event, coordinates) {
              _.invoke(onSelect, 'call', null, coordinates);
            });

            if (mapEl.hasClass('ezra_map_initialized')) {
              resolveMapLoad();
            } else {
              mapEl.bind('ezra_map_initialized', function () {
                resolveMapLoad();
                addMarkers();
              });
            }

            return ctrl;
          },
        ],
        link: function (scope /*, element, attrs*/) {
          center.then(function (ct) {
            var options = {};

            if (ct) {
              options.center = ct;
            }

            if (scope.getCoordinates) {
              options.markerCoordinates = scope.getCoordinates();
            }

            load_ezra_map(options);
          });
        },
      };
    },
  ]);
})();
