// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  window.zsDefine('nl.mintlab.utils.generateUuid', function () {
    function generateUuid() {
      var cryptoObj = window.crypto || window.msCrypto;
      return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, function (
        c
      ) {
        return (
          c ^
          (cryptoObj.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
        ).toString(16);
      });
    }

    return function (prefix) {
      return generateUuid(prefix);
    };
  });
})();
