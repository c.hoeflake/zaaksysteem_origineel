// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.case.relation').directive('zsPipAuthorizedForm', [
    '$parse',
    '$q',
    '$http',
    'pipAuthorizationService',
    'systemMessageService',
    function (
      $parse,
      $q,
      $http,
      pipAuthorizationService,
      systemMessageService
    ) {
      var safeApply = window.zsFetch('nl.mintlab.utils.safeApply'),
        formConfig = {
          name: 'authorized',
          fields: [
            {
              name: 'relation_type',
              label: 'Relatietype',
              type: 'radio',
              data: {
                options: [
                  {
                    label: 'Burger',
                    value: 'natuurlijk_persoon',
                  },
                  {
                    label: 'Organisatie',
                    value: 'bedrijf',
                  },
                ],
              },
              default: 'natuurlijk_persoon',
              required: true,
            },
            {
              name: 'betrokkene',
              label: 'Betrokkene',
              type: 'spot-enlighter',
              data: {
                restrict:
                  'contact/<[relation_type==="natuurlijk_persoon"?"natuurlijk_persoon":"bedrijf"]>',
                label: 'decorated_name||handelsnaam',
              },
              required: true,
            },
            {
              name: 'role',
              label: 'Rol betrokkene',
              type: 'select',
              data: {
                options: [],
              },
              default: null,
              required: true,
            },
            {
              name: 'role_custom',
              label: 'Andere rol',
              type: 'text',
              data: {
                placeholder: '',
              },
              default: '',
              required: 'role==="Anders"',
              when: 'role==="Anders"',
            },
            {
              name: 'magic_string_prefix',
              label: 'Magic string prefix',
              type: 'text',
              disabled: true,
              required: true,
            },
            {
              name: 'pip_authorized',
              label: 'Gemachtigd voor deze zaak',
              type: 'checkbox',
              required: true,
              default: true,
            },
            {
              name: 'notify_subject',
              label: 'Verstuur bevestiging per e-mail',
              type: 'checkbox',
              default: false,
              when: '!!pip_authorized',
            },
          ],
          actions: [
            {
              name: 'submit',
              type: 'submit',
              label: 'Toevoegen',
            },
          ],
          promises: [
            {
              watch: 'pip_authorized',
              when: '!!pip_authorized',
              then: 'notify_subject=true',
            },
          ],
        };

      return {
        require: ['zsPipAuthorizedForm', 'zsFormTemplateParser'],
        scope: true,
        controller: [
          '$scope',
          '$attrs',
          function ($scope, $attrs) {
            var ctrl = this,
              zsFormTemplateParser,
              config = angular.copy(formConfig),
              authorization = angular.copy($scope.$eval($attrs.authorization)),
              defaults,
              roles;

            function getDefaults() {
              var defaults, isCustom;

              if (!authorization) {
                return;
              }

              defaults = _.pick(
                authorization,
                'role',
                'magic_string_prefix',
                'pip_authorized'
              );

              isCustom = !_.find(roles, { value: defaults.role });

              if (isCustom) {
                defaults.role = 'Anders';
                defaults.role_custom = authorization.role;
              }

              return defaults;
            }

            function getAuthorizationFromForm(values) {
              var formAuth,
                betrokkene = values.betrokkene;

              if (!betrokkene) {
                // create betrokkene object from authorization
                betrokkene = {
                  id: authorization.betrokkene_id,
                  object_type: authorization.betrokkene_type,
                  decorated_name: authorization.name,
                  handelsnaam: authorization.name,
                };
              }

              values = angular.copy(values);

              if (betrokkene) {
                values.betrokkene_identifier =
                  'betrokkene-' + betrokkene.object_type + '-' + betrokkene.id;
              }

              if (values.role === 'Anders') {
                values.role = values.role_custom;
              }

              formAuth = pipAuthorizationService.getApiParams(values);
              formAuth.name =
                betrokkene.decorated_name || betrokkene.handelsnaam;

              return formAuth;
            }

            (function () {
              var cancel, reloadPrefix;

              $scope.$on('form.change', function (event, field, nwVal, oldVal) {
                if (
                  (field.name === 'role' || field.name === 'role_custom') &&
                  nwVal !== oldVal
                ) {
                  zsFormTemplateParser.setValue('magic_string_prefix', '');
                }
              });

              $scope.$on('form.change.committed', function (event, field) {
                if (field.name === 'role' || field.name === 'role_custom') {
                  reloadPrefix();
                }
              });

              reloadPrefix = _.debounce(
                function () {
                  safeApply($scope, function () {
                    var role = zsFormTemplateParser.getValue('role'),
                      caseId = $scope.$eval($attrs.caseId);

                    if (role === 'Anders') {
                      role = zsFormTemplateParser.getValue('role_custom');
                    }

                    if (cancel) {
                      cancel.resolve();
                    }

                    cancel = $q.defer();

                    if (!role) {
                      return;
                    }

                    $http({
                      method: 'GET',
                      url: ' /zaak/' + caseId + '/update/betrokkene/suggestion',
                      params: {
                        rol: role,
                      },
                      timeout: cancel.promise,
                    })
                      .success(function (response) {
                        zsFormTemplateParser.setValue(
                          'magic_string_prefix',
                          response
                        );
                      })
                      .error(function (data, status) {
                        if (status !== 0) {
                          throw systemMessageService.emitLoadError(
                            'magic string'
                          );
                        }
                      });
                  });
                },
                250,
                { maxWait: 500 }
              );
            })();

            ctrl.link = function (controllers) {
              zsFormTemplateParser = controllers[0];
            };

            ctrl.getFormConfig = function () {
              return config;
            };

            ctrl.handleSubmit = function ($values) {
              var authorization = getAuthorizationFromForm($values),
                promise;

              promise = $parse($attrs.submit)($scope, {
                $authorization: authorization,
              });

              if (promise) {
                promise.then(function () {
                  $scope.closePopup();
                });
              }
            };

            pipAuthorizationService.getRoles().then(function (r) {
              roles = r;

              _.find(config.fields, { name: 'role' }).data.options = roles;

              if (authorization) {
                _.remove(config.fields, function (field) {
                  return (
                    ['betrokkene', 'relation_type', 'notify_subject'].indexOf(
                      field.name
                    ) !== -1
                  );
                });

                defaults = getDefaults();

                _.each(defaults, function (value, key) {
                  _.find(config.fields, { name: key }).value = value;
                });
              }

              _.find(config.actions, { name: 'submit' }).label = authorization
                ? 'Wijzigen'
                : 'Toevoegen';
            });

            return ctrl;
          },
        ],
        controllerAs: 'pipAuthorizedForm',
        link: function (scope, element, attrs, controllers) {
          controllers[0].link(controllers.splice(1));
        },
      };
    },
  ]);
})();
