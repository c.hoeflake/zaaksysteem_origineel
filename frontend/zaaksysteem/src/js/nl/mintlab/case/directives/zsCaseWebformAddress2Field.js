// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  angular.module('Zaaksysteem.case').directive('zsCaseWebformAddress2Field', [
    function () {
      return {
        scope: true,
        require: ['zsCaseWebformAddress2Field', '^zsCaseWebformField'],
        controller: [
          'smartHttp',
          '$element',
          '$compile',
          '$scope',
          'sessionService',
          'composedReducer',
          function (
            smartHttp,
            $element,
            $compile,
            $scope,
            sessionService,
            composedReducer
          ) {
            var ctrl = this,
              parentController,
              config,
              input = $element[0].querySelector('input'),
              placeholder = $element[0].querySelector('.address-placeholder'),
              iframe = document.createElement('iframe');
            currentValue = null;
            currentValueFlat = null;

            if (input && input.value) {
              try {
                currentValue = JSON.parse(input.value);
                currentValueFlat = { label: currentValue.address.full };
              } catch (err) {}
            }

            try {
              angular.forEach(
                $element.parent().parent().parent().children(),
                function (el) {
                  el.className ===
                    'kenmerk-veld ezra_field_wrapper row ng-scope' &&
                    $(el).insertAfter(el.parentElement);
                }
              );
            } catch (err) {
              console.log(err);
            }

            var sessionResource = sessionService.createResource($scope);

            var sessionReducer = composedReducer(
              { scope: $scope },
              sessionResource
            ).reduce(function (session) {
              return session ? session.instance : {};
            });

            function config() {
              return sessionReducer.data().configurable;
            }

            Object.defineProperty($scope, 'config', { value: config });

            Object.defineProperty($scope, 'spotEnlighterAddressValue', {
              get: function () {
                return currentValueFlat;
              },
              set: function (enlighterVal) {
                if (!enlighterVal) {
                  input.value = '';
                  currentValue = null;
                  currentValueFlat = null;
                  sendMessage({
                    type: 'setMarker',
                    version: 4,
                    name: input.name,
                    value: null,
                  });
                } else {
                  var geometry = {
                    type: 'Point',
                    coordinates: [
                      enlighterVal.geo_punt.coordinates[0],
                      enlighterVal.geo_punt.coordinates[1],
                    ],
                  };

                  var val = {
                    bag: {
                      id: enlighterVal.id.split('-')[1],
                      type: 'nummeraanduiding',
                    },
                    address: { full: enlighterVal.label },
                    geojson: {
                      type: 'FeatureCollection',
                      features: [
                        {
                          type: 'Feature',
                          properties: {},
                          geometry: geometry,
                        },
                      ],
                    },
                  };
                  input.value = JSON.stringify(val);
                  currentValue = val;
                  currentValueFlat = {
                    label: currentValue.address.full,
                  };
                  sendMessage({
                    type: 'setMarker',
                    version: 4,
                    name: input.name,
                    value: geometry,
                  });
                }
              },
            });

            var sendMessage = function (message) {
              iframe.contentWindow.postMessage(message, '*');
            };

            var setClosestAddress = function (lat, lon) {
              smartHttp
                .connect({
                  method: 'GET',
                  url: '/zsnl_bag/bag/find-nearest',
                  params: {
                    latitude: lat,
                    longitude: lon,
                  },
                })
                .success(function (response) {
                  var bagData = response.data[0];
                  var fullAddress = (
                    bagData.straatnaam +
                    ' ' +
                    bagData.huisnummer_autocomplete +
                    ', ' +
                    (bagData.postcode || '') +
                    ' ' +
                    bagData.provincie
                  ).replace('  ', ' ');

                  var geometry = {
                    type: 'Point',
                    coordinates: [
                      bagData.geo_punt.coordinates[0],
                      bagData.geo_punt.coordinates[1],
                    ],
                  };

                  var val = {
                    bag: {
                      id: bagData.id.split('-')[1],
                      type: 'nummeraanduiding',
                    },
                    address: { full: fullAddress },
                    geojson: {
                      type: 'FeatureCollection',
                      features: [
                        {
                          type: 'Feature',
                          properties: {},
                          geometry: geometry,
                        },
                      ],
                    },
                  };
                  input.value = JSON.stringify(val);
                  currentValue = val;
                  currentValueFlat = { label: currentValue.address.full };
                  sendMessage({
                    type: 'setMarker',
                    version: 4,
                    name: input.name,
                    value: geometry,
                  });
                });
            };

            smartHttp
              .connect({
                method: 'GET',
                url: '/api/v1/map/ol_settings',
              })
              .success(function (response) {
                var map_center = response.result.instance.map_center;
                var wms_layers = response.result.instance.wms_layers;
                var map_application_url =
                  response.result.instance.map_application_url;
                var map_application = response.result.instance.map_application;

                config = {
                  center: map_center.split(',').map(Number),
                  appUrl:
                    map_application === 'external'
                      ? map_application_url
                      : window.location.origin +
                        '/external-components/index.html?component=map',
                  wmsLayers: wms_layers
                    .filter(function (layer) {
                      return layer.instance.active;
                    })
                    .map(function (layer) {
                      return {
                        url: layer.instance.url,
                        layers: layer.instance.layer_name,
                      };
                    }),
                };
                ctrl.initMap();
              });

            ctrl.initMap = function () {
              iframe.src = config.appUrl;
              iframe.style.width = '100%';
              iframe.style.height = '450px';
              iframe.title = input.name;
              iframe.frameBorder = '0';
              iframe.allow = 'fullscreen';
              iframe.allowFullscreen = true;
              iframe.addEventListener('load', function () {
                sendMessage({
                  type: 'init',
                  name: input.name,
                  version: 4,
                  value: {
                    initialFeature: null,
                    center: config.center,
                    wmsLayers: config.wmsLayers,
                    canDrawFeatures: false,
                  },
                });
                if (currentValue) {
                  sendMessage({
                    type: 'setMarker',
                    version: 4,
                    name: input.name,
                    value: currentValue.geojson.features[0].geometry,
                  });
                }
                window.top.addEventListener('message', ctrl.handleMessage);
              });
              placeholder.innerHtml = '';

              var addressSelect = $compile(
                '<div style="margin: 10px 0 15px 0;" class="spot-enlighter-wrapper"><input type="text" data-ng-model="spotEnlighterAddressValue" data-zs-placeholder="Vul een adres" data-zs-spot-enlighter data-zs-spot-enlighter-restrict="address_v2" data-zs-spot-enlighter-label="label" /></div>'
              )($scope)[0];
              placeholder.appendChild(addressSelect);
              placeholder.appendChild(iframe);
            };

            ctrl.onRemove = function () {
              currentValue = null;
              currentValueFlat = null;
              sendMessage({
                type: 'setMarker',
                version: 4,
                name: input.name,
                value: null,
              });
            };

            ctrl.handleMessage = function (event) {
              if (event.data.type === 'click') {
                setClosestAddress(
                  event.data.value.coordinates[1],
                  event.data.value.coordinates[0]
                );
              }
            };

            ctrl.link = function (controllers) {
              parentController = controllers[0];
              parentController.setGetter(function () {
                return input.value;
              });
            };

            ctrl.handleAddressSelect = function (addressObject) {
              addressObject &&
                sendMessage({
                  type: 'setMarker',
                  version: 4,
                  name: input.name,
                  value: {
                    type: 'Point',
                    coordinates: [
                      addressObject.geo_punt.coordinates[0],
                      addressObject.geo_punt.coordinates[1],
                    ],
                  },
                });
            };

            return ctrl;
          },
        ],
        controllerAs: 'zsCaseWebformAddress2Field',
        link: function (scope, element, attrs, controllers) {
          controllers[0].link(controllers.slice(1));
        },
      };
    },
  ]);
})();
