// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.case').directive('zsCaseWebformGeojsonField', [
    function () {
      return {
        scope: true,
        require: ['zsCaseWebformGeojsonField', '^zsCaseWebformField'],
        controller: [
          'smartHttp',
          '$element',
          function (smartHttp, $element) {
            var ctrl = this,
              parentController,
              config,
              input = $element[0].querySelector('input'),
              placeholder = $element[0].querySelector('.geojson-placeholder'),
              iframe = document.createElement('iframe');

            var sendMessage = function (message) {
              iframe.contentWindow.postMessage(message, '*');
            };

            try {
              angular.forEach(
                $element.parent().parent().parent().children(),
                function (el) {
                  el.className ===
                    'kenmerk-veld ezra_field_wrapper row ng-scope' &&
                    $(el).insertAfter(el.parentElement);
                }
              );
            } catch (err) {
              console.log(err);
            }

            smartHttp
              .connect({
                method: 'GET',
                url: '/api/v1/map/ol_settings',
              })
              .success(function (response) {
                var map_center = response.result.instance.map_center;
                var wms_layers = response.result.instance.wms_layers;
                var map_application_url =
                  response.result.instance.map_application_url;
                var map_application = response.result.instance.map_application;

                config = {
                  center: map_center.split(',').map(Number),
                  appUrl:
                    map_application === 'external'
                      ? map_application_url
                      : window.location.origin +
                        '/external-components/index.html?component=map',
                  wmsLayers: wms_layers
                    .filter(function (layer) {
                      return layer.instance.active;
                    })
                    .map(function (layer) {
                      return {
                        url: layer.instance.url,
                        layers: layer.instance.layer_name,
                      };
                    }),
                };
                ctrl.initMap();
              });

            ctrl.initMap = function () {
              iframe.src = config.appUrl;
              iframe.style.width = '100%';
              iframe.style.height = '450px';
              iframe.title = input.name;
              iframe.frameBorder = '0';
              iframe.allow = 'fullscreen';
              iframe.allowFullscreen = true;
              iframe.addEventListener('load', function () {
                sendMessage({
                  type: 'init',
                  name: input.name,
                  version: 4,
                  value: {
                    initialFeature: !input.value || JSON.parse(input.value),
                    center: config.center,
                    wmsLayers: config.wmsLayers,
                    canDrawFeatures: true,
                  },
                });
                window.top.addEventListener('message', ctrl.handleMessage);
              });
              placeholder.innerHtml = '';
              placeholder.appendChild(iframe);
            };

            ctrl.handleMessage = function (event) {
              if (event.data.type === 'featureChange') {
                input.value = JSON.stringify(event.data.value);
              }
            };

            ctrl.link = function (controllers) {
              parentController = controllers[0];
              parentController.setGetter(function () {
                return input.value;
              });
            };

            return ctrl;
          },
        ],
        controllerAs: 'zsCaseWebformGeojsonField',
        link: function (scope, element, attrs, controllers) {
          controllers[0].link(controllers.slice(1));
        },
      };
    },
  ]);
})();
