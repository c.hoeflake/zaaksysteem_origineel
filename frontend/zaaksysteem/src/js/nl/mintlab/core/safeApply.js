// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.core').factory('safeApply', [
    function () {
      var safeApply = window.zsFetch('nl.mintlab.utils.safeApply');

      return safeApply;
    },
  ]);
})();
