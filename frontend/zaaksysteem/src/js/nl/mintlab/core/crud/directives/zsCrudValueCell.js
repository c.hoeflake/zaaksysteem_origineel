// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem').directive('zsCrudValueCell', [
    '$compile',
    'templateCompiler',
    function ($compile, templateCompiler) {
      return {
        require: ['^zsCrudTemplateParser'],
        link: function (scope, element, attrs, controllers) {
          var zsCrud = controllers[0],
            column = scope.column,
            compiler;

          function initCompiler() {
            var template;
            if (column.templateUrl) {
              templateCompiler
                .getCompiler(column.templateUrl)
                .then(function (cpl) {
                  setCompiler(cpl);
                });
            } else {
              template = column.template || '<[getColumnValue(column,item)]>';
              setCompiler($compile(template));
            }
          }

          function setCompiler(cpl) {
            compiler = cpl;
            recompile();
          }

          function recompile() {
            if (column.dynamic) {
              compiler(scope, function (el /*, scope*/) {
                element.append(el);
              });
            } else {
              compiler(scope.$new(), function (el, compileScope) {
                setTimeout(function () {
                  element[0].innerHTML = el[0].outerHTML;
                  compileScope.$destroy();
                });
              });
            }
          }

          function onItemChange() {
            recompile();
          }

          initCompiler();

          if (!column.dynamic) {
            zsCrud.itemChangeListeners.push(onItemChange);

            scope.$on('$destroy', function () {
              _.pull(zsCrud.itemChangeListeners, onItemChange);
            });
          }
        },
      };
    },
  ]);
})();
