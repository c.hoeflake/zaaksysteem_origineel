// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.admin.instances')
    .directive('zsInstancesCustomerHostList', [
      'zsApi',
      'systemMessageService',
      function (api, systemMessageService) {
        return {
          restrict: 'E',
          replace: true,
          template:
            '<div class="summary-block summary-block-hosts">' +
            '<div class="summary-block-title summary-block-title-small">' +
            'Hosts' +
            '<button class="summary-block-title-edit-button button-secondary button button-smaller right" data-ng-show="instancesCustomerHostList.isEditable()" data-zs-popup="\'/html/admin/instances/instances.html#host-create\'" ng-click="openPopup()">' +
            '<i class="icon-font-awesome icon-plus icon-only"></i>' +
            '</button>' +
            '</div>' +
            '<div class="summary-block-content">' +
            '<div class="summary-row form-field form-field-small-text" data-ng-repeat="host in instancesCustomerHostList.getHosts()">' +
            '<div class="summary-row-content row row-double-actions">' +
            '<div class="summary-row-title row-title"><[host.instance.fqdn]></div>' +
            '<div class="summary-row-options row-actions row-actions-text-overflow" data-ng-show="instancesCustomerHostList.isEditable()">' +
            '<button data-zs-popup="\'/html/admin/instances/instances.html#host-edit\'" ng-click="openPopup()" class="row-action">' +
            '<i class="icon-font-awesome icon-only icon-pencil"></i>' +
            '</button>' +
            '<button data-zs-confirm="instancesCustomerHostList.deleteHost(host)" data-zs-confirm-label="Weet u zeker dat u deze host wil verwijderen?" data-zs-confirm-verb="Verwijder" class="row-action">' +
            '<i class="icon-font-awesome icon-only icon-remove"></i>' +
            '</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<span data-ng-show="!instancesCustomerHostList.getHosts().length" class="empty-row-placeholder empty-row-placeholder-small">Er zijn nog geen hosts toegevoegd</span>' +
            '</div>' +
            '</div>',
          scope: {
            customer: '&',
            instances: '&',
            onHostUpdate: '&',
            editable: '&',
          },
          controller: [
            '$scope',
            function ($scope) {
              var ctrl = this,
                instances = [],
                hosts = [];
              var controlpanelInstance;

              function getControlPanelId() {
                return $scope.customer().reference;
              }

              function triggerHostUpdate() {
                $scope.onHostUpdate({ $hosts: hosts });
              }

              function loadHosts() {
                api
                  .get({
                    url:
                      '/api/v1/controlpanel/' + getControlPanelId() + '/host',
                  })
                  .then(function (response) {
                    hosts = hosts.concat(response.data);

                    // this only returns two pages. fix it

                    if (response.pager && response.pager.hasNext()) {
                      return response.pager.next();
                    }

                    return hosts;
                  })
                  .then(function (response) {
                    if (response.data && response.data.length) {
                      hosts = hosts.concat(response.data);
                    }
                  })
                  ['catch'](function () {
                    systemMessageService.emitLoadError('hosts');
                  });
              }

              ctrl.getHosts = function () {
                return hosts;
              };

              ctrl.handleAddSubmit = function ($host) {
                hosts.push($host);

                return api
                  .post({
                    url:
                      '/api/v1/controlpanel/' +
                      getControlPanelId() +
                      '/host/create',
                    data: $host.instance,
                  })
                  .then(function (response) {
                    _.extend($host, response.data[0]);
                    systemMessageService.emitSave();

                    triggerHostUpdate();
                  })
                  ['catch'](function (err) {
                    _.pull(hosts, $host);
                    throw err;
                  });
              };

              ctrl.handleEditSubmit = function ($host) {
                var host = _.find(hosts, { reference: $host.reference }),
                  copy = angular.copy(host);

                _.extend(host, $host);

                return api
                  .post({
                    url:
                      '/api/v1/controlpanel/' +
                      getControlPanelId() +
                      '/host/' +
                      host.reference +
                      '/update',
                    data: $host.instance,
                  })
                  .then(function (response) {
                    _.extend(host, response.data[0]);

                    systemMessageService.emitSave();

                    triggerHostUpdate();
                  })
                  ['catch'](function (err) {
                    _.extend(host, copy);
                    throw err;
                  });
              };

              ctrl.deleteHost = function (host) {
                _.pull(hosts, host);

                api
                  .post({
                    url:
                      '/api/v1/controlpanel/' +
                      getControlPanelId() +
                      '/host/' +
                      host.reference +
                      '/delete',
                  })
                  .then(function () {
                    systemMessageService.emitSave();

                    triggerHostUpdate();
                  })
                  ['catch'](function () {
                    systemMessageService.emitSaveError();
                    hosts.push(host);
                  });
              };

              ctrl.getControlpanelInstance = function () {
                return controlpanelInstance;
              };

              ctrl.getInstances = function () {
                return instances;
              };

              ctrl.isEditable = $scope.editable;

              $scope.$watch($scope.instances, function (instancePromise) {
                if (instancePromise) {
                  instancePromise.then(function (inst) {
                    instances = inst;
                  });
                }
              });

              $scope.$watch($scope.customer, function (customer) {
                if (customer) {
                  controlpanelInstance = customer.instance;
                  loadHosts();
                }
              });
            },
          ],
          controllerAs: 'instancesCustomerHostList',
        };
      },
    ]);
})();
