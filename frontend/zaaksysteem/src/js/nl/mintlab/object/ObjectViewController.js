// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.object')
    .controller('nl.mintlab.object.ObjectViewController', [
      '$scope',
      function ($scope) {
        var ctrl = this,
          objectId,
          template,
          object;

        ctrl.init = function (id) {
          objectId = id;

          object = $scope.objectView.getObject();

          switch (object.type) {
            case 'product':
            case 'faq':
              template = '/html/object/view/pq.html';
              break;

            default:
              template = null;
              break;
          }
        };

        ctrl.getObjectTemplate = function () {
          return template;
        };

        $scope.getObject = function () {
          return object;
        };

        return ctrl;
      },
    ]);
})();
