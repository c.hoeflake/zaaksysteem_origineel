// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularSanitize from 'angular-sanitize';

export default angular
  .module('zsTruncate', [angularSanitize])
  .directive('zsTruncate', [
    '$sanitize',
    ($sanitize) => {
      return {
        restrict: 'A',
        link: (scope, element, attrs) => {
          attrs.$observe('zsTruncate', (value) => {
            let l =
                attrs.zsTruncateLength === undefined
                  ? 100
                  : Number(attrs.zsTruncateLength),
              val = value || '',
              html = $sanitize(
                l && val.length > l ? `${val.substr(0, l)}&hellip;` : val
              );

            element[0].innerHTML = html;

            element[0].title = val;
          });
        },
      };
    },
  ]).name;
