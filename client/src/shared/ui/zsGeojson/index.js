// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import get from 'lodash/get';
import './styles.scss';

const tryFunction = (x = '') => typeof x === 'function' ? x() : x;

export default angular.module('zsGeojson', []).directive('zsGeojson', [
  () => {
    return {
      restrict: 'E',
      template,
      scope: {
        mapName: '&',
        mapSettings: '&',
        mapHeight: '&',
        canDrawFeatures: '&',
        initialFeature: '&',
        handleChange: '&',
      },
      bindToController: true,
      controller: [
        '$element',
        function ($element) {
          let ctrl = this;
          const mapName = ctrl.mapName();
          const mapSettings = ctrl.mapSettings();
          const initialFeature = ctrl.initialFeature();
          const canDrawFeatures = ctrl.canDrawFeatures();
          const handleChange = ctrl.handleChange();
          const useGeojson = get(mapSettings, 'map_application') === 'external';
          const appUrl = useGeojson
            ? get(mapSettings, 'map_application_url')
            : `${window.location.origin}/external-components/index.html?component=map`;
          const mapCenter = get(mapSettings, 'map_center', '')
            .split(',')
            .map(Number);
          const wmsLayers = get(mapSettings, 'wms_layers', [])
            .filter(({ instance: { active } }) => active)
            .map(({ instance: { url, layer_name } }) => ({
              url,
              layers: layer_name,
            }));

          const handleMessage = (event) => {
            if (Boolean(canDrawFeatures) && get(event, 'data.name') === tryFunction(mapName)) {
              if (event.data.type === 'featureChange' && handleChange) {
                handleChange(event.data.value);
              }
            }
          };

          let iframe = document.createElement('iframe');

          iframe.src = appUrl;
          iframe.style.width = '100%';
          iframe.style.height = ctrl.mapHeight() || '100%';
          iframe.style.border = '1px solid #CCC';
          iframe.title = 'geojson';
          iframe.allow = 'fullscreen';
          iframe.addEventListener('load', function () {
            iframe.contentWindow.postMessage(
              {
                type: 'init',
                name: tryFunction(mapName),
                version: 4,
                value: {
                  initialFeature: tryFunction(initialFeature),
                  center: mapCenter,
                  wmsLayers,
                  canDrawFeatures,
                },
              },
              '*'
            );

            canDrawFeatures &&
              window.top.addEventListener('message', handleMessage);
          });

          $element.find('geojson-content').replaceWith(iframe);
        },
      ],
      controllerAs: 'vm',
    };
  },
]).name;
