// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import ruleEngine from '.';
import difference from 'lodash/difference';
import keys from 'lodash/keys';

describe('ruleEngine', () => {
  test('should return an object with the initial keys', () => {
    const result = ruleEngine([], {});

    expect(
      difference(keys(result), [
        'hidden',
        'disabled',
        'values',
        'hiddenByGroup',
        'hiddenByAttribute',
      ]).length
    ).toBe(0);
  });

  test('should execute the given rules', () => {
    const result = ruleEngine(
      [
        {
          conditions: {
            conditions: [],
          },
          then: [
            {
              type: 'set_value',
              data: {
                value: 'foo',
                attribute_name: 'foo',
              },
            },
          ],
        },
        {
          conditions: {
            conditions: [
              {
                validates_true: false,
              },
            ],
            type: 'and',
          },
          else: [
            {
              type: 'set_value',
              data: {
                value: 'bar',
                attribute_name: 'bar',
              },
            },
          ],
        },
      ],
      {}
    );

    expect(result.values.foo).toBe('foo');
    expect(result.values.bar).toBe('bar');
  });

  test('should overwrite the results of earlier actions with the last one', () => {
    const result = ruleEngine(
      [
        {
          conditions: {
            conditions: [],
          },
          then: [
            {
              type: 'set_value',
              data: {
                value: 'foo',
                attribute_name: 'foo',
              },
            },
          ],
        },
        {
          conditions: {
            conditions: [],
          },
          then: [
            {
              type: 'set_value',
              data: {
                value: 'bar',
                attribute_name: 'foo',
              },
            },
          ],
        },
      ],
      {}
    );

    expect(result.values.foo).toBe('bar');
  });

  test('should clear the values of hidden attributes', () => {
    const result = ruleEngine(
      [
        {
          conditions: {
            conditions: [],
          },
          then: [
            {
              type: 'hide_attribute',
              data: {
                attribute_name: 'foo',
              },
            },
          ],
        },
      ],
      {
        foo: 'bar',
      }
    );

    expect(result.values.foo).toBe(null);
  });
});
