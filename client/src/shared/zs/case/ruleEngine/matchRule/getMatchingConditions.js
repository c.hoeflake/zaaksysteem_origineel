// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import matchCondition from './matchCondition';

export default (conditions, values, hidden) => {
  return conditions.filter((condition) =>
    matchCondition(condition, values || {}, hidden || {})
  );
};
