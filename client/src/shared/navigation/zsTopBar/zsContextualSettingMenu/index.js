// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import controller from './../../../ui/zsDropdownMenu/controller';
import template from './../../../ui/zsDropdownMenu/template.html';
import angularUiRouter from 'angular-ui-router';
import viewRegistrarModule from './../../../util/route/viewRegistrar';
import getActiveStates from './../../../util/route/getActiveStates';
import find from 'lodash/find';

export default angular
  .module('zsContextualSettingMenu', [angularUiRouter, viewRegistrarModule])
  .directive('zsContextualSettingMenu', [
    '$rootScope',
    '$compile',
    '$document',
    '$animate',
    '$state',
    'viewRegistrar',
    ($rootScope, $compile, $document, $animate, $state, viewRegistrar) => {
      let wrapper = angular.element(`<div>${template}</div>`),
        containerTpl = wrapper.find('ul');

      containerTpl.removeAttr('ng-if');

      containerTpl = containerTpl[0].outerHTML;

      wrapper.find('ul').remove();
      wrapper.find('li').remove();

      return {
        template: wrapper[0].innerHTML,
        restrict: 'E',
        scope: {
          onClear: '&',
          onReset: '&',
        },
        bindToController: true,
        controller: [
          '$scope',
          '$element',
          function (scope, element) {
            let ctrl = this,
              positionOptions = {
                attachment: 'top right',
                target: 'bottom right',
                reference: element.find('button').eq(0),
                updateOn: 'default watch',
                offset: { x: 0, y: 0 },
              },
              content;

            ctrl.name = 'Actie';

            let destroyContent = () => {
              if (content) {
                content.scope().$destroy();

                $animate.leave(content);

                content = null;
              }
            };

            let setContent = () => {
              let states = getActiveStates($state.$current),
                routeWithSettings,
                isoScope = scope.$new();

              if (content) {
                destroyContent();
              }

              content = angular.element(containerTpl);

              routeWithSettings = find(
                states.concat().reverse(),
                (state) => state.settings !== undefined
              );

              if (routeWithSettings) {
                let view = viewRegistrar.getView(routeWithSettings.name),
                  viewController = view.controller();

                isoScope.viewController = viewController;

                isoScope.close = ctrl.closeMenu;

                content.append(angular.element(routeWithSettings.settings));
              }

              $compile(content)(isoScope);

              $animate.enter(content, element, element.find('button'));
            };

            ctrl.options = () => null;

            ctrl.buttonIcon = 'dots-vertical';

            ctrl.buttonStyle = () => {
              return {
                'top-bar-button': true,
              };
            };

            ctrl.onOpen = () => {
              setContent();
            };

            ctrl.onClose = () => {
              destroyContent();
            };

            ctrl.positionOptions = () => positionOptions;

            controller.call(ctrl, $document, scope, element, $animate);

            $rootScope.$on('$viewContentLoaded', () => {
              scope.$evalAsync(() => {
                if (ctrl.isMenuOpen()) {
                  setContent();
                }
              });
            });
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
