// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import includes from 'lodash/includes';

/**
 * Determine if a target array contains at
 * least one element of an array of values.
 *
 * @param {Array<string>} haystack
 *   The target array to search in.
 * @param {Array<string>} values
 *   An array of values to search for.
 * @return {boolean}
 */
export const containsOneOf = (haystack, values) =>
  values.some((item) => haystack.indexOf(item) >= 0);

export default ({ user, $state, auxiliaryRouteService }) => {
  const aboutStateName = auxiliaryRouteService.append($state.current, 'about');
  const versionStateName = auxiliaryRouteService.append(
    $state.current,
    'version'
  );
  const isUser = includes(user.capabilities, 'gebruiker');
  const isBehandelaar = isUser && includes(user.capabilities, 'dashboard');

  return [
    {
      name: 'user',
      children: [
        {
          name: 'dashboard',
          label: 'Dashboard',
          href: $state.href('home') || '/intern',
          icon: 'home',
          when: isBehandelaar,
        },
        {
          name: 'communication',
          label: 'Communicatie',
          href: '/intern/communication',
          icon: 'message',
          when: isBehandelaar && includes(user.capabilities, 'message_intake'),
        },
        {
          name: 'intake',
          label: 'Documentintake',
          href: '/zaak/intake?scope=documents',
          icon: 'file',
          when:
            isBehandelaar &&
            includes(user.capabilities, 'documenten_intake_subject'),
        },
        {
          name: 'search',
          label: 'Uitgebreid zoeken',
          href: '/search',
          icon: 'magnify',
          when: isBehandelaar && includes(user.capabilities, 'search'),
        },
        {
          name: 'search_contact',
          label: 'Contacten zoeken',
          href: '/betrokkene/search',
          icon: 'account-search',
          when: isBehandelaar && includes(user.capabilities, 'contact_search'),
        },
        {
          name: 'export',
          label: 'Exportbestanden',
          href: '/exportqueue',
          icon: 'file-cloud',
          when: isBehandelaar,
        },
      ],
    },
    {
      name: 'admin',
      children: [
        {
          name: 'admin',
          label: 'Beheren',
          href: '/admin/catalogus',
          icon: 'sync',
          when: containsOneOf(user.capabilities, [
            'admin',
            'useradmin',
            'beheer_zaaktype_admin',
          ]),
        },
      ],
    },
    {
      name: 'info',
      children: [
        {
          name: 'help',
          label: 'Help',
          href: 'https://help.zaaksysteem.nl',
          icon: 'help-circle',
          external: true,
          when: isBehandelaar,
        },
        {
          name: 'info',
          label: 'Over Zaaksysteem.nl',
          href: $state.href(aboutStateName) || '/intern/!over',
          icon: 'information',
          when: isBehandelaar,
        },
        {
          name: 'version',
          label: 'Nieuw in deze versie',
          href: $state.href(versionStateName) || '/intern/!version',
          icon: 'lightbulb-outline',
          class: '{ "version-unseen": vm.versionUnseen() } ',
          when: isBehandelaar,
        },
      ],
    },
  ];
};
