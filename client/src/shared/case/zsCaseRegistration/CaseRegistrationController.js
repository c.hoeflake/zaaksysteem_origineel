// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import assign from 'lodash/fp/assign';
import find from 'lodash/find';
import findIndex from 'lodash/findIndex';
import first from 'lodash/first';
import flatten from 'lodash/flatten';
import get from 'lodash/get';
import identity from 'lodash/identity';
import includes from 'lodash/includes';
import isEmpty from 'lodash/isEmpty';
import isEqual from 'lodash/isEqual';
import keyBy from 'lodash/keyBy';
import last from 'lodash/last';
import map from 'lodash/map';
import mapValues from 'lodash/mapValues';
import merge from 'lodash/merge';
import omit from 'lodash/omit';
import pickBy from 'lodash/pickBy';
import reject from 'lodash/reject';
import seamlessImmutable from 'seamless-immutable';
import attrToVorm from './../../zs/vorm/attrToVorm';
import defaultMessages from './../../vorm/util/vormValidator/messages';
import formatAttributeValues from './formatAttributeValues';
import parseAttributeValues from './parseAttributeValues';
import reduceButtons from './reduceButtons';
import reduceSteps from './reduceSteps';
import ruleEngine from './../../zs/case/ruleEngine/v1';
import vormValidateAttributes from './../../zs/case/validateAttributes/vormValidateAttributes';
import registerCase from './registerCase';

export default class CaseRegistrationController {
  static get $inject() {
    return [
      '$scope',
      '$timeout',
      '$document',
      '$state',
      '$http',
      'composedReducer',
      'registrationFormCompiler',
      'preventNavigation',
      'vormValidator',
      'snackbarService',
      'roleService',
    ];
  }

  constructor(
    scope,
    $timeout,
    $document,
    $state,
    $http,
    composedReducer,
    registrationFormCompiler,
    preventNavigation,
    vormValidator,
    snackbarService,
    roleService
  ) {
    const ctrl = this;

    ctrl.$onInit = () => {
      let values = seamlessImmutable(ctrl.values || {});
      let touched = seamlessImmutable({});
      let resetTouched = () => {
        touched = seamlessImmutable({});
      };
      let displayAllValidation = false;
      let collapsed = true;
      let submitting = false;
      let setSubmitting = (status) => {
        submitting = status;
      };
      let lastStep;

      const registrationPhaseReducer = composedReducer(
        { scope },
        () => ctrl.casetype
      ).reduce((casetype) => {
        let registrationPhase = first(casetype.instance.phases);
        let processedFields = registrationPhase.fields.map((field) => {
          // Since no magic_string is provided through backend for text_block attributes
          // (they are not first class citizens apparently) we need to stick a magic_string
          // value on it manually.
          if (field.type === 'text_block') {
            return seamlessImmutable(field).merge({
              magic_string: `text_block_${field.id}`,
              default_value: field.properties.text_content,
            });
          }

          return field;
        });

        return seamlessImmutable(registrationPhase).merge({
          fields: processedFields,
        });
      });

      const presetRequestorReducer = composedReducer(
        { scope },
        () => ctrl.casetype
      ).reduce((casetype) => get(casetype, 'instance.preset_client', null));

      const requestorReducer = composedReducer(
        { scope },
        presetRequestorReducer,
        () => ctrl.requestor
      ).reduce((presetRequestor, requestor) => {
        const isPresetRequestor =
          presetRequestor !== null &&
          presetRequestor.reference === requestor.reference;
        return requestor.merge({
          instance: requestor.instance.merge({
            preset_client: isPresetRequestor ? 'Ja' : 'Nee',
          }),
        });
      });

      const settingsReducer = composedReducer(
        { scope },
        () => ctrl.casetype
      ).reduce((casetype) => casetype.instance.settings);

      const casePropertiesReducer = composedReducer(
        { scope },
        () => ctrl.casetype
      ).reduce((casetype) => casetype.instance.properties);

      const fieldReducer = composedReducer(
        { scope },
        registrationPhaseReducer,
        () => ctrl.requestor
      ).reduce((phase, requestor) => {
        let idToNameMapping = mapValues(
          keyBy(phase.fields, 'id'),
          'magic_string'
        );

        return keyBy(
          seamlessImmutable(phase.fields)
            .asMutable({ deep: true })
            .map((attr) => {
              let field = attrToVorm(attr, {
                include: {
                  dateRange: idToNameMapping,
                },
              });

              if (field.template === 'calendar') {
                field = merge(field, {
                  data: {
                    provider: {
                      requestor: requestor.instance.old_subject_identifier,
                    },
                  },
                });
              } else if (field.template === 'text_block') {
                field = merge(field, {
                  name: `text_block_${field.id}`,
                  hideLabel: true,
                });
              } else if (field.type === 'geolatlon') {
                field = merge(field, {
                  data: {
                    onFeaturesSelect: (features) => {
                      let attributeId = get(
                        attr,
                        'properties.map_wms_feature_attribute_id'
                      );
                      let attribute = find(phase.fields, {
                        catalogue_id: attributeId,
                      });
                      let val;

                      if (attribute) {
                        if (features.length) {
                          val = last(features);
                        } else {
                          val = null;
                        }

                        ctrl.handleChange(attribute.magic_string, val);
                      }
                    },
                  },
                });
              } else if (field.type === 'appointment') {
                field = merge(field, {
                  data: {
                    provider: {
                      requestor: requestor.reference,
                      appointmentInterfaceUuid:
                        attr.properties.appointment_interface_uuid,
                      locationId: attr.properties.location_id,
                      productId: attr.properties.product_id,
                    },
                  },
                });
              } else if (field.type === 'geojson') {
                field = merge(field, {
                  data: {
                    mapSettings: ctrl.mapSettings
                  },
                });
              } else if (field.data && field.data.options) {
                field = assign(field, {
                  data: assign(field.data, {
                    options: field.data.options.filter((opt) => opt.active),
                  }),
                });
              }

              return field;
            })
            .map((field) => {
              let mapped = field;

              switch (field.$attribute.type) {
                case 'file':
                  mapped = merge(field, {
                    data: {
                      target: '/api/v1/case/prepare_file',
                      transform: [
                        '$file',
                        '$data',
                        ($file, $data) => {
                          let files = map(
                            $data.result.instance.references,
                            (filename, reference) => ({
                              filename,
                              reference,
                            })
                          );

                          return files[0];
                        },
                      ],
                    },
                  });
                  break;
              }

              return mapped;
            }),
          'name'
        );
      });

      // values can only be set once, so no need to wrap it in a getter
      const defaultsReducer = composedReducer(
        { scope },
        settingsReducer,
        fieldReducer,
        registrationPhaseReducer,
        () => ctrl.requestor,
        () => ctrl.documents
      ).reduce((settings, fields, registrationPhase, requestor, documents) => {
        let allocation;
        let email;
        let mobile;
        let landline;

        let defaults = seamlessImmutable({}).merge(
          parseAttributeValues(
            fields,
            mapValues(
              keyBy(
                registrationPhase.fields.filter(
                  (field) => !isEmpty(field.default_value)
                ),
                'magic_string'
              ),
              'default_value'
            )
          )
        );

        if (settings.allow_take_on_create) {
          allocation = {
            type: 'me',
            data: {
              me: true,
            },
          };
        } else if (settings.allow_assign_on_create) {
          allocation = {
            type: 'org-unit',
            data: {
              unit: String(registrationPhase.route.group.instance.group_id),
              role: String(registrationPhase.route.role.instance.role_id),
            },
          };
        }

        if (settings.intake_show_contact_info && requestor) {
          email = get(requestor, 'instance.subject.instance.email_address');
          mobile = get(
            requestor,
            'instance.subject.instance.mobile_phone_number'
          );
          landline = get(requestor, 'instance.subject.instance.phone_number');
        }

        if (documents) {
          defaults = defaults.merge({
            $files: documents.map((file) => {
              let originDate = get(file, 'metadata_id.origin_date', null);

              if (originDate) {
                originDate = new Date(
                  ...originDate.split('-').map((pt, index) => {
                    // month is zero-indexed
                    return index === 1 ? pt - 1 : pt;
                  })
                );
              }

              return {
                reference: file.filestore_id.uuid,
                name: file.name,
                extension: file.extension,
                origin: get(file, 'metadata_id.origin', 'Inkomend'),
                origin_date: originDate,
                description: get(file, 'metadata_id.description', ''),
              };
            }),
          });
        }

        defaults = defaults.merge({
          $allocation: allocation,
          $email: email,
          $mobile: mobile,
          $landline: landline,
          $confidentiality: 'public',
        });

        return defaults;
      });

      const valueReducer = composedReducer({ scope }, () => values).reduce(
        (vals) => vals
      );

      const ruleResultReducer = composedReducer(
        { scope },
        fieldReducer,
        defaultsReducer,
        valueReducer,
        () => ctrl.rules,
        () => ctrl.channelOfContact,
        requestorReducer,
        casePropertiesReducer
      ).reduce(
        (
          fieldsByName,
          defaults,
          vals,
          rules,
          channelOfContact,
          requestor,
          casetypeProperties
        ) => {
          let attrValues = parseAttributeValues(
            fieldsByName,
            defaults.merge(vals)
          );

          return ruleEngine(rules, {
            attributes: attrValues,
            channel_of_contact: channelOfContact,
            requestor,
            casetypeProperties,
          });
        }
      );

      const defaultAllocationReducer = composedReducer(
        { scope },
        ruleResultReducer,
        registrationPhaseReducer
      ).reduce((ruleResult, registrationPhase) => ({
        type: 'org-unit',
        data: ruleResult.allocation
          ? ruleResult.allocation
          : {
              unit: String(registrationPhase.route.group.instance.group_id),
              role: String(registrationPhase.route.role.instance.role_id),
            },
      }));

      composedReducer(
        {
          scope,
          mode: 'hot',
        },
        fieldReducer,
        ruleResultReducer
      )
        .reduce((fieldsByName, ruleResult) => {
          let prefill = formatAttributeValues(fieldsByName, ruleResult.prefill);

          return ruleResult.allocation
            ? assign(prefill, {
                $allocation: {
                  type: 'org-unit',
                  data: ruleResult.allocation,
                },
              })
            : prefill;
        })
        .subscribe((prefill, previous = {}) => {
          // If there is a difference between the prefilled values object
          // and the previously state of the values object AND the currently
          // calculated prefilled values object is bigger than the previous
          // state, then update values object.
          if (
            !isEqual(prefill, previous) &&
            Object.keys(prefill).length > Object.keys(previous).length
          ) {
            values = values.merge(
              pickBy(
                assign(prefill, previous),
                (value, name) => prefill[name] !== previous[name]
              )
            );
          }
        });

      const finalValueReducer = composedReducer(
        { scope },
        fieldReducer,
        valueReducer,
        ruleResultReducer,
        defaultsReducer
      ).reduce((fieldsByName, vals, ruleResult, defaults) => {
        const resultValues = pickBy(ruleResult.values, (value, key) =>
          Boolean(ruleResult.disabled[key])
        );

        const merged = defaults.merge(vals).merge(
          formatAttributeValues(
            fieldsByName,
            pickBy(ruleResult.values, (value, key) => {
              return !!ruleResult.disabled[key];
            })
          )
        );

        map(resultValues, (value, key) => {
          values = values.merge({ [key]: value });
        });

        return merged;
      });

      const stepsReducer = composedReducer(
        { scope },
        registrationPhaseReducer,
        settingsReducer,
        fieldReducer,
        requestorReducer,
        () => ruleResultReducer.data().hidden,
        () => ruleResultReducer.data().disabled,
        () => ruleResultReducer.data().pause_application,
        () => ctrl.step,
        () => lastStep,
        () => ctrl.documents
      ).reduce((...rest) => reduceSteps(...rest, $state));

      const currentGroupReducer = composedReducer(
        { scope },
        stepsReducer,
        () => ctrl.step
      ).reduce((steps, selectedStep) => {
        let group = selectedStep
          ? steps.filter((step) => step.name === selectedStep)[0]
          : steps[0];

        return group;
      });

      const validityReducer = composedReducer(
        { scope },
        currentGroupReducer,
        finalValueReducer
      ).reduce((group, vals) => {
        let messages = assign(defaultMessages, {
          telephone:
            'Er is geen geldig telefoonnummer ingevuld (10 t/m 15 cijfers en + is toegestaan)',
        });

        let validation = vormValidateAttributes(
          vormValidator,
          flatten(map(group.fieldsets, 'fields')),
          vals,
          messages,
          null,
          (field, value, v) => {
            let empty = isEmpty(value);

            switch (field.name) {
              case '$landline':
              case '$mobile':
                if (!empty && !/^\+?\d{10,15}$/.test(value)) {
                  v.telephone = messages.telephone || true;
                }

                break;
            }

            return v;
          }
        );

        return validation;
      });

      const displayedValidityReducer = composedReducer(
        { scope },
        validityReducer,
        () => touched,
        () => displayAllValidation
      ).reduce((validity, touchedFields, displayValidation) => {
        let validations = validity.validations;

        if (!displayValidation) {
          validations = mapValues(validations, (validationList, name) => {
            return !touched[name]
              ? validationList.map((validation) => {
                  return omit(validation, 'required');
                })
              : validationList;
          });
        }

        return validations;
      });

      const buttonReducer = composedReducer(
        { scope },
        stepsReducer,
        currentGroupReducer,
        () => submitting
      ).reduce((steps, currentGroup, isSubmitting) =>
        reduceButtons(steps, currentGroup).map((button) => {
          return isSubmitting ? assign(button, { disabled: true }) : button;
        })
      );

      const detailFieldReducer = composedReducer(
        { scope },
        () => ctrl.requestor,
        () => ctrl.recipient,
        () => ctrl.channelOfContact,
        () => ctrl.documents
      ).reduce((requestor, recipient, channelOfContact, documents) =>
        [
          {
            name: 'requestor',
            label: 'Aanvrager',
            value: get(requestor, 'instance.display_name', 'Geen'),
          },
          {
            name: 'recipient',
            label: 'Ontvanger',
            value: get(recipient, 'instance.display_name', 'Geen'),
          },
          {
            name: 'channel_of_contact',
            label: 'Contactkanaal',
            value: channelOfContact,
          },
          get(documents, 'length')
            ? {
                name: 'document',
                // leave singular for now, once multiple documents are supported, change to plural
                label: 'Document',
                value: documents
                  .map((document) => `${document.name}${document.extension}`)
                  .join(', '),
              }
            : null,
        ].filter(identity)
      );

      const messagesReducer = composedReducer(
        { scope },
        finalValueReducer,
        () => ctrl.step
      ).reduce((vals, step) => {
        let messages;

        messages =
          step === 'bevestig'
            ? map(vals.$files, (file) => {
                return get(vals[file.case_document], 'reference') ===
                  file.reference
                  ? null
                  : {
                      icon: 'alert-circle',
                      type: 'warning',
                      name: file.reference,
                      label: `Let op: het bestand ${file.name} is niet gekoppeld aan een zaakdocument. Het komt wel in de documentenlijst terecht.`,
                    };
              }).filter(identity)
            : [];

        return messages;
      });

      const getCurrentIndex = () => {
        return findIndex(
          stepsReducer.data(),
          (group) => group.name === currentGroupReducer.data().name
        );
      };

      ctrl.loadPreviousValues = (caseValues) => {
        if (caseValues) {
          values = values.merge(caseValues);

          $timeout(() => {
            snackbarService.info(
              'De waarden van de vorige zaak zijn ingevuld. U kunt nu wijzingen aanbrengen.'
            );
          }, 2500);
        }
      };

      ctrl.handleChange = (name, value) => {
        let valueToMerge = value;

        if (
          name === '$allocation' &&
          get(valueToMerge, 'type') === 'org-unit' &&
          get(finalValueReducer.data(), '$allocation.type') !== 'org-unit'
        ) {
          valueToMerge = defaultAllocationReducer.data();
        }

        if (name === '$files') {
          values = values.merge(
            mapValues(
              keyBy(
                value.map((file) => {
                  return {
                    name: file.case_document,
                    file: {
                      filename: file.name + file.extension,
                      reference: file.reference,
                    },
                  };
                }),
                'name'
              ),
              'file'
            )
          );
        } else if (includes(map(values.$files, 'case_document'), name)) {
          values = values.merge({
            $files: values.$files.map((file) => {
              if (file.case_document === name) {
                return assign(file, { case_document: null });
              }

              return file;
            }),
          });
        }

        values = values.merge({ [name]: valueToMerge });
        touched = touched.merge({ [name]: true });
        // remove hidden fields from touched object
        touched = seamlessImmutable(
          pickBy(
            touched.asMutable(),
            (val, key) => !ruleResultReducer.data().hidden[key]
          )
        );

        // set last step to current to prevent issues w/ changed data and disappearing steps
        lastStep = ctrl.getCurrentGroup().name;
      };

      const allocationReducer = composedReducer(
        { scope },
        roleService.createResource(scope)
      ).reduce((roles) => roles);

      ctrl.submit = () => {
        const intakeShowContactInfo = settingsReducer.data()
          .intake_show_contact_info;
        const allocationData = allocationReducer.data();

        registerCase(
          $http,
          $state,
          ctrl,
          intakeShowContactInfo,
          allocationData,
          fieldReducer,
          snackbarService,
          resetTouched,
          setSubmitting
        );
      };

      ctrl.cancel = () => {
        $state.go('home');
      };

      ctrl.goToFirstInvalid = () => {
        let field;
        let validations = ctrl.getValidations();
        let el;

        displayAllValidation = true;

        field = find(
          flatten(map(ctrl.getCurrentGroup().fieldsets, 'fields')),
          (f) => {
            const validation = validations[f.name];

            return reject(validation, isEmpty).length > 0;
          }
        );

        el = $document[0].querySelector(`[data-name="${field.name}"]`);
        el.scrollIntoView();
      };

      ctrl.advance = () => {
        let index = getCurrentIndex();

        if (!ctrl.isValid() && !ctrl.skipRequired) {
          ctrl.goToFirstInvalid();
        } else if (index === ctrl.getSteps().length - 1) {
          ctrl.submit();
        } else {
          $state.go(
            $state.current.name,
            { step: ctrl.getSteps()[index + 1].name },
            { inherit: true }
          );
        }
      };

      ctrl.back = () => {
        const index = getCurrentIndex();

        if (index === 0) {
          ctrl.cancel();
        } else {
          $state.go(
            $state.current.name,
            { step: ctrl.getSteps()[index - 1].name },
            { inherit: true }
          );
        }
      };

      ctrl.handleButtonClick = (name) => {
        let el = $document[0].querySelector('[name="case-register-top"]');

        $timeout(() => {
          el.scrollIntoView();
        }, 0);

        displayAllValidation = false;

        if (name === 'next') {
          ctrl.advance();
        } else {
          ctrl.back();
        }
      };

      ctrl.getValues = finalValueReducer.data;

      ctrl.getAttrValues = composedReducer(
        { scope },
        fieldReducer,
        finalValueReducer
      ).reduce((fieldsByName, vals) => {
        return seamlessImmutable(
          pickBy(vals, (value, key) => !!fieldsByName[key])
        );
      }).data;

      ctrl.getSteps = stepsReducer.data;
      ctrl.getButtons = buttonReducer.data;
      ctrl.getCurrentGroup = currentGroupReducer.data;

      ctrl.getValidations = () => get(validityReducer.data(), 'validations');

      ctrl.getDisplayedValidations = displayedValidityReducer.data;
      ctrl.isValid = () => get(validityReducer.data(), 'valid', false);
      ctrl.canSkip = () =>
        includes(
          get(ctrl.user, 'capabilities'),
          'case_registration_allow_partial'
        ) || includes(get(ctrl.user, 'capabilities'), 'admin');

      ctrl.getDetailFields = detailFieldReducer.data;
      ctrl.getMessages = messagesReducer.data;

      ctrl.getDocumentUrl = () => {
        if (ctrl.documents) {
          return `/zaak/intake/${ctrl.documents[0].id}/download/pdf?inline=1`;
        }
      };

      ctrl.isPdfSupported = () => {
        const getActiveXObject = (name) => {
          try {
            return new window.ActiveXObject(name);
          } catch (e) {
            // allow failures
          }
        };

        const hasAcrobatInstalled =
          getActiveXObject('AcroPDF.PDF') || getActiveXObject('PDF.PdfCtrl');
        const isIos =
          /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

        return (
          navigator.mimeTypes['application/pdf'] ||
          hasAcrobatInstalled ||
          !isIos
        );
      };

      ctrl.isCollapsed = () => collapsed;

      ctrl.toggleCollapse = () => {
        collapsed = !collapsed;
      };

      ctrl.getFields = fieldReducer.data;
      ctrl.getPauseApplicationData = () => ctrl.getCurrentGroup().paused;

      preventNavigation(scope, (event, to, toParams) => {
        // ZS-TODO: fix this properly
        // toParams is undefined when refreshing the page while registering a new case
        // (in Internet Explorer that breaks execution in the debugger!)
        if (toParams === undefined) {
          return false;
        }

        let shouldPrevent =
          !isEmpty(touched.asMutable()) && !toParams.ignoreUnsavedChanges;

        return shouldPrevent;
      });

      currentGroupReducer.subscribe((currentGroup) => {
        // set last step to current step if greater than last step
        if (
          getCurrentIndex() >
          findIndex(ctrl.getSteps(), (group) => group.name === lastStep)
        ) {
          lastStep = currentGroup.name;
        }
      });
    };

    ctrl.getCompiler = () => registrationFormCompiler;
  }
}
