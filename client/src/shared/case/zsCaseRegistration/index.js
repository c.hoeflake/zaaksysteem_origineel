// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';

import appointmentModule from './../../vorm/types/appointment';
import calendarModule from './../../vorm/types/calendar';
import checkboxListModule from './../../vorm/types/checkboxList';
import geojsonModule from './../../vorm/types/geojson';
import composedReducerModule from './../../api/resource/composedReducer';
import formModule from './../../vorm/types/form';
import inputModule from './../../vorm/types/input';
import mapModule from './../../vorm/types/map';
import preventNavigationModule from './../../util/preventNavigation';
import radioModule from './../../vorm/types/radio';
import registrationFormCompilerModule from './registrationFormCompiler';
import richTextModule from './../../vorm/types/richText';
import selectModule from './../../vorm/types/select';
import snackbarServiceModule from './../../ui/zsSnackbar/snackbarService';
import textareaModule from './../../vorm/types/textarea';
import textblockModule from './../../vorm/types/textblock';
import vormAllocationPickerModule from './../../zs/vorm/vormAllocationPicker';
import vormFieldsetModule from './../../vorm/vormFieldset';
import vormObjectSuggestModule from './../../object/vormObjectSuggest';
import vormValidatorModule from './../../vorm/util/vormValidator';
import zsCasePauseApplicationModule from './../zsCasePauseApplication';
import zsCaseReuseValuesModule from './../zsCaseReuseValues';
import zsScrollFadeModule from './../../ui/zsScrollFade';
import zsPdfViewerModule from './../../../shared/ui/zsPdfViewer';
import zsGeojson from '../../../shared/ui/zsGeojson';

import controller from './CaseRegistrationController';
import template from './template.html';
import './styles.scss';

export default angular
  .module('zsCaseRegistration', [
    composedReducerModule,
    angularUiRouterModule,
    registrationFormCompilerModule,
    vormFieldsetModule,
    vormAllocationPickerModule,
    preventNavigationModule,
    zsCasePauseApplicationModule,
    zsCaseReuseValuesModule,
    vormValidatorModule,
    snackbarServiceModule,
    zsScrollFadeModule,
    inputModule,
    checkboxListModule,
    geojsonModule,
    radioModule,
    selectModule,
    textareaModule,
    richTextModule,
    mapModule,
    calendarModule,
    textblockModule,
    appointmentModule,
    vormObjectSuggestModule,
    zsPdfViewerModule,
    formModule,
    zsGeojson,
  ])
  .component('zsCaseRegistration', {
    bindings: {
      casetype: '<',
      casetypeV2: '<',
      recipient: '<',
      requestor: '<',
      channelOfContact: '<',
      rules: '<',
      step: '<',
      values: '<',
      user: '<',
      documents: '<',
      mapSettings: '<'
    },
    controller,
    template,
  }).name;
