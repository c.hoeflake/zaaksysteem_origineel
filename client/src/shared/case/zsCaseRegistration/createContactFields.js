// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
export default () => {
  return [
    {
      name: '$email',
      label: 'E-mailadres',
      template: 'email',
      description: 'E-mailadres van aanvrager',
    },
    {
      name: '$landline',
      label: 'Telefoonnummer',
      template: 'text',
      description: 'Telefoonnummer van aanvrager',
    },
    {
      name: '$mobile',
      label: 'Telefoonnummer (mobiel)',
      template: 'text',
      description: 'Mobiel nummer van aanvrager',
    },
  ];
};
