// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default [
  { value: 28, label: '4 weken' },
  { value: 42, label: '6 weken' },
  { value: 93, label: '3 maanden' },
  { value: 186, label: '6 maanden' },
  { value: 279, label: '9 maanden' },
  { value: 365, label: '1 jaar' },
  { value: 548, label: '1,5 jaar' },
  { value: 730, label: '2 jaar' },
  { value: 1095, label: '3 jaar' },
  { value: 1460, label: '4 jaar' },
  { value: 1825, label: '5 jaar' },
  { value: 2190, label: '6 jaar' },
  { value: 2555, label: '7 jaar' },
  { value: 2920, label: '8 jaar' },
  { value: 3285, label: '9 jaar' },
  { value: 3650, label: '10 jaar' },
  { value: 4015, label: '11 jaar' },
  { value: 4380, label: '12 jaar' },
  { value: 4745, label: '13 jaar' },
  { value: 5110, label: '14 jaar' },
  { value: 5475, label: '15 jaar' },
  { value: 5840, label: '16 jaar' },
  { value: 6935, label: '19 jaar' },
  { value: 7300, label: '20 jaar' },
  { value: 7665, label: '21 jaar' },
  { value: 10950, label: '30 jaar' },
  { value: 14600, label: '40 jaar' },
  { value: 18250, label: '50 jaar' },
  { value: 40150, label: '110 jaar' },
  { value: 99999, label: 'Bewaren' },
];
