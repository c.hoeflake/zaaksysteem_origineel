// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouter from 'angular-ui-router';
import stateRegistrarModule from './../stateRegistrar';
import assign from 'lodash/assign';
import last from 'lodash/last';
import initial from 'lodash/initial';

const AUX_SEPARATOR = '!';

export default angular
  .module('auxiliaryRoute', [angularUiRouter, stateRegistrarModule])
  .config([
    '$stateProvider',
    ($stateProvider) => {
      let orig = $stateProvider.state,
        auxStates = [];

      let getParentName = (name) => {
        let ancestors = name.split('.');

        ancestors.pop();

        return ancestors.join('.');
      };

      let initAuxState = (name, opts) => {
        auxStates = auxStates.concat(assign({ name }, opts));
      };

      $stateProvider.state = (...rest) => {
        let [name, opts] = rest;

        if (!opts) {
          opts = name;
          name = opts.name;
        }

        if (opts.auxiliary) {
          initAuxState(name, opts);
          return $stateProvider;
        }

        orig(...rest);

        return $stateProvider;
      };

      let origGet = $stateProvider.$get;

      $stateProvider.$get = function (...rest) {
        let registrar = last(rest),
          states = registrar.getStates(),
          $state;

        let getParent = (name) => {
          return registrar.getState(getParentName(name));
        };

        auxStates.forEach((state) => {
          let descendants,
            ownName = last(state.name.split('.')),
            parent = getParent(state.name);

          descendants = states.filter((st) => {
            let p = getParent(st.self.name);

            while (p && p !== parent) {
              p = getParent(p.self.name);
            }

            return parent ? p : p === parent;
          });

          if (parent) {
            descendants = [parent].concat(descendants);
          }

          // skip root route
          descendants = descendants.filter((descendant) => !!descendant.url);

          descendants.forEach((descendant) => {
            let childState = assign({}, state, {
              name: `${descendant.name}.${ownName}`,
              url: state.url.replace(/^\//, `/${AUX_SEPARATOR}`),
              parent: descendant.name,
            });

            orig(childState);
          });
        });

        $state = origGet(...initial(rest));

        return $state;
      };

      $stateProvider.$get.$inject = origGet.$inject.concat('stateRegistrar');
    },
  ])
  .factory('auxiliaryRouteService', [
    '$rootScope',
    '$location',
    ($rootScope, $location) => {
      let currentBase;

      let parse = (url) => {
        let aux,
          base = url;

        if (url.indexOf(AUX_SEPARATOR) !== -1) {
          let parts = url.split(AUX_SEPARATOR);

          aux = parts.pop();
          base = parts.join(AUX_SEPARATOR);
        }

        return {
          aux,
          base,
        };
      };

      let parseNames = (name, url) => {
        let parsed = parse(url),
          aux = null,
          base = null;

        if (parsed.aux) {
          let split = name.split('.');

          aux = last(split);
          base = initial(split).join('.');
        } else {
          base = name;
        }

        return { aux, base };
      };

      let state = (target, name, url) => {
        let names = parseNames(name, url),
          stateName = target;

        if (names.aux) {
          stateName += `.${names.aux}`;
        }

        return stateName;
      };

      let append = (stateObj, auxState) => {
        let stateName = stateObj.name;

        if (stateObj.auxiliary) {
          stateName = initial(stateName.split('.')).join('.');
        }

        return [stateName, last(auxState.split('.'))].join('.');
      };

      let getBase = (stateName) => {
        return parseNames(stateName, $location.url()).base;
      };

      $rootScope.$on('$stateChangeSuccess', (event, to) => {
        currentBase = getBase(to.name);
      });

      return {
        parse,
        parseNames,
        state,
        append,
        getCurrentBase: () => currentBase,
      };
    },
  ]).name;
