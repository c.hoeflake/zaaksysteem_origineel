// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
const getYearMonthNumber = (date) =>
  date.getFullYear() * 100 + (date.getMonth() + 1);

export default getYearMonthNumber;
