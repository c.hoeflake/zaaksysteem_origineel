// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
const isSame = (a, b) => JSON.stringify(a) === JSON.stringify(b);

export default (resource, done = () => {}, interval = 1000) => {
  resource.reload();
  const prev1 = resource.state();
  setTimeout(() => {
    const prev2 = resource.state();
    isSame(prev1, prev2)
      ? setTimeout(() => {
          const prev3 = resource.state();
          isSame(prev2, prev3)
            ? setTimeout(() => {
                const prev4 = resource.state();
                isSame(prev4, prev3)
                  ? setTimeout(() => {
                      resource.reload();
                      done();
                    }, interval)
                  : done();
              }, interval)
            : done();
        }, interval)
      : done();
  }, interval);
};
