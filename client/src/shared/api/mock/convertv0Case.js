// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import mapKeys from 'lodash/mapKeys';
import mapValues from 'lodash/mapValues';
import pickBy from 'lodash/pickBy';
import isArray from 'lodash/isArray';
import startsWith from 'lodash/startsWith';
import get from 'lodash/get';
import merge from 'lodash/merge';
import assign from 'lodash/assign';
import map from 'lodash/map';
import shortid from 'shortid';
import catv0Key from './catv0Key';

export default (caseObj) => {
  let convertedCase = {
    reference: caseObj.id,
    instance: assign(
      mapKeys(
        pickBy(caseObj.values, (value, key) => {
          return key.match(/^case\.([a-z_]+)$/);
        }),
        (value, key) => {
          return key.replace('case.', '');
        }
      ),
      {
        attributes: mapKeys(
          mapValues(
            pickBy(get(caseObj, 'values', {}), (value, key) =>
              startsWith(key, 'attribute.')
            ),
            (value /*, key*/) => {
              return isArray(value) ? value : [value];
            }
          ),
          (value, key) => key.replace('attribute.', '')
        ),
        actions: get(caseObj, 'case.case_actions.by_milestone', []),
        checklists: get(caseObj, 'case.checklist.by_milestone', []),
        // TODO: hacky way to get casetype UUID from legacy API call
        permissions: get(caseObj, 'case.permissions', []),
        case_documents: get(caseObj, 'case.case_documents', []),
        casetype: merge(
          {
            instance: {
              // eslint-disable-next-line quotes
              name: get(caseObj, "values['case.casetype.name']"),
            },
            reference: get(caseObj, 'case.class_uuid'),
            // eslint-disable-next-line quotes
            table_id: get(caseObj, "values['case.casetype.id']"),
            type: 'casetype',
            // eslint-disable-next-line quotes
            version: get(caseObj, "values['case.casetype.node.id']"),
          },
          catv0Key('casetype', caseObj.values)
        ),
        child_relations: {
          instance: {
            rows: [],
          },
          type: 'set',
        },
        id: shortid(),
        relations: {
          instance: {
            rows: map(caseObj.related_objects, (relation) => {
              return {
                reference: relation.related_object_id,
                type: relation.related_object_type,
                instance: get(relation.related_object, 'values', {}),
                label: get(relation.related_object, 'label', ''),
              };
            }),
          },
          type: 'set',
        },
        parent_relation: {},
        pending_changes: get(caseObj, 'case.pending_changes', {}),
        mutations: mapKeys(
          pickBy(caseObj.values, (value, key) => startsWith(key, 'object.')),
          (value, key) => key.replace('object.', '')
        ),
        requestor: catv0Key('requestor', caseObj.values),
        recipient: catv0Key('recipient', caseObj.values),
        assignee: catv0Key('assignee', caseObj.values),
        coordinator: catv0Key('coordinator', caseObj.values),
        location:
          catv0Key('case_location', caseObj.values, 'nummeraanduiding') ||
          catv0Key('case_location', caseObj.values, 'openbareruimte'),
      }
    ),
  };

  return convertedCase;
};
