// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import assign from 'lodash/assign';
import vormTemplateServiceModule from './../../vormTemplateService';
import vormInvokeModule from './../../vormInvoke';
import uniqueId from 'lodash/uniqueId';
import template from './template.html';

const getTabIndex = (selected, hasOptionSelected) => {
  if (!hasOptionSelected) {
    return 0;
  }

  return selected ? 0 : -1;
};

export default angular
  .module('vorm.types.radio', [vormTemplateServiceModule, vormInvokeModule])
  .directive('vormRadioGroup', [
    'vormInvoke',
    (vormInvoke) => {
      return {
        template,
        restrict: 'E',
        require: ['vormRadioGroup', 'ngModel'],
        scope: {
          templateData: '&',
          inputId: '&',
          tabindex: '&',
        },
        bindToController: true,
        controller: [
          function () {
            let ctrl = this,
              ngModel,
              name;

            ctrl.link = (controllers) => {
              [ngModel] = controllers;
              ctrl.value = ngModel.$modelValue;

              name = uniqueId('radio-');

              ngModel.$formatters.push((val) => {
                ctrl.value = val;
                return val;
              });
            };

            ctrl.getName = () => {
              return name;
            };

            ctrl.handleChange = () => {
              ngModel.$setViewValue(ctrl.value, 'click');
            };

            ctrl.getOptions = () => {
              let options = vormInvoke(ctrl.templateData().options).map(
                (option) =>
                  assign(option, {
                    selected: ctrl.value === option.value,
                    tabindex: getTabIndex(
                      ctrl.value === option.value,
                      ctrl.value !== null
                    ),
                    random: Math.random(),
                  })
              );

              return options;
            };

            ctrl.tabindex = (value) => {
              if (ctrl.value === null) {
                return 0;
              }

              return value === ctrl.value ? 0 : -1;
            };

            ctrl.readOnly = () => {
              let readOnly = vormInvoke(ctrl.templateData().readOnly);

              return readOnly;
            };
          },
        ],
        controllerAs: 'vm',
        link: (scope, element, attrs, controllers) => {
          controllers.shift().link(controllers);
        },
      };
    },
  ])
  .run([
    'vormTemplateService',
    function (vormTemplateService) {
      const el = angular.element(
        '<vorm-radio-group ng-model role="radiogroup" aria-labelledby="{{vm.getInputId()}}-label"></vorm-radio-group>'
      );

      vormTemplateService.registerType('radio', {
        control: el,
      });
    },
  ]).name;
