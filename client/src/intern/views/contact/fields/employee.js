// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { getValue } from './format';

const summary = true;

export default [
  {
    summary,
    key: 'first_names',
    label: 'Voornamen',
    getValue,
  },
  {
    summary,
    key: 'surname',
    label: 'Achternaam',
    getValue,
  },
];
