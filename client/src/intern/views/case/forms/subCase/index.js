// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import get from 'lodash/get';
import first from 'lodash/head';
import identity from 'lodash/identity';
import propCheck from './../../../../../shared/util/propCheck';

export default function getSubCase(data) {
  let capabilities = {
    expand: true,
  };
  let subjectOptions = [
    {
      value: 'natuurlijk_persoon',
      label: 'Burger',
    },
    {
      value: 'bedrijf',
      label: 'Organisatie',
    },
  ];
  let typeOptions = [
    {
      value: 'deelzaak',
      label: 'Deelzaak',
    },
    {
      value: 'gerelateerd',
      label: 'Gerelateerde zaak',
    },
  ];

  propCheck.throw(
    propCheck.shape({
      requestorName: propCheck.string.optional,
      phaseOptions: propCheck.array,
      isLastPhase: propCheck.bool,
    }),
    data
  );

  if (data.isLastPhase) {
    typeOptions = [
      {
        value: 'vervolgzaak_datum',
        label: 'Vervolgzaak (datum)',
      },
      {
        value: 'vervolgzaak',
        label: 'Vervolgzaak (periode)',
      },
    ].concat(typeOptions);
  }

  return {
    getDefaults() {
      return {
        subcase_requestor_type: 'aanvrager',
        requestor_type: 'natuurlijk_persoon',
        type: 'deelzaak',
        resolve_in_phase: get(first(data.phaseOptions), 'value'),
      };
    },

    getCapabilities() {
      return capabilities;
    },

    processChange(name, value, values) {
      let vals = values;

      if (name === 'requestor_type') {
        vals = vals.merge({ requestor: null });
      }

      return vals.merge({ [name]: value });
    },

    fields(roleOptions) {
      return [
        {
          name: 'allocation',
          label: 'Toewijzing',
          template: 'org-unit',
          required: true,
        },
        {
          name: 'subcase_requestor_type',
          label: 'Aanvrager deel-/vervolgzaak',
          template: 'select',
          data: {
            options: [
              {
                value: 'aanvrager',
                label: `Aanvrager van de huidige zaak (${data.requestorName})`,
              },
              {
                value: 'betrokkene',
                label: 'Betrokkene rol van de huidige zaak',
              },
              {
                value: 'anders',
                label: 'Andere aanvrager',
              },
              {
                value: 'behandelaar',
                label: 'Behandelaar van de huidige zaak',
              },
              {
                value: 'ontvanger',
                label: 'Ontvanger van de huidige zaak',
              },
            ],
          },
          required: true,
        },
        {
          name: 'subcase_requestor_role',
          label: 'Type betrokkene',
          template: 'select',
          data: {
            options: roleOptions,
          },
          when: [
            '$values',
            (values) => values.subcase_requestor_type === 'betrokkene',
          ],
        },
        {
          name: 'requestor_type',
          label: 'Andere aanvrager type',
          template: 'radio',
          data: {
            options: subjectOptions,
          },
          required: true,
          when: [
            '$values',
            (values) => values.subcase_requestor_type === 'anders',
          ],
        },
        {
          name: 'requestor',
          label: 'Andere aanvrager',
          template: 'object-suggest',
          data: {
            objectType: ['$values', (values) => values.requestor_type],
          },
          when: [
            '$values',
            (values) => values.subcase_requestor_type === 'anders',
          ],
        },
        {
          name: 'type',
          template: 'select',
          label: 'Soort',
          data: {
            options: typeOptions,
          },
          when: ['expanded', identity],
        },
        {
          name: 'resolve_in_phase',
          template: 'select',
          label: 'Afhandelen in fase',
          data: {
            options: data.phaseOptions,
          },
          required: true,
          when: [
            '$values',
            'expanded',
            (values, expanded) => expanded && values.type === 'deelzaak',
          ],
        },
        {
          name: 'copy_related_cases',
          template: 'checkbox',
          label: 'Gerelateerde zaken kopiëren',
          when: ['expanded', identity],
        },
        {
          name: 'copy_related_objects',
          template: 'checkbox',
          label: 'Gerelateerde objecten kopiëren',
          when: ['expanded', identity],
        },
        {
          name: 'copy_attributes',
          template: 'checkbox',
          label: 'Kenmerken kopiëren',
          when: ['expanded', identity],
        },
        {
          name: 'copy_selected_attributes',
          template: 'hidden',
          label: "foo",
          when: ['expanded', identity],
        },
        {
          name: 'copy_subject_role',
          template: 'checkbox',
          label: 'Betrokkenen kopiëren',
          when: ['expanded', identity],
        },
        {
          name: 'copy_roles',
          label: 'Betrokkenen met deze rollen kopiëren',
          template: 'checkbox-list',
          data: {
            options: roleOptions,
          },
          when: [
            '$values',
            'expanded',
            (values, expanded) => expanded && values.copy_subject_role,
          ],
        },
        {
          name: 'automatic_assignment',
          template: 'checkbox',
          label: 'Zaak automatisch in behandeling nemen',
          when: ['expanded', identity],
        },
        {
          name: 'start_date',
          template: 'date',
          label: 'Starten na',
          when: [
            '$values',
            'expanded',
            (vals, expanded) => expanded && vals.type === 'vervolgzaak_datum',
          ],
          required: true,
        },
        {
          name: 'start_after',
          template: {
            inherits: 'text',
            control: (el) =>
              angular.element(
                `<div>
								${el[0].outerHTML} dagen
							</div>`
              ),
          },
          label: 'Starten na',
          when: [
            '$values',
            'expanded',
            (vals, expanded) => expanded && vals.type === 'vervolgzaak',
          ],
          required: true,
        },
      ];
    },
  };
}
