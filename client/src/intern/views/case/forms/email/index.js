// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import messages from './../../../../../shared/vorm/util/vormValidator/messages';
import every from 'lodash/every';
import identity from 'lodash/identity';
import trim from 'lodash/trim';
import defaults from 'lodash/defaults';
import find from 'lodash/find';
import get from 'lodash/get';
import emailRegex from 'email-regex';
import propCheck from './../../../../../shared/util/propCheck';
import './styles.scss';

let exampleAddresses = 'voorbeeld@zaaksysteem.nl; voorbeeld@mintlab.nl',
  regex = emailRegex({ exact: true });

let validateEmail = (str, options) => {
  let emails = [str],
    opts = defaults({}, options, { multiple: false }),
    validation = null;

  if (opts.multiple) {
    emails = (str || '').split(/[;,]/g).map(trim).filter(identity);
  }

  if (options.required && !str) {
    validation = {
      required: messages.required,
    };
  } else if (str) {
    if (
      !every(emails, (email) => {
        return regex.test(email) || email.match(/^\[\[(.*)\]\]$/);
      })
    ) {
      validation = {
        email: 'Dit is geen geldig e-mailadres of magic string.',
      };
    }
  }

  return [validation];
};

export default (data, options) => {
  let opts = defaults({}, options, {
      showTemplateSelect: true,
    }),
    capabilities = {};

  propCheck.throw(
    propCheck.shape({
      templates: opts.showTemplateSelect
        ? propCheck.array
        : propCheck.any.optional,
      requestorName: propCheck.any.optional,
    }),
    data
  );

  return {
    getDefaults: () => {
      return { recipient_type: 'behandelaar' };
    },
    processChange: (name, value, values) => {
      const vals = { [name]: value };
      const templateId = name === 'template' ? value : values.template;
      const tpl = find(data.templates, { id: templateId });

      if (name === 'recipient_type') {
        if (value === 'aanvrager') {
          vals.recipient_address = values.requestor_address;
        } else if (value === 'overig') {
          vals.recipient_address = get(tpl, 'email');
        } else {
          vals.recipient_address = '';
        }
      }

      if (name === 'template') {
        vals.recipient_type = get(tpl, 'rcpt', '');
        vals.betrokkene_role = get(tpl, 'betrokkene_role', '');
        vals.recipient_address =
          vals.recipient_type === 'overig'
            ? get(tpl, 'email')
            : values.requestor_address;
        vals.case_documents = get(
          tpl,
          'bibliotheek_notificaties_id.attachments',
          []
        );
        vals.email_subject = get(
          tpl,
          'bibliotheek_notificaties_id.subject',
          ''
        );
        vals.email_content = get(
          tpl,
          'bibliotheek_notificaties_id.message',
          ''
        );
        vals.behandelaar =
          tpl && tpl.behandelaar
            ? {
                label: tpl.betrokkene_naam || tpl.behandelaar,
                data: {
                  id: tpl.behandelaar.split('-')[2],
                },
                type: tpl.behandelaar.split('-')[1],
              }
            : null;
      }

      return values.merge(vals);
    },
    getCapabilities: () => capabilities,
    fields: (roleOptions) => {
      return [
        {
          name: 'template',
          label: 'E-mailsjabloon',
          template: 'select',
          data: {
            options: (data.templates || []).map((tpl) => {
              return {
                value: tpl.id,
                label: tpl.bibliotheek_notificaties_id.label,
              };
            }),
            notSelectedLabel: 'Geen',
          },
          when: opts.showTemplateSelect,
          required: false,
        },
        {
          name: 'recipient_type',
          label: 'Type ontvanger',
          template: 'select',
          data: {
            options: [
              {
                value: 'behandelaar',
                label: 'Collega',
              },
              {
                value: 'aanvrager',
                label: `Aanvrager (${data.requestorName || 'Onbekend'})`,
              },
              {
                value: 'gemachtigde',
                label: 'Gemachtigde',
              },
              {
                value: 'betrokkene',
                label: 'Betrokkene',
              },
              {
                value: 'overig',
                label: 'Overig',
              },
            ].filter(
              (item) => !(data.presetRequestor && item.value === 'aanvrager')
            ),
          },
          required: true,
        },
        {
          name: 'betrokkene_role',
          label: 'Type betrokkene',
          template: 'select',
          data: {
            options: roleOptions,
          },
          when: ['$values', (values) => values.recipient_type === 'betrokkene'],
          required: true,
        },
        {
          name: 'recipient_unavailable',
          label: '',
          template: {
            inherits: 'text',
            control: () => {
              return angular.element(
                `<div class="no-recipient" ng-model>
									<span>Geen e-mailadres beschikbaar voor ${data.requestorName}</span>
									<a class="btn btn-flat" href="${data.requestorLink}">Bewerk</a>
								</div>`
              );
            },
          },
          when: [
            '$values',
            (values) =>
              values.recipient_type === 'aanvrager' &&
              !values.recipient_address,
          ],
          disabled: false,
          required: true,
        },
        {
          name: 'behandelaar',
          label: '',
          template: 'object-suggest',
          data: {
            placeholder: 'Zoek uw collega',
            objectType: 'medewerker',
          },
          when: [
            '$values',
            (values) => values.recipient_type === 'behandelaar',
          ],
          required: true,
          limit: false,
        },
        {
          name: 'recipient_address',
          label: '',
          template: 'text',
          data: {
            placeholder: 'E-mailadres',
          },
          when: ['$values', (values) => values.recipient_type === 'overig'],
          valid: [
            '$values',
            (values) =>
              validateEmail(values.recipient_address, {
                multiple: true,
                required: true,
              }),
          ],
        },
        {
          name: 'recipient_cc',
          label: '',
          template: 'text',
          data: {
            placeholder: `Cc ${exampleAddresses}`,
          },
          valid: [
            '$values',
            (values) =>
              validateEmail(values.recipient_cc, {
                multiple: true,
                required: false,
              }),
          ],
        },
        {
          name: 'recipient_bcc',
          label: '',
          template: 'text',
          data: {
            placeholder: `Bcc ${exampleAddresses}`,
          },
          valid: [
            '$values',
            (values) =>
              validateEmail(values.recipient_bcc, {
                multiple: true,
                required: false,
              }),
          ],
        },
        {
          name: 'email_subject',
          label: '',
          template: 'text',
          data: {
            placeholder: 'Onderwerp',
          },
          valid: ['$values', (values) => !!values.email_subject],
          required: true,
        },
        {
          name: 'email_content',
          label: '',
          template: 'textarea',
          data: {
            placeholder: 'Bericht',
          },
          valid: ['$values', (values) => !!values.email_content],
          required: true,
        },
        {
          name: 'case_documents',
          label: '',
          template: {
            inherits: 'text',
            display: () => {
              return angular.element(
                `<div>
									<zs-icon icon-type="attachment"></zs-icon>
									{{delegate.value.naam||delegate.value.name}}
								</div>`
              );
            },
          },
          when: [
            '$values',
            (values) => values.case_documents && values.case_documents.length,
          ],
          disabled: true,
        },
      ];
    },
  };
};
