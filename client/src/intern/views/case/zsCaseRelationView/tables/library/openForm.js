// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import seamlessImmutable from 'seamless-immutable';

const openForm = ($q, $compile, scope, zsModal, options) => {
  return $q((resolve, reject) => {
    let modalScope = scope.$new(true);
    let modal;

    modalScope.vm = {
      fields: options.form.fields,
      defaults: seamlessImmutable(options.form.defaults || {}).merge(
        options.defaults
      ),
      actions: [
        {
          type: 'submit',
          label: 'Opslaan',
          click: [
            '$values',
            (values) => {
              modal.hide();

              $q.when(resolve(values))
                .then(() => {
                  modalScope.$destroy();
                })
                .catch((err) => {
                  modalScope.show();

                  return $q.reject(err);
                });
            },
          ],
        },
      ],
      handleChange: (name, value, values) => {
        return options.form.processChange
          ? options.form.processChange(name, value, values)
          : values;
      },
    };

    modal = zsModal({
      title: options.title,
      el: $compile(
        `<vorm-form
          data-fields="vm.fields"
          data-defaults="vm.defaults"
          data-actions="vm.actions"
          on-change="vm.handleChange($name, $value, $values)"
        >
        </vorm-form>`
      )(modalScope),
    });

    modal.open();

    modal.onClose(() => {
      modalScope.$destroy();

      return false;
    });

    modalScope.$on('$destroy', () => {
      modal.close();

      reject();
    });
  });
};

export default openForm;
