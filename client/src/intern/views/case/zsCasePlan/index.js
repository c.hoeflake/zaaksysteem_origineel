// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import vormFieldsetModule from './../../../../shared/vorm/vormFieldset';
import vormValidatorModule from './../../../../shared/vorm/util/vormValidator';
import mutationServiceModule from './../../../../shared/api/resource/mutationService';
import actionsModule from './../zsCaseRelationView/actions';
import seamlessImmutable from 'seamless-immutable';
import get from 'lodash/get';
import form from './form';

export default angular
  .module('zsCasePlan', [
    vormFieldsetModule,
    vormValidatorModule,
    mutationServiceModule,
    actionsModule,
  ])
  .directive('zsCasePlan', [
    'mutationService',
    'vormValidator',
    (mutationService, vormValidator) => {
      return {
        restrict: 'E',
        template,
        scope: {
          caseId: '&',
          caseUuid: '&',
          onSubmit: '&',
        },
        bindToController: true,
        controller: [
          function () {
            let ctrl = this,
              fields,
              validity = { valid: false, validations: [] },
              values = seamlessImmutable(form.defaults);

            fields = form.fields;

            ctrl.getFields = () => fields;

            ctrl.getValues = () => values;

            ctrl.handleChange = (name, value) => {
              values = values.merge({ [name]: value });

              validity = vormValidator(fields, values);
            };

            ctrl.handleSubmit = () => {
              ctrl.onSubmit({
                $promise: mutationService
                  .add({
                    type: 'case/relation/scheduled_job/add',
                    request: {
                      url: `/api/case/${ctrl.caseId()}/planned-cases`,
                      params: {
                        // add default paging parameters to ensure mutations
                        // are picked up by resources
                        zapi_num_rows: 20,
                        zapi_page: 1,
                      },
                    },
                    data: {
                      job: 'CreateCase',
                      next_run: values.from.toISOString(),
                      interval_period: values.has_pattern
                        ? get(values, 'pattern.every.type')
                        : 'once',
                      interval_value: values.has_pattern
                        ? get(values, 'pattern.every.count')
                        : 1,
                      runs_left: values.has_pattern
                        ? get(values, 'pattern.repeat')
                        : 1,
                      copy_relations: values.copy_relations,
                      case_uuid: ctrl.caseUuid(),
                      casetype_uuid: values.casetype.id,
                    },
                  })
                  .asPromise(),
              });
            };

            ctrl.isSubmitDisabled = () => {
              return !validity.valid;
            };
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
