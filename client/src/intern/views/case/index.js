// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import zsCasePhaseViewModule from './zsCaseView/zsCasePhaseView';
import zsCaseAdminViewModule from './zsCaseView/zsCaseAdminView';
import zsCaseNavigationModule from './zsCaseView/zsCaseNavigation';
import zsCaseSummaryModule from './zsCaseView/zsCaseSummary';
import zsCaseSettingsModule from './zsCaseSettings';
import zsCaseMapViewModule from './zsCaseMapView';
import zsGeojson from '../../../shared/ui/zsGeojson';
import zsCaseFileUploadModule from './zsCaseFileUpload';
import zsCaseTemplateGenerateModule from './zsCaseTemplateGenerate';
import zsCaseAddSubjectModule from './zsCaseAddSubject';
import zsCaseSendEmailModule from './zsCaseSendEmail';
import zsCaseAboutViewModule from './zsCaseAboutView';
import zsCasePlanModule from './zsCasePlan';
import './styles.scss';

export default angular.module('Zaaksysteem.intern.case', [
  zsCasePhaseViewModule,
  zsCaseAdminViewModule,
  zsCaseNavigationModule,
  zsCaseSummaryModule,
  zsCaseSettingsModule,
  zsCaseMapViewModule,
  zsGeojson,
  zsCaseFileUploadModule,
  zsCaseTemplateGenerateModule,
  zsCaseAddSubjectModule,
  zsCaseSendEmailModule,
  zsCaseAboutViewModule,
  zsCasePlanModule,
]).name;
