// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import vormFieldsetModule from './../../../../../../../../shared/vorm/vormFieldset';
import vormValidatorModule from './../../../../../../../../shared/vorm/util/vormValidator';
import resourceModule from './../../../../../../../../shared/api/resource';
import composedReducerModule from './../../../../../../../../shared/api/resource/composedReducer';
import zsIconModule from './../../../../../../../../shared/ui/zsIcon';
import zsEmailPreviewModule from './../../../../../../../../shared/ui/zsEmailPreview';
import controller from './CaseActionFormController';
import template from './template.html';

export default angular
  .module('zsCaseActionForm', [
    vormFieldsetModule,
    vormValidatorModule,
    composedReducerModule,
    resourceModule,
    zsIconModule,
    zsEmailPreviewModule,
  ])
  .component('zsCaseActionForm', {
    bindings: {
      caseId: '&',
      action: '&',
      templates: '&',
      caseDocuments: '&',
      requestor: '&',
      phases: '&',
      formNote: '&',
      onActionSave: '&',
      onActionExecute: '&',
    },
    controller,
    template,
  }).name;
