// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import composedReducerModule from './../../../../../../shared/api/resource/composedReducer';
import capitalize from 'lodash/capitalize';
import shortid from 'shortid';
import find from 'lodash/find';
import termOptions from './../../../../../../shared/case/termOptions';
import './styles.scss';

export default angular
  .module('zsCaseResult', [composedReducerModule])
  .directive('zsCaseResult', [
    'composedReducer',
    (composedReducer) => {
      return {
        restrict: 'E',
        template,
        scope: {
          results: '&',
          result: '&',
          onResultChange: '&',
          canChange: '&',
        },
        bindToController: true,
        controller: [
          '$scope',
          function (scope) {
            let ctrl = this;

            ctrl.getResultOptions = composedReducer(
              { scope },
              ctrl.result,
              ctrl.results
            ).reduce((result, results) => {
              return (results || []).map((resultOption) => {
                const help = [
                  {
                    label: 'Resultaattype-generiek',
                    value: capitalize(resultOption.type),
                  },
                  {
                    label: 'Selectielijst',
                    value: resultOption.selection_list,
                  },
                  {
                    label: 'Activeer bewaartermijn',
                    value: resultOption.trigger_archival ? 'Ja' : 'Nee',
                  },
                  {
                    label: 'Procestype-nummer',
                    value: resultOption.process_type_number,
                  },
                  {
                    label: 'Procestype-naam',
                    value: resultOption.process_type_name,
                  },
                  {
                    label: 'Procestype-object',
                    value: resultOption.process_type_object,
                  },
                  {
                    label: 'Herkomst',
                    value: resultOption.origin,
                  },
                  {
                    label: 'Archiefnominatie (waardering)',
                    value: resultOption.type_of_archiving,
                  },
                  {
                    label: 'Procestermijn',
                    value: resultOption.process_term,
                  },
                  {
                    label: 'Bewaartermijn',
                    value: find(termOptions, {
                      value: resultOption.period_of_preservation,
                    }).label,
                  },
                ];

                return {
                  id: shortid(),
                  value: resultOption.resultaat_id,
                  label: resultOption.label || capitalize(resultOption.type),
                  selected: result === resultOption.resultaat_id,
                  help: help
                    .map(
                      (row) =>
                        `<b>${row.label}</b>: ${
                          row.value || row.value === 0 ? row.value : '-'
                        }`
                    )
                    .join('<br/>'),
                };
              });
            }).data;

            ctrl.hasResult = () => !!ctrl.result();

            ctrl.clearResult = () => {
              ctrl.onResultChange({ $result: null });
            };

            ctrl.handleResultChange = (result) => {
              ctrl.onResultChange({ $result: result });
            };
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
