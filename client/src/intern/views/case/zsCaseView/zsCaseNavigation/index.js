// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import auxiliaryRouteModule from './../../../../../shared/util/route/auxiliaryRoute';
import composedReducerModule from './../../../../../shared/api/resource/composedReducer';
import vormInvokeModule from './../../../../../shared/vorm/vormInvoke';
import zsTooltipModule from './../../../../../shared/ui/zsTooltip';
import mapValues from 'lodash/mapValues';
import get from 'lodash/get';
import startsWith from 'lodash/startsWith';
import assign from 'lodash/assign';
import shortid from 'shortid';
import values from 'lodash/values';
import template from './template.html';
import './styles.scss';

export default angular
  .module('zsCaseNavigation', [
    composedReducerModule,
    vormInvokeModule,
    zsTooltipModule,
    auxiliaryRouteModule,
  ])
  .directive('zsCaseNavigation', [
    '$state',
    '$stateParams',
    'composedReducer',
    'vormInvoke',
    'auxiliaryRouteService',
    (
      $state,
      $stateParams,
      composedReducer,
      vormInvoke,
      auxiliaryRouteService
    ) => {
      const LINKS = [
        {
          name: 'phase',
          label: 'Fasen',
          url: [
            '$state',
            (state) => {
              return state.href('case');
            },
          ],
          counter: [
            'case',
            (caseObj) =>
              values(get(caseObj, 'instance.pending_changes', {})).length,
          ],
          icon: 'check-all',
        },
        {
          name: 'docs',
          label: 'Documenten',
          url: ['$state', (state) => state.href('case.docs')],
          counter: [
            'case',
            (caseObj) => get(caseObj, 'instance.num_unaccepted_files', 0),
          ],
          icon: 'file',
        },
        {
          name: 'timeline',
          label: 'Timeline',
          url: ['$state', (state) => state.href('case.timeline')],
          icon: 'clock',
        },
        {
          name: 'communication',
          label: 'Communicatie',
          url: ['$state', (state) => state.href('case.communication')],
          counter: [
            'case',
            (caseObj) => get(caseObj, 'instance.num_unread_communication', 0),
          ],
          icon: 'message',
        },
        {
          name: 'location',
          label: 'Kaart',
          url: ['$state', (state) => state.href('case.location')],
          icon: 'map',
          visible: ['case', 'isGeojsonOn', (caseObj, isGeojsonOn) => !!caseObj.instance.location && !isGeojsonOn],
        },
        {
          name: 'geojson',
          label: 'Kaart',
          url: ['$state', (state) => state.href('case.geojson')],
          icon: 'map',
          visible: ['isGeojsonOn', (isGeojsonOn) => isGeojsonOn]
        },
        {
          name: 'relations',
          label: 'Relaties',
          url: ['$state', (state) => state.href('case.relations')],
          icon: 'link',
        },
      ].map((linkInfo) => {
        return assign({}, linkInfo, {
          style: [
            '$state',
            (state) => {
              return startsWith(state.current.name, `case.${linkInfo.name}`)
                ? { selected: true }
                : null;
            },
          ],
        });
      });

      return {
        restrict: 'E',
        template,
        scope: {
          case: '&',
          casetype: '&',
          isGeojsonOn: '&',
          isCollapsed: '&',
        },
        bindToController: true,
        controller: [
          '$scope',
          function ($scope) {
            let ctrl = this,
              linkReducer;

            linkReducer = composedReducer(
              { scope: $scope },
              ctrl.case,
              ctrl.casetype,
              ctrl.isGeojsonOn,
              () => auxiliaryRouteService.getCurrentBase(),
              ctrl.isCollapsed
            ).reduce((caseObj, casetype, isGeojsonOn, base, isCollapsed) => {
              let invokeOpts = {
                  case: caseObj,
                  casetype,
                  isGeojsonOn,
                  $state,
                },
                links;

              links = LINKS.map((linkInfo) =>
                assign(
                  { id: shortid(), title: isCollapsed ? linkInfo.label : '' },
                  mapValues(linkInfo, (value) => vormInvoke(value, invokeOpts))
                )
              ).filter((link) => {
                return link.visible === undefined || !!link.visible;
              });

              return links;
            });

            ctrl.getLinks = linkReducer.data;
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
