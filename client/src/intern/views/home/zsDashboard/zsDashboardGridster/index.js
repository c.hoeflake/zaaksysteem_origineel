// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import mapValues from 'lodash/mapValues';
import mapKeys from 'lodash/mapKeys';
import assign from 'lodash/assign';

export default angular
  .module('zsDashboardGridster', [])
  .directive('zsDashboardGridster', [
    () => {
      return {
        restrict: 'A',
        require: 'gridster',
        link: (scope, element, attrs, gridster) => {
          let getKey = (key) => key.replace('widget.', '');

          // modified from angular-gridster's autoSetItemPosition, which modifies the grid

          let getItemPosition = (sizeX, sizeY) => {
            // walk through each row and column looking for a place it will fit
            for (let rowIndex = 0; rowIndex < gridster.maxRows; ++rowIndex) {
              for (let colIndex = 0; colIndex < gridster.columns; ++colIndex) {
                // only insert if position is not already taken and it can fit
                let items = gridster.getItems(
                  rowIndex,
                  colIndex,
                  sizeX,
                  sizeY,
                  []
                );

                if (
                  items.length === 0 &&
                  gridster.canItemOccupy({ sizeX, sizeY }, rowIndex, colIndex)
                ) {
                  return {
                    row: rowIndex,
                    col: colIndex,
                  };
                }
              }
            }

            return { row: 0, col: 0 };
          };

          scope.vm.onItemAdd.push((item) => {
            let customItemMap = scope.vm.customItemMap,
              widgetData = mapValues(
                customItemMap,
                (value /*, key*/) => item[getKey(value)]
              ),
              position = getItemPosition(widgetData.sizeX, widgetData.sizeY);

            assign(
              item,
              mapKeys(
                mapValues(
                  mapKeys(['row', 'col']),
                  (value, key) => position[key]
                ),
                (value, key) => getKey(customItemMap[key])
              )
            );
          });
        },
      };
    },
  ]).name;
