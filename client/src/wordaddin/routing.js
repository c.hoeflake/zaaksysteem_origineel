// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import uiRouter from 'angular-ui-router';
import wordAppModule from './shared/wordApp';
import caseSearch from './caseSearch';
import caseDetail from './caseDetail';
import snackbarServiceModule from './../shared/ui/zsSnackbar/snackbarService';
import zsUiViewProgressModule from './../shared/ui/zsNProgress/zsUiViewProgress';
import flatten from 'lodash/flatten';
import merge from 'lodash/merge';

export default angular
  .module('Zaaksysteem.officeaddins.word.routing', [
    uiRouter,
    snackbarServiceModule,
    caseSearch.moduleName,
    caseDetail.moduleName,
    wordAppModule,
    zsUiViewProgressModule,
  ])
  .config([
    '$stateProvider',
    '$urlRouterProvider',
    '$locationProvider',
    ($stateProvider, $urlRouterProvider, $locationProvider) => {
      $locationProvider.html5Mode(true);

      $urlRouterProvider.otherwise(($injector) => {
        let $state = $injector.get('$state');

        $state.go('caseSearch');
      });

      $stateProvider.state({
        name: 'root',
        template: '<word-app></word-app>',
        resolve: {
          user: [
            '$rootScope',
            '$q',
            '$window',
            'snackbarService',
            'sessionService',
            ($rootScope, $q, $window, snackbarService, sessionService) => {
              let resource = sessionService.createResource($rootScope);

              return resource
                .asPromise()
                .then(() => resource)
                .catch(() => {
                  $window.location = `/auth/login?referer=${$window.location.pathname}`;

                  return snackbarService.error(
                    'U bent niet ingelogd. U wordt doorverwezen naar het loginscherm.'
                  );
                });
            },
          ],
        },
      });

      flatten(
        [caseSearch, caseDetail].map((routeConfig) => routeConfig.config)
      ).forEach((route) => {
        let mergedState = merge(route.route, {
          parent: 'root',
        });

        $stateProvider.state(route.state, mergedState);
      });
    },
  ])
  .run([
    '$rootScope',
    ($rootScope) => {
      $rootScope.$on('$stateChangeError', (...rest) => {
        console.log(...rest);
      });
    },
  ]).name;
