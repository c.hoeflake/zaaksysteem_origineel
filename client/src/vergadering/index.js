// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import 'angular-i18n/angular-locale_nl';
import ngAnimate from 'angular-animate';
import routing from './routing';
import resourceModule from './../shared/api/resource';
import zsResourceConfiguration from './../shared/api/resource/resourceReducer/zsResourceConfiguration';
import meetingAppModule from './shared/meetingApp';
import appServiceModule from './shared/appService';
import serviceworkerModule from './../shared/util/serviceworker';
import snackbarServiceModule from '../shared/ui/zsSnackbar/snackbarService';
import without from 'lodash/without';
import uniq from 'lodash/uniq';
import get from 'lodash/get';
import find from 'lodash/find';
import sessionServiceModule from '../shared/user/sessionService';
import loadIconFont from '../shared/util/loadIconFont';

export default angular
  .module('Zaaksysteem.meeting', [
    routing,
    resourceModule,
    ngAnimate,
    appServiceModule,
    serviceworkerModule,
    meetingAppModule,
    sessionServiceModule,
    snackbarServiceModule,
  ])
  .config([
    '$provide',
    ($provide) => {
      // make sure sourcemaps work
      // from https://github.com/angular/angular.js/issues/5217
      $provide.decorator('$exceptionHandler', [
        '$delegate',
        ($delegate) => {
          return (exception, cause) => {
            $delegate(exception, cause);
            setTimeout(() => {
              console.error(exception.stack);
            });
          };
        },
      ]);
    },
  ])
  .config([
    'appServiceProvider',
    (appServiceProvider) => {
      appServiceProvider
        .setDefaultState({
          expanded: false,
          filters: [],
          grouped: true,
        })
        .reduce('filter_add', 'filters', (filters, filterToAdd) =>
          uniq(filters.concat(filterToAdd))
        )
        .reduce('filter_remove', 'filters', (filters, filterToRemove) =>
          without(filters, filterToRemove)
        )
        .reduce('filter_clear', 'filters', () => [])
        .reduce('toggle_grouped', 'grouped', (isGrouped) => !isGrouped)
        .reduce('toggle_expand', 'expanded', (isExpanded) => !isExpanded)
        .reduce('disable_grouped', 'grouped', () => false);
    },
  ])
  .config([
    '$httpProvider',
    ($httpProvider) => {
      $httpProvider.defaults.withCredentials = true;
      $httpProvider.defaults.headers.common['X-Client-Type'] = 'web';
      $httpProvider.useApplyAsync(true);
      $httpProvider.interceptors.push([
        'zsStorage',
        '$window',
        '$q',
        (zsStorage, $window, $q) => ({
          response: (response) => {
            if (response.config.url === '/api/v1/session/current') {
              if (!response.data.result.instance.logged_in_user) {
                zsStorage.clear();
                $window.location.href = `/auth/login?referer=${$window.location.pathname}`;
                return $q.reject(response);
              }
            }

            return $q.resolve(response);
          },
          responseError: (rejection) => {
            if (
              rejection.status === 401 ||
              // configuration_incomplete implies the API is not configured for public accces,
              // which means we don't have a logged in user
              get(rejection, 'data.result.instance.type') ===
                'api/v1/configuration_incomplete' ||
              get(rejection, 'data.result.instance.type') ===
                'api/v1/error/api_interface_id'
            ) {
              zsStorage.clear();
              $window.location.href = `/auth/login?referer=${$window.location.pathname}`;
            }

            return $q.reject(rejection);
          },
        }),
      ]);
    },
  ])
  .config([
    'resourceProvider',
    (resourceProvider) => {
      zsResourceConfiguration(resourceProvider.configure);
    },
  ])
  .run(loadIconFont)
  .run([
    '$animate',
    ($animate) => {
      $animate.enabled(true);
    },
  ])
  .run([
    '$window',
    'serviceWorker',
    ($window, serviceWorker) => {
      let enable = ENV.USE_SERVICE_WORKERS;
      let worker = serviceWorker();

      if (worker.isSupported()) {
        worker.isEnabled().then((enabled) => {
          if (enabled !== enable) {
            return enable
              ? worker.enable()
              : worker.disable().then(() => {
                  if (enabled) {
                    $window.location.reload();
                  }
                });
          }
        });
      }
    },
  ])
  .run([
    '$window',
    '$http',
    '$q',
    'snackbarService',
    ($window, $http, $q, snackbarService) => {
      // This checks the localStorage for locally stored
      // proposal notes. If any are found, the UUIDs for those
      // cases get retrieved, then the contents get stored in
      // ZS through the api/v1/case/<ref>/note/create call.
      // Once all notes are succesfully stored, the locally
      // stored proposal notes get cleared from localStorage
      // so we make sure this is done only once per device.
      let notes;

      if (
        typeof $window.localStorage.meetingAppNotes !== 'undefined' &&
        $window.localStorage.meetingAppNotes !== 'undefined'
      ) {
        notes = JSON.parse($window.localStorage.meetingAppNotes);
      }

      if (notes && notes.length) {
        snackbarService.wait(
          'Uw notities worden gemigreerd naar Zaaksysteem. Een moment geduld...',
          {
            promise: $http({
              url: '/api/v1/case',
              params: {
                zql: `SELECT {} FROM case WHERE case.number IN (${notes
                  .map((note) => note.id)
                  .join(',')})`,
              },
            })
              .then((data) => {
                return data.data.result.instance.rows.map((item) => {
                  return {
                    reference: item.reference,
                    content: find(
                      notes,
                      (note) => note.id === item.instance.number
                    ).content,
                  };
                });
              })
              .then((preparedNotes) => {
                let promises = preparedNotes.map((note) => {
                  return $http({
                    url: `/api/v1/case/${note.reference}/note/create`,
                    method: 'POST',
                    data: {
                      content: note.content,
                    },
                  });
                });

                return $q.all(promises);
              })
              .then(() => {
                $window.localStorage.removeItem('meetingAppNotes');
              }),
            then: () =>
              'Migratie succesvol voltooid. Uw notities zijn nu opgeslagen in Zaaksysteem.',
            catch: () =>
              'Er ging iets mis bij het migreren van uw notities. Neem contact op met uw beheerder voor meer informatie.',
          }
        );
      }
    },
  ]).name;
