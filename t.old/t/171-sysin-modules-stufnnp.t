#! perl

### This TEST script tests the default CSV implementation

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use File::Spec::Functions;


use_ok('Zaaksysteem::Backend::Sysin::Modules::STUFNNP');


sub create_config_interface {
    my $params                  = shift || {};

    return $zs->create_named_interface_ok(
        {
            module              => 'stufconfig',
            name                => 'STUF PRS Parsing',
            interface_config    => {
                mk_async_url    => 'http://localhost:3331/stuf',
                mk_sync_url     => 'http://localhost:3331/stuf',
                mk_ontvanger    => 'TESM',
                %{ $params }
            }
        },
    );
}

sub _create_stuf_interface {
    my $cfg = create_config_interface();

    $zs->create_named_interface_ok(
        {
            module          => 'stufnnp',
            name            => 'STUF NNP Parsing',
        },
    );
}


$zs->zs_transaction_ok(sub {
    my $cfg = create_config_interface();

    my $VALIDATION_MAP = {
        'Bedrijf'     => {
            'handelsnaam'               => 'Mintlab B.V. 1',
            'dossiernummer'             => '51987465',
            'faillisement'              => undef,
            'surseance'                 => undef,
            'rechtsvorm'                => '41',
            'vestiging_straatnaam'      => 'Donker Curtiusstraat',
            'vestiging_huisnummer'      => '7',
            'vestiging_huisletter'      => 'a',
            'vestiging_woonplaats'      => 'Amsterdam',
        },
    };

    my $interface = $zs->create_named_interface_ok(
        {
            module          => 'stufnnp',
            name            => 'STUF NNP Parsing',
        },
    );

    ### Last created object_subscription
    my $transaction = $interface->process({
        config_interface_id => $cfg->id,
        input_data => $zs->get_file_contents_as_string(
            catfile(STUF_TEST_XML_PATH, qw[0204 nnp 101-nnp-create-mintlab.xml])
        ),
    });

    ok($transaction, 'NNP Transaction completed');
    ok(!$transaction->error_count, 'NNP Transaction: no errors');
    is($transaction->success_count, 1, 'NNP Transaction: 1 success');
    is($transaction->external_transaction_id, 'MK0002680371', 'PRS Transaction: external transaction id');
    is($transaction->automated_retry_count, undef, 'NNP Transaction: automated retry count');
    ok($transaction->date_created, 'NNP Transaction: date created');
    ok(!$transaction->date_next_retry, 'NNP Transaction: no date next_retry');
    ok($transaction->date_last_retry, 'NNP Transaction: date last retry');

    is($transaction->records->count, 1, 'Got single transaction record');

    my $record      = $transaction->records->first;

    is($record->transaction_record_to_objects, 1, 'Got one mutation');

    my $bms        = $record->transaction_record_to_objects->search(
        {
            'local_table'           => 'Bedrijf',
            'transaction_record_id' => $record->id,
        }
    );

    is($bms->count, 1, 'Got single Bedrijf mutation record');

    my $bm         = $bms->first;

    my $b          = $schema->resultset('Bedrijf')->find(
        $bm->local_id
    );

    #note(explain({ $b->get_columns }));
    ok($b->authenticated, 'Record is authenticated');
    is($b->authenticatedby, 'kvk', 'Record is authenticated by GBA');

    is(
        $schema->resultset('ObjectSubscription')->search(
            {
                'local_table'   => 'Bedrijf',
                'local_id'      => $b->id,
            }
        )->count,
        1,
        'Got single subscription'
    );

    for my $key (keys %{ $VALIDATION_MAP->{Bedrijf} }) {
        my $givenvalue = $b->$key;
        my $wantedvalue = $VALIDATION_MAP->{Bedrijf}->{ $key };

        is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    }

}, 'Tested: Create single PRS mutation');

$zs->zs_transaction_ok(sub {
    my $cfg = create_config_interface();

    my $interface =$zs->create_named_interface_ok(
        {
            module          => 'stufnnp',
            name            => 'STUF NNP Parsing',
        },
    );

    ### Last created object_subscription
    my $transaction = $interface->process({
        config_interface_id => $cfg->id,
        input_data => $zs->get_file_contents_as_string(
            catfile(STUF_TEST_XML_PATH, qw[0204 nnp 106-nnp-create-foreigner.xml])
        )
    });

    ok(!$transaction->error_count, 'PRS Transaction: no errors');
    is($transaction->success_count, 1, 'PRS Transaction: 1 success');
    is($transaction->records->count, 1, 'Got single transaction record');

    my $record      = $transaction->records->first;
    is($record->transaction_record_to_objects, 1, 'Got one mutation');

    my $nnpm        = $record->transaction_record_to_objects->search(
        {
            'local_table'           => 'Bedrijf',
            'transaction_record_id' => $record->id,
        }
    )->first;
    my $nnp          = $schema->resultset('Bedrijf')->find(
        $nnpm->local_id
    );

    ok($nnp->authenticated, 'Record is authenticated');
    is($nnp->authenticatedby, 'kvk', 'Record is authenticated by KVK');

    is(
        $schema->resultset('ObjectSubscription')->search(
            {
                'local_table'   => 'Bedrijf',
                'local_id'      => $nnp->id,
            }
        )->count,
        1,
        'Got single subscription'
    );

    is($nnp->vestiging_landcode, 5010, 'Got correct landcode for foreigner');

    is($nnp->vestiging_adres_buitenland1, '2e Carabinierslaan 19', 'Found adres_buitenland1');
    is($nnp->vestiging_adres_buitenland2, 'Foreigncity', 'Found adres_buitenland2');
    is($nnp->vestiging_adres_buitenland3, 'Foreigntown', 'Found adres_buitenland3');

}, 'Tested: Create single NNP FOREIGNER mutation');

$zs->zs_transaction_ok(sub {
    my $cfg = create_config_interface();

    my $interface = $zs->create_named_interface_ok({
        module          => 'stufnnp',
        name            => 'STUF NNP Parsing',
    });

    ### Last created object_subscription
    my $transaction = $interface->process({
        config_interface_id => $cfg->id,
        input_data => $zs->get_file_contents_as_string(
            catfile(STUF_TEST_XML_PATH, qw[0204 nnp 107-nnp-create-geen_handelsnaam_wel_statutaire_naam.xml])
        ),
    });

    ok(!$transaction->error_count, 'PRS Transaction: no errors');
    is($transaction->success_count, 1, 'PRS Transaction: 1 success');
    is($transaction->records->count, 1, 'Got single transaction record');

    my $record      = $transaction->records->first;
    is($record->transaction_record_to_objects, 1, 'Got one mutation');

    my $nnpm        = $record->transaction_record_to_objects->search(
        {
            'local_table'           => 'Bedrijf',
            'transaction_record_id' => $record->id,
        }
    )->first;
    my $nnp          = $schema->resultset('Bedrijf')->find(
        $nnpm->local_id
    );

    ok($nnp->authenticated, 'Record is authenticated');
    is($nnp->authenticatedby, 'kvk', 'Record is authenticated by KVK');

    is(
        $schema->resultset('ObjectSubscription')->search(
            {
                'local_table'   => 'Bedrijf',
                'local_id'      => $nnp->id,
            }
        )->count,
        1,
        'Got single subscription'
    );

    is($nnp->handelsnaam, 'Mintlab B.V. Statutair', 'Got correct landcode for foreigner');
}, 'Tested: Create single NNP FOREIGNER mutation');

$zs->zs_transaction_ok(sub {
    my $cfg = create_config_interface();

    my $interface = $zs->create_named_interface_ok({
        module          => 'stufnnp',
        name            => 'STUF NNP Parsing',
    });

    ### Last created object_subscription
    my $transaction = $interface->process({
        config_interface_id => $cfg->id,
        input_data => $zs->get_file_contents_as_string(
            catfile(STUF_TEST_XML_PATH, qw[0204 nnp 104-nnp-create-bug_vestigingsnummer_field.xml])
        )
    });

    ok defined $transaction, 'NNP transaction: defined';
    ok !$transaction->error_count, 'NNP transaction: no errors';
    is $transaction->success_count, 1, 'NNP transaction: 1 success';
    is $transaction->records->count, 1, 'NNP transaction: 1 record';

    my $record = $transaction->records->first;

    is $record->transaction_record_to_objects->count, 1, 'NNP transaction record: 1 mutation';

    my $nnpm = $record->transaction_record_to_objects->search({
        local_table => 'Bedrijf',
        transaction_record_id => $record->id
    })->first;

    isa_ok $nnpm, 'Zaaksysteem::Backend::Sysin::TransactionRecordToObject::Component';

    my $nnp = $schema->resultset('Bedrijf')->find($nnpm->local_id);

    isa_ok $nnp, 'Zaaksysteem::DB::Component::Bedrijf';

    is $nnp->vestigingsnummer, '556738987634', 'NNP transaction: vestigingsnummer field';
}, 'Tested: Create single NNP with vestigingsNummer field');


$zs->zs_transaction_ok(sub {
    my $cfg = create_config_interface();

    my $interface = $zs->create_named_interface_ok({
        module          => 'stufnnp',
        name            => 'STUF NNP Parsing',
    });

    ### Last created object_subscription
    my $transaction = $interface->process({
        config_interface_id => $cfg->id,
        input_data => $zs->get_file_contents_as_string(
            catfile(STUF_TEST_XML_PATH, qw[0204 nnp 121-nnp-update-mintlab.xml])
        ),
    });

    like($transaction->records->first->output, qr/no_entry_found/, 'Missing subscription');
}, 'Tested: NNP Update Mutation, missing object subscription');

$zs->zs_transaction_ok(sub {
    my $cfg = create_config_interface();

    my $interface = $zs->create_named_interface_ok({
        module          => 'stufnnp',
        name            => 'STUF NNP Parsing',
    });

    ### Last created object_subscription
    my $transaction = $interface->process({
        config_interface_id => $cfg->id,
        input_data => $zs->get_file_contents_as_string(
            catfile(STUF_TEST_XML_PATH, qw[0204 nnp 103-nnp-create-bug_malformed_name.xml])
        )
    });

    ok !$transaction->error_count, 'PRS Transaction: no errors';
    is $transaction->success_count, 1, 'PRS Transaction: 1 success';
    is $transaction->records->count, 1, 'Got single transaction record';

    my $record = $transaction->records->first;

    is $record->transaction_record_to_objects, 1, 'Got one mutation';

    my $nnpm = $record->transaction_record_to_objects->search({
        local_table           => 'Bedrijf',
        transaction_record_id => $record->id,
    })->first;

    my $nnp = $schema->resultset('Bedrijf')->find($nnpm->local_id);

    ok $nnp->authenticated, 'Record is authenticated';
    is $nnp->authenticatedby, 'kvk', 'Record is authenticated by KVK';

    my $subscription_count = $schema->resultset('ObjectSubscription')->search({
        local_table => 'Bedrijf',
        local_id    => $nnp->id,
    })->count;

    is $subscription_count, 1, 'Got single subscription';

    is $nnp->handelsnaam, 'Mintlab B.V. 1', 'Got correct handelsname, stripped of newlines';
}, 'Tested: Create single NNP with malformed name mutation');

$zs->zs_transaction_ok(sub {
    my $cfg = create_config_interface();

    my $interface = $zs->create_named_interface_ok({
        module          => 'stufnnp',
        name            => 'STUF NNP Parsing',
    });

    my $VALIDATION_MAP = {
        'Bedrijf'     => {
            'handelsnaam'               => 'Mintlab B.V. 11',
            'dossiernummer'             => '51987465',
            'faillisement'              => undef,
            'surseance'                 => undef,
            'rechtsvorm'                => '41',
            'vestiging_straatnaam'      => 'Donker Curtiusstraat',
            'vestiging_huisnummer'      => '7',
            'vestiging_woonplaats'      => 'Amsterdam',
            'vestigingsnummer'          => '556738987634',
        },
    };

    ###
    ### CREATE FIRST ENTRY
    ###
    my $transaction = $interface->process({
        config_interface_id => $cfg->id,
        input_data => $zs->get_file_contents_as_string(
            catfile(STUF_TEST_XML_PATH, qw[0204 nnp 101-nnp-create-mintlab.xml])
        ),
    });


    ###
    ### MUTATE CREATED ENTRY
    ###

    my $second_transaction = $interface->process({
        input_data              => $zs->get_file_contents_as_string(
                                    catfile(STUF_TEST_XML_PATH, qw[0204 nnp 121-nnp-update-mintlab.xml])
                                ),
    });


    my $record     = $second_transaction->records->first;

    my $mutation   = $record->transaction_record_to_objects->search(
        {
            'local_table' => 'Bedrijf',
            'transaction_record_id' => $record->id,
        }
    )->first;

    ok($mutation, 'Found Bedrijf mutation');

    my $np          = $schema->resultset('Bedrijf')->find(
        $mutation->local_id
    );

    ok(!$second_transaction->error_count, 'Succesfully mutated NNP entry');

    for my $key (keys %{ $VALIDATION_MAP->{Bedrijf} }) {
        my $givenvalue = $np->$key;
        my $wantedvalue = $VALIDATION_MAP->{Bedrijf}->{ $key };

        is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    }

}, 'Tested: NNP Update Mutation');

$zs->zs_transaction_ok(sub {
    my $cfg = create_config_interface();

    my $interface = $zs->create_named_interface_ok({
        module          => 'stufnnp',
        name            => 'STUF NNP Parsing',
    });

    my $VALIDATION_MAP = {
        'Bedrijf'     => {
            'handelsnaam'               => 'Mintlab B.V. 11',
            'dossiernummer'             => '51987465',
            'faillisement'              => undef,
            'surseance'                 => undef,
            'rechtsvorm'                => '41',
            'vestiging_straatnaam'      => 'Donker Curtiusstraat',
            'vestiging_huisnummer'      => '7',
            'vestiging_woonplaats'      => 'Amsterdam',
            'vestigingsnummer'          => '556738987634',
        },
    };

    ###
    ### CREATE FIRST ENTRY
    ###
    my $transaction = $interface->process({
        config_interface_id => $cfg->id,
        input_data => $zs->get_file_contents_as_string(
            catfile(STUF_TEST_XML_PATH, qw[0204 nnp 101-nnp-create-mintlab.xml])
        ),
    });


    ###
    ### MUTATE CREATED ENTRY
    ###

    my $duplicate               = $zs->get_file_contents_as_string(
                                    catfile(STUF_TEST_XML_PATH, qw[0204 nnp 101-nnp-create-mintlab.xml])
                                );

    $duplicate =~ s/Mintlab B.V/MLLLAB B.V./;
    $duplicate =~ s/sleutelGegevensbeheer="1200001"/sleutelGegevensbeheer="1200002"/;
    $duplicate =~ s/sleutelVerzendend="9200001"/sleutelVerzendend="9200002"/;

    my $second_transaction = $interface->process({
        input_data              => $duplicate,
    });

    my $record     = $transaction->records->first;

    my $mutation   = $record->transaction_record_to_objects->search(
        {
            'local_table' => 'Bedrijf',
            'transaction_record_id' => $record->id,
        }
    )->first;

    my $second_record     = $second_transaction->records->first;

    my $second_mutation   = $second_record->transaction_record_to_objects->search(
        {
            'local_table' => 'Bedrijf',
            'transaction_record_id' => $second_record->id,
        }
    )->first;

    isnt($mutation->local_id, $second_mutation->local_id, 'Found different local_id for bedrijf');

}, 'Tested: ZS-3566 Prevent updating existing bedrijf on duplicated dossiernummer');


$zs->zs_transaction_ok(sub {
    my $cfg = create_config_interface();

    my $interface = $zs->create_named_interface_ok({
        module          => 'stufnnp',
        name            => 'STUF NNP Parsing',
    });

    my $VALIDATION_MAP = {
        'Bedrijf'     => {
            'handelsnaam'               => 'Mintlab B.V. 21',
            'faillisement'              => undef,
            'surseance'                 => undef,
            'vestiging_straatnaam'      => 'Voorstraat',
            'vestiging_huisnummer'      => '43',
            'vestiging_woonplaats'      => 'Vianen',
        },
    };

    ###
    ### CREATE FIRST ENTRY
    ###
    my $transaction = $interface->process({
        config_interface_id => $cfg->id,
        input_data => $zs->get_file_contents_as_string(
            catfile(STUF_TEST_XML_PATH, qw[0204 nnp 102-nnp-create-bug_no_address.xml])
        ),
    });

    my $record     = $transaction->records->first;

    my $mutation   = $record->transaction_record_to_objects->search(
        {
            'local_table' => 'Bedrijf',
            'transaction_record_id' => $record->id,
        }
    )->first;

    my $b          = $schema->resultset('Bedrijf')->find(
        $mutation->local_id
    );

    for my $key (keys %{ $VALIDATION_MAP->{Bedrijf} }) {
        my $givenvalue = $b->$key;
        my $wantedvalue = $VALIDATION_MAP->{Bedrijf}->{ $key };

        is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    }

}, 'Tested: NNP Missing Addres Bug');

$zs->zs_transaction_ok(sub {
    my $cfg = create_config_interface();

    my $interface = $zs->create_named_interface_ok({
        module          => 'stufnnp',
        name            => 'STUF NNP Parsing',
    });

    ###
    ### CREATE FIRST ENTRY
    ###
    my $transaction = $interface->process({
        config_interface_id => $cfg->id,
        input_data => $zs->get_file_contents_as_string(
            catfile(STUF_TEST_XML_PATH, qw[0204 nnp 101-nnp-create-mintlab.xml])
        ),
    });

    my $record     = $transaction->records->first;

    is(
        $schema->resultset('ObjectSubscription')->search_active->count,
        1,
        'Single object subscription'
    );

    my $mutation   = $record->transaction_record_to_objects->search(
        {
            'local_table' => 'Bedrijf',
            'transaction_record_id' => $record->id,
        }
    )->first;

    ok($mutation, 'Found Bedrijf mutation');

    ###
    ### DELETE CREATED ENTRY
    ###
    my $second_transaction = $interface->process({
        input_data              => $zs->get_file_contents_as_string(
                                    catfile(STUF_TEST_XML_PATH, qw[0204 nnp 131-nnp-delete-mintlab.xml])
                                ),
    });

    my $np          = $schema->resultset('Bedrijf')->find(
        $mutation->local_id
    );

    ok(!$second_transaction->error_count, 'Succesfully deleted NNP entry');
    ok($np->deleted_on, 'Bedrijf is deleted');

    ok(
        !$schema->resultset('ObjectSubscription')->search_active->count,
        'Object subscription removed'
    );
}, 'Tested: PRS Delete');

$zs->zs_transaction_ok(sub {
    my $cfg = create_config_interface();

    my $interface = $zs->create_named_interface_ok({
        module          => 'stufnnp',
        name            => 'STUF NNP Parsing',
    });

    my $inserted_transaction = $interface->process({
        config_interface_id => $cfg->id,
        input_data => $zs->get_file_contents_as_string(
            catfile(STUF_TEST_XML_PATH, qw[0204 nnp 101-nnp-create-mintlab.xml])
        ),
    });

    my $transaction = $interface->process_trigger(
        'disable_subscription',
        {
            subscription_id => $interface->object_subscription_interface_ids->first->id,
        }
    );

}, 'Tested: PRS remove afnemerindicatie');

$zs->zs_transaction_ok(sub {
    my $cfg = create_config_interface();

    my $interface = $zs->create_named_interface_ok({
        module          => 'stufnnp',
        name            => 'STUF NNP Parsing',
    });

    ### Last created object_subscription
    my $transaction = $interface->process({
        config_interface_id => $cfg->id,
        input_data => $zs->get_file_contents_as_string(
            catfile(STUF_TEST_XML_PATH, qw[0204 nnp 105-nnp-create-check_handelsnaam.xml])
        ),
    });

    my $record      = $transaction->records->first;

    my $bms        = $record->transaction_record_to_objects->search(
        {
            'local_table'           => 'Bedrijf',
            'transaction_record_id' => $record->id,
        }
    );

    my $bm         = $bms->first;

    my $b          = $schema->resultset('Bedrijf')->find(
        $bm->local_id
    );

    # note(explain({ $b->get_columns }));
    ok($b->handelsnaam, 'Record has a long handelsnaam');
    ok($b->vestiging_woonplaats, 'Record has a vestigings_woonplaats');
    ok($b->dossiernummer, 'Record has a dossiernummer');

}, 'ZS-3256 ZS-3257 Missing handelsnamen/woonplaats fixes');

$zs->zs_transaction_ok(sub {
    my $cfg = create_config_interface();

    my $interface = $zs->create_named_interface_ok({
        module          => 'stufnnp',
        name            => 'STUF NNP Parsing',
    });

    my $VALIDATION_MAP = {
        'Bedrijf'     => {
            'handelsnaam'               => 'Mintlab B.V. 11',
            'dossiernummer'             => '51987465',
            'faillisement'              => undef,
            'surseance'                 => undef,
            'rechtsvorm'                => '41',
            'vestiging_straatnaam'      => 'Donker Curtiusstraat',
            'vestiging_huisnummer'      => '7',
            'vestiging_woonplaats'      => 'Amsterdam',
            'vestigingsnummer'          => '556738987634',
        },
    };

    ###
    ### CREATE FIRST ENTRY
    ###
    my $transaction = $interface->process({
        config_interface_id => $cfg->id,
        input_data => $zs->get_file_contents_as_string(
            catfile(STUF_TEST_XML_PATH, qw[0204 nnp 101-nnp-create-mintlab.xml])
        ),
    });


    ###
    ### MUTATE CREATED ENTRY
    ###

    my $second_transaction = $interface->process({
        config_interface_id => $cfg->id,
        input_data => $zs->get_file_contents_as_string(
            catfile(STUF_TEST_XML_PATH, qw[0204 nnp 132-nnp-delete-mintlab-by_enddate.xml])
        ),
    });


    my $record     = $transaction->records->first;

    my $mutation   = $record->transaction_record_to_objects->search(
        {
            'local_table' => 'Bedrijf',
            'transaction_record_id' => $record->id,
        }
    )->first;

    ok($mutation, 'Found Bedrijf mutation');

    my $np          = $schema->resultset('Bedrijf')->find(
        $mutation->local_id
    );

    ok(!$second_transaction->error_count, 'Succesfully mutated NNP entry');

    ok($np->deleted_on, 'Company is deleted');
    ok($np->subscription_id->date_deleted, 'Subscription has been removed');


}, 'ZS-3727 Delete NNP by inspecting enddate');


zs_done_testing();
