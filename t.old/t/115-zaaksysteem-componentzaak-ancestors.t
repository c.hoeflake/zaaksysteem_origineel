#! perl
use TestSetup;
initialize_test_globals_ok;

$zs->zs_transaction_ok(sub {
    my $case1 = $zs->create_case_ok();
    my $case2 = $zs->create_case_ok();
    my $case3 = $zs->create_case_ok();

    $case2->update({pid => $case1->id});
    $case3->update({pid => $case2->id});

    is_deeply(
        [map { $_->id } $case3->ancestors],
        [map { $_->id } ($case2, $case1)],
        "->ancestors on a case with parent + grandparent",
    );

    is_deeply(
        [map { $_->id } $case2->ancestors],
        [map { $_->id } $case1],
        "->ancestors on a case with parent only",
    );

    is_deeply(
        [map { $_->id } $case1->ancestors],
        [],
        "->ancestors on a case without a parent",
    );

# Dit zorgt voor een zowat infinate loop
#    $case1->update({pid => $case3->id});
#
#    {
#        my @warnings;
#        local $SIG{__WARN__} = sub { push @warnings, [@_]; };
#        is_deeply(
#            [map { $_->id } $case1->ancestors],
#            [map { $_->id } ($case3, $case2, $case1)],
#            "->ancestors on a case without a parent + grandparent that is also its child",
#        );
#
#        is_deeply(
#            \@warnings,
#            [
#                [ "Case " . $case1->id . " has a loop in its parent hierarchy.\n" ]
#            ],
#            'A warning was emitted because of the parent loop'
#        );
#    }

}, 'Case ancestors');

zs_done_testing();
