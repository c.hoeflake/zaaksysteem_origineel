use utf8;
package Zaaksysteem::Schema::Logging;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::Logging

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<logging>

=cut

__PACKAGE__->table("logging");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'logging_id_seq'

=head2 zaak_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 betrokkene_id

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 aanvrager_id

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 is_bericht

  data_type: 'integer'
  is_nullable: 1

=head2 component

  data_type: 'varchar'
  is_nullable: 1
  size: 64

=head2 component_id

  data_type: 'integer'
  is_nullable: 1

=head2 seen

  data_type: 'integer'
  is_nullable: 1

=head2 onderwerp

  data_type: 'text'
  is_nullable: 1

=head2 bericht

  data_type: 'text'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 deleted_on

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 event_type

  data_type: 'text'
  is_nullable: 1

=head2 event_data

  data_type: 'text'
  is_nullable: 1

=head2 created_by

  data_type: 'text'
  is_nullable: 1

=head2 modified_by

  data_type: 'text'
  is_nullable: 1

=head2 deleted_by

  data_type: 'text'
  is_nullable: 1

=head2 created_for

  data_type: 'text'
  is_nullable: 1

=head2 created_by_name_cache

  data_type: 'text'
  is_nullable: 1
  original: {data_type => "varchar"}

=head2 object_uuid

  data_type: 'uuid'
  is_foreign_key: 1
  is_nullable: 1
  size: 16

=head2 restricted

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "logging_id_seq",
  },
  "zaak_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "betrokkene_id",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "aanvrager_id",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "is_bericht",
  { data_type => "integer", is_nullable => 1 },
  "component",
  { data_type => "varchar", is_nullable => 1, size => 64 },
  "component_id",
  { data_type => "integer", is_nullable => 1 },
  "seen",
  { data_type => "integer", is_nullable => 1 },
  "onderwerp",
  { data_type => "text", is_nullable => 1 },
  "bericht",
  { data_type => "text", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "deleted_on",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "event_type",
  { data_type => "text", is_nullable => 1 },
  "event_data",
  { data_type => "text", is_nullable => 1 },
  "created_by",
  { data_type => "text", is_nullable => 1 },
  "modified_by",
  { data_type => "text", is_nullable => 1 },
  "deleted_by",
  { data_type => "text", is_nullable => 1 },
  "created_for",
  { data_type => "text", is_nullable => 1 },
  "created_by_name_cache",
  {
    data_type   => "text",
    is_nullable => 1,
    original    => { data_type => "varchar" },
  },
  "object_uuid",
  { data_type => "uuid", is_foreign_key => 1, is_nullable => 1, size => 16 },
  "restricted",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 messages

Type: has_many

Related object: L<Zaaksysteem::Schema::Message>

=cut

__PACKAGE__->has_many(
  "messages",
  "Zaaksysteem::Schema::Message",
  { "foreign.logging_id" => "self.id" },
  undef,
);

=head2 object_uuid

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ObjectData>

=cut

__PACKAGE__->belongs_to(
  "object_uuid",
  "Zaaksysteem::Schema::ObjectData",
  { uuid => "object_uuid" },
);

=head2 zaak_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to("zaak_id", "Zaaksysteem::Schema::Zaak", { id => "zaak_id" });

=head2 zaaktype_nodes

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeNode>

=cut

__PACKAGE__->has_many(
  "zaaktype_nodes",
  "Zaaksysteem::Schema::ZaaktypeNode",
  { "foreign.logging_id" => "self.id" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-01-23 10:55:52
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:i+l898FgQOuR3sKrvJO76g

__PACKAGE__->load_components(
    "+Zaaksysteem::DB::Component::Logging",
    "+Zaaksysteem::Helper::ToJSON",
    __PACKAGE__->load_components()
);

__PACKAGE__->belongs_to('maybezaak' => 'Zaaksysteem::Schema::Zaak', { id => 'zaak_id' }, { join_type => 'left' });

__PACKAGE__->resultset_class('Zaaksysteem::DB::ResultSet::Logging');

__PACKAGE__->add_columns(
  "id",
  {
    data_type => "INTEGER",
    default_value => undef,
    is_nullable => 1,
    size => undef,
    is_auto_increment => 1,
  }
);

__PACKAGE__->inflate_column('event_data', {
    inflate => sub { JSON::XS->new->decode(shift || '{}') },
    deflate => sub { JSON::XS->new->canonical(1)->encode(shift || {}) }
});

# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

