use utf8;
package Zaaksysteem::Schema::ZaaktypeRelatie;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ZaaktypeRelatie

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<zaaktype_relatie>

=cut

__PACKAGE__->table("zaaktype_relatie");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'zaaktype_relatie_id_seq'

=head2 zaaktype_node_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 relatie_zaaktype_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 zaaktype_status_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 relatie_type

  data_type: 'text'
  is_nullable: 1

=head2 eigenaar_type

  data_type: 'text'
  default_value: 'aanvrager'
  is_nullable: 0

=head2 start_delay

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 created

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 status

  data_type: 'integer'
  is_nullable: 1

=head2 kopieren_kenmerken

  data_type: 'integer'
  is_nullable: 1

=head2 ou_id

  data_type: 'integer'
  is_nullable: 1

=head2 role_id

  data_type: 'integer'
  is_nullable: 1

=head2 automatisch_behandelen

  data_type: 'boolean'
  is_nullable: 1

=head2 required

  data_type: 'varchar'
  is_nullable: 1
  size: 12

=head2 betrokkene_authorized

  data_type: 'boolean'
  is_nullable: 1

=head2 betrokkene_notify

  data_type: 'boolean'
  is_nullable: 1

=head2 betrokkene_id

  data_type: 'text'
  is_nullable: 1

=head2 betrokkene_role

  data_type: 'text'
  is_nullable: 1

=head2 betrokkene_role_set

  data_type: 'text'
  is_nullable: 1

=head2 betrokkene_prefix

  data_type: 'text'
  is_nullable: 1

=head2 eigenaar_id

  data_type: 'text'
  is_nullable: 1

=head2 eigenaar_role

  data_type: 'text'
  is_nullable: 1

=head2 show_in_pip

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=head2 pip_label

  data_type: 'text'
  is_nullable: 1

=head2 subject_role

  data_type: 'text[]'
  is_nullable: 1

=head2 copy_subject_role

  data_type: 'boolean'
  default_value: false
  is_nullable: 1

=head2 copy_related_cases

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=head2 copy_related_objects

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=head2 copy_selected_attributes

  data_type: 'hstore'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "zaaktype_relatie_id_seq",
  },
  "zaaktype_node_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "relatie_zaaktype_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "zaaktype_status_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "relatie_type",
  { data_type => "text", is_nullable => 1 },
  "eigenaar_type",
  { data_type => "text", default_value => "aanvrager", is_nullable => 0 },
  "start_delay",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "created",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "status",
  { data_type => "integer", is_nullable => 1 },
  "kopieren_kenmerken",
  { data_type => "integer", is_nullable => 1 },
  "ou_id",
  { data_type => "integer", is_nullable => 1 },
  "role_id",
  { data_type => "integer", is_nullable => 1 },
  "automatisch_behandelen",
  { data_type => "boolean", is_nullable => 1 },
  "required",
  { data_type => "varchar", is_nullable => 1, size => 12 },
  "betrokkene_authorized",
  { data_type => "boolean", is_nullable => 1 },
  "betrokkene_notify",
  { data_type => "boolean", is_nullable => 1 },
  "betrokkene_id",
  { data_type => "text", is_nullable => 1 },
  "betrokkene_role",
  { data_type => "text", is_nullable => 1 },
  "betrokkene_role_set",
  { data_type => "text", is_nullable => 1 },
  "betrokkene_prefix",
  { data_type => "text", is_nullable => 1 },
  "eigenaar_id",
  { data_type => "text", is_nullable => 1 },
  "eigenaar_role",
  { data_type => "text", is_nullable => 1 },
  "show_in_pip",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
  "pip_label",
  { data_type => "text", is_nullable => 1 },
  "subject_role",
  { data_type => "text[]", is_nullable => 1 },
  "copy_subject_role",
  { data_type => "boolean", default_value => \"false", is_nullable => 1 },
  "copy_related_cases",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
  "copy_related_objects",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
  "copy_selected_attributes",
  { data_type => "hstore", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 relatie_zaaktype_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaaktype>

=cut

__PACKAGE__->belongs_to(
  "relatie_zaaktype_id",
  "Zaaksysteem::Schema::Zaaktype",
  { id => "relatie_zaaktype_id" },
);

=head2 zaaktype_node_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeNode>

=cut

__PACKAGE__->belongs_to(
  "zaaktype_node_id",
  "Zaaksysteem::Schema::ZaaktypeNode",
  { id => "zaaktype_node_id" },
);

=head2 zaaktype_status_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeStatus>

=cut

__PACKAGE__->belongs_to(
  "zaaktype_status_id",
  "Zaaksysteem::Schema::ZaaktypeStatus",
  { id => "zaaktype_status_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-10-01 12:22:49
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:UheMVWa6OyCXT84/qU757A
__PACKAGE__->resultset_class('Zaaksysteem::DB::ResultSet::ZaaktypeRelatie');

use Zaaksysteem::DB::HStore;

__PACKAGE__->inflate_column('copy_selected_attributes', {
    inflate => sub {
        Zaaksysteem::DB::HStore::decode(shift);
    },
    deflate => sub {
        Zaaksysteem::DB::HStore::encode(shift);
    }
});

__PACKAGE__->load_components(
    "+Zaaksysteem::DB::Component::ZaaktypeRelatie",
    __PACKAGE__->load_components()
);

# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

