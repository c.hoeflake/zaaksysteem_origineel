use utf8;
package Zaaksysteem::Schema::Bedrijf;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::Bedrijf

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<bedrijf>

=cut

__PACKAGE__->table("bedrijf");

=head1 ACCESSORS

=head2 search_index

  data_type: 'tsvector'
  is_nullable: 1

=head2 search_term

  data_type: 'text'
  is_nullable: 1

=head2 object_type

  data_type: 'text'
  default_value: 'bedrijf'
  is_nullable: 1

=head2 searchable_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'searchable_searchable_id_seq'

=head2 search_order

  data_type: 'text'
  is_nullable: 1

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'bedrijf_id_seq'

=head2 dossiernummer

  data_type: 'varchar'
  is_nullable: 1
  size: 8

=head2 subdossiernummer

  data_type: 'varchar'
  is_nullable: 1
  size: 4

=head2 hoofdvestiging_dossiernummer

  data_type: 'varchar'
  is_nullable: 1
  size: 8

=head2 hoofdvestiging_subdossiernummer

  data_type: 'varchar'
  is_nullable: 1
  size: 4

=head2 vorig_dossiernummer

  data_type: 'varchar'
  is_nullable: 1
  size: 8

=head2 vorig_subdossiernummer

  data_type: 'varchar'
  is_nullable: 1
  size: 4

=head2 handelsnaam

  data_type: 'text'
  is_nullable: 1

=head2 rechtsvorm

  data_type: 'smallint'
  is_nullable: 1

=head2 kamernummer

  data_type: 'smallint'
  is_nullable: 1

=head2 faillisement

  data_type: 'smallint'
  is_nullable: 1

=head2 surseance

  data_type: 'smallint'
  is_nullable: 1

=head2 telefoonnummer

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 email

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 vestiging_adres

  data_type: 'text'
  is_nullable: 1

=head2 vestiging_straatnaam

  data_type: 'text'
  is_nullable: 1

=head2 vestiging_huisnummer

  data_type: 'bigint'
  is_nullable: 1

=head2 vestiging_huisnummertoevoeging

  data_type: 'text'
  is_nullable: 1

=head2 vestiging_postcodewoonplaats

  data_type: 'text'
  is_nullable: 1

=head2 vestiging_postcode

  data_type: 'varchar'
  is_nullable: 1
  size: 6

=head2 vestiging_woonplaats

  data_type: 'text'
  is_nullable: 1

=head2 correspondentie_adres

  data_type: 'text'
  is_nullable: 1

=head2 correspondentie_straatnaam

  data_type: 'text'
  is_nullable: 1

=head2 correspondentie_huisnummer

  data_type: 'bigint'
  is_nullable: 1

=head2 correspondentie_huisnummertoevoeging

  data_type: 'text'
  is_nullable: 1

=head2 correspondentie_postcodewoonplaats

  data_type: 'text'
  is_nullable: 1

=head2 correspondentie_postcode

  data_type: 'varchar'
  is_nullable: 1
  size: 6

=head2 correspondentie_woonplaats

  data_type: 'text'
  is_nullable: 1

=head2 hoofdactiviteitencode

  data_type: 'integer'
  is_nullable: 1

=head2 nevenactiviteitencode1

  data_type: 'integer'
  is_nullable: 1

=head2 nevenactiviteitencode2

  data_type: 'integer'
  is_nullable: 1

=head2 werkzamepersonen

  data_type: 'integer'
  is_nullable: 1

=head2 contact_naam

  data_type: 'varchar'
  is_nullable: 1
  size: 64

=head2 contact_aanspreektitel

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 contact_voorletters

  data_type: 'varchar'
  is_nullable: 1
  size: 19

=head2 contact_voorvoegsel

  data_type: 'varchar'
  is_nullable: 1
  size: 8

=head2 contact_geslachtsnaam

  data_type: 'varchar'
  is_nullable: 1
  size: 95

=head2 contact_geslachtsaanduiding

  data_type: 'varchar'
  is_nullable: 1
  size: 1

=head2 authenticated

  data_type: 'smallint'
  is_nullable: 1

=head2 authenticatedby

  data_type: 'text'
  is_nullable: 1

=head2 fulldossiernummer

  data_type: 'text'
  is_nullable: 1

=head2 import_datum

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 deleted_on

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 verblijfsobject_id

  data_type: 'varchar'
  is_nullable: 1
  size: 16

=head2 system_of_record

  data_type: 'varchar'
  is_nullable: 1
  size: 32

=head2 system_of_record_id

  data_type: 'bigint'
  is_nullable: 1

=head2 vestigingsnummer

  data_type: 'bigint'
  is_nullable: 1

=head2 vestiging_huisletter

  data_type: 'text'
  is_nullable: 1

=head2 correspondentie_huisletter

  data_type: 'text'
  is_nullable: 1

=head2 vestiging_adres_buitenland1

  data_type: 'text'
  is_nullable: 1

=head2 vestiging_adres_buitenland2

  data_type: 'text'
  is_nullable: 1

=head2 vestiging_adres_buitenland3

  data_type: 'text'
  is_nullable: 1

=head2 vestiging_landcode

  data_type: 'integer'
  default_value: 6030
  is_nullable: 1

=head2 correspondentie_adres_buitenland1

  data_type: 'text'
  is_nullable: 1

=head2 correspondentie_adres_buitenland2

  data_type: 'text'
  is_nullable: 1

=head2 correspondentie_adres_buitenland3

  data_type: 'text'
  is_nullable: 1

=head2 correspondentie_landcode

  data_type: 'integer'
  default_value: 6030
  is_nullable: 1

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 1
  size: 16

=head2 rsin

  data_type: 'bigint'
  is_nullable: 1

=head2 oin

  data_type: 'bigint'
  is_nullable: 1

=head2 main_activity

  data_type: 'jsonb'
  default_value: '{}'
  is_nullable: 0

=head2 secondairy_activities

  data_type: 'jsonb'
  default_value: '[]'
  is_nullable: 0

=head2 date_founded

  data_type: 'date'
  is_nullable: 1
  timezone: 'UTC'

=head2 date_registration

  data_type: 'date'
  is_nullable: 1
  timezone: 'UTC'

=head2 date_ceased

  data_type: 'date'
  is_nullable: 1
  timezone: 'UTC'

=head2 vestiging_latlong

  data_type: 'point'
  is_nullable: 1

=head2 vestiging_bag_id

  data_type: 'bigint'
  is_nullable: 1

=head2 preferred_contact_channel

  data_type: 'enum'
  default_value: 'pip'
  extra: {custom_type_name => "preferred_contact_channel",list => ["email","mail","pip","phone"]}
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "search_index",
  { data_type => "tsvector", is_nullable => 1 },
  "search_term",
  { data_type => "text", is_nullable => 1 },
  "object_type",
  { data_type => "text", default_value => "bedrijf", is_nullable => 1 },
  "searchable_id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "searchable_searchable_id_seq",
  },
  "search_order",
  { data_type => "text", is_nullable => 1 },
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "bedrijf_id_seq",
  },
  "dossiernummer",
  { data_type => "varchar", is_nullable => 1, size => 8 },
  "subdossiernummer",
  { data_type => "varchar", is_nullable => 1, size => 4 },
  "hoofdvestiging_dossiernummer",
  { data_type => "varchar", is_nullable => 1, size => 8 },
  "hoofdvestiging_subdossiernummer",
  { data_type => "varchar", is_nullable => 1, size => 4 },
  "vorig_dossiernummer",
  { data_type => "varchar", is_nullable => 1, size => 8 },
  "vorig_subdossiernummer",
  { data_type => "varchar", is_nullable => 1, size => 4 },
  "handelsnaam",
  { data_type => "text", is_nullable => 1 },
  "rechtsvorm",
  { data_type => "smallint", is_nullable => 1 },
  "kamernummer",
  { data_type => "smallint", is_nullable => 1 },
  "faillisement",
  { data_type => "smallint", is_nullable => 1 },
  "surseance",
  { data_type => "smallint", is_nullable => 1 },
  "telefoonnummer",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "email",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "vestiging_adres",
  { data_type => "text", is_nullable => 1 },
  "vestiging_straatnaam",
  { data_type => "text", is_nullable => 1 },
  "vestiging_huisnummer",
  { data_type => "bigint", is_nullable => 1 },
  "vestiging_huisnummertoevoeging",
  { data_type => "text", is_nullable => 1 },
  "vestiging_postcodewoonplaats",
  { data_type => "text", is_nullable => 1 },
  "vestiging_postcode",
  { data_type => "varchar", is_nullable => 1, size => 6 },
  "vestiging_woonplaats",
  { data_type => "text", is_nullable => 1 },
  "correspondentie_adres",
  { data_type => "text", is_nullable => 1 },
  "correspondentie_straatnaam",
  { data_type => "text", is_nullable => 1 },
  "correspondentie_huisnummer",
  { data_type => "bigint", is_nullable => 1 },
  "correspondentie_huisnummertoevoeging",
  { data_type => "text", is_nullable => 1 },
  "correspondentie_postcodewoonplaats",
  { data_type => "text", is_nullable => 1 },
  "correspondentie_postcode",
  { data_type => "varchar", is_nullable => 1, size => 6 },
  "correspondentie_woonplaats",
  { data_type => "text", is_nullable => 1 },
  "hoofdactiviteitencode",
  { data_type => "integer", is_nullable => 1 },
  "nevenactiviteitencode1",
  { data_type => "integer", is_nullable => 1 },
  "nevenactiviteitencode2",
  { data_type => "integer", is_nullable => 1 },
  "werkzamepersonen",
  { data_type => "integer", is_nullable => 1 },
  "contact_naam",
  { data_type => "varchar", is_nullable => 1, size => 64 },
  "contact_aanspreektitel",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "contact_voorletters",
  { data_type => "varchar", is_nullable => 1, size => 19 },
  "contact_voorvoegsel",
  { data_type => "varchar", is_nullable => 1, size => 8 },
  "contact_geslachtsnaam",
  { data_type => "varchar", is_nullable => 1, size => 95 },
  "contact_geslachtsaanduiding",
  { data_type => "varchar", is_nullable => 1, size => 1 },
  "authenticated",
  { data_type => "smallint", is_nullable => 1 },
  "authenticatedby",
  { data_type => "text", is_nullable => 1 },
  "fulldossiernummer",
  { data_type => "text", is_nullable => 1 },
  "import_datum",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "deleted_on",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "verblijfsobject_id",
  { data_type => "varchar", is_nullable => 1, size => 16 },
  "system_of_record",
  { data_type => "varchar", is_nullable => 1, size => 32 },
  "system_of_record_id",
  { data_type => "bigint", is_nullable => 1 },
  "vestigingsnummer",
  { data_type => "bigint", is_nullable => 1 },
  "vestiging_huisletter",
  { data_type => "text", is_nullable => 1 },
  "correspondentie_huisletter",
  { data_type => "text", is_nullable => 1 },
  "vestiging_adres_buitenland1",
  { data_type => "text", is_nullable => 1 },
  "vestiging_adres_buitenland2",
  { data_type => "text", is_nullable => 1 },
  "vestiging_adres_buitenland3",
  { data_type => "text", is_nullable => 1 },
  "vestiging_landcode",
  { data_type => "integer", default_value => 6030, is_nullable => 1 },
  "correspondentie_adres_buitenland1",
  { data_type => "text", is_nullable => 1 },
  "correspondentie_adres_buitenland2",
  { data_type => "text", is_nullable => 1 },
  "correspondentie_adres_buitenland3",
  { data_type => "text", is_nullable => 1 },
  "correspondentie_landcode",
  { data_type => "integer", default_value => 6030, is_nullable => 1 },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 1,
    size => 16,
  },
  "rsin",
  { data_type => "bigint", is_nullable => 1 },
  "oin",
  { data_type => "bigint", is_nullable => 1 },
  "main_activity",
  { data_type => "jsonb", default_value => "{}", is_nullable => 0 },
  "secondairy_activities",
  { data_type => "jsonb", default_value => "[]", is_nullable => 0 },
  "date_founded",
  { data_type => "date", is_nullable => 1, timezone => "UTC" },
  "date_registration",
  { data_type => "date", is_nullable => 1, timezone => "UTC" },
  "date_ceased",
  { data_type => "date", is_nullable => 1, timezone => "UTC" },
  "vestiging_latlong",
  { data_type => "point", is_nullable => 1 },
  "vestiging_bag_id",
  { data_type => "bigint", is_nullable => 1 },
  "preferred_contact_channel",
  {
    data_type => "enum",
    default_value => "pip",
    extra => {
      custom_type_name => "preferred_contact_channel",
      list => ["email", "mail", "pip", "phone"],
    },
    is_nullable => 1,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<bedrijf_uuid_key>

=over 4

=item * L</uuid>

=back

=cut

__PACKAGE__->add_unique_constraint("bedrijf_uuid_key", ["uuid"]);

=head1 RELATIONS

=head2 custom_object_relationships

Type: has_many

Related object: L<Zaaksysteem::Schema::CustomObjectRelationship>

=cut

__PACKAGE__->has_many(
  "custom_object_relationships",
  "Zaaksysteem::Schema::CustomObjectRelationship",
  { "foreign.related_organization_id" => "self.id" },
  undef,
);

=head2 gegevensmagazijn_subjectens

Type: has_many

Related object: L<Zaaksysteem::Schema::GegevensmagazijnSubjecten>

=cut

__PACKAGE__->has_many(
  "gegevensmagazijn_subjectens",
  "Zaaksysteem::Schema::GegevensmagazijnSubjecten",
  { "foreign.nnp_uuid" => "self.uuid" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-09-11 10:42:17
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:2MXeMIJ2brserHB84mNyow

__PACKAGE__->resultset_class('Zaaksysteem::SBUS::ResultSet::Bedrijf');

__PACKAGE__->load_components(
    "+Zaaksysteem::DB::Component::Bedrijf",
    __PACKAGE__->load_components()
);

__PACKAGE__->belongs_to(
  "verblijfsobject_id",
  "Zaaksysteem::Schema::BagVerblijfsobject",
  { "identificatie" => "verblijfsobject_id" },
  { is_foreign_key_constraint => 0 },
);

__PACKAGE__->belongs_to(
    "subscription_id",
    "Zaaksysteem::Schema::ObjectSubscription",
    # {
    #     #'foreign.local_table' => 'self_resultsource.name',
    #     'foreign.local_id'    => 'self.id'
    # }

    sub {
        my $args                    = shift;

        return {
            "$args->{foreign_alias}.local_table"        => { '=' => 'Bedrijf' },
            "$args->{foreign_alias}.local_id::NUMERIC"  => \"= $args->{self_alias}.id",
        }
    },
    { join_type => 'left' }

);

__PACKAGE__->inflate_column('main_activity', {
  inflate => sub { JSON::XS->new->decode(shift // '{}') },
  deflate => sub { JSON::XS->new->encode(shift // {}) },
});

__PACKAGE__->inflate_column('secondairy_activities', {
  inflate => sub { JSON::XS->new->decode(shift // '[]') },
  deflate => sub { JSON::XS->new->encode(shift // []) },
});

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

