use utf8;
package Zaaksysteem::Schema::FileCaseDocument;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::FileCaseDocument

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<file_case_document>

=cut

__PACKAGE__->table("file_case_document");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'file_case_document_id_seq'

=head2 file_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 case_document_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 magic_string

  data_type: 'text'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "file_case_document_id_seq",
  },
  "file_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "case_document_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "magic_string",
  { data_type => "text", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<file_case_document_file_id_case_document_id_key>

=over 4

=item * L</file_id>

=item * L</case_document_id>

=back

=cut

__PACKAGE__->add_unique_constraint(
  "file_case_document_file_id_case_document_id_key",
  ["file_id", "case_document_id"],
);

=head1 RELATIONS

=head2 case_document_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeKenmerken>

=cut

__PACKAGE__->belongs_to(
  "case_document_id",
  "Zaaksysteem::Schema::ZaaktypeKenmerken",
  { id => "case_document_id" },
);

=head2 file_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::File>

=cut

__PACKAGE__->belongs_to("file_id", "Zaaksysteem::Schema::File", { id => "file_id" });


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-10-05 16:41:39
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:lNWKi/hQCipzEh8qGBE+Vg

__PACKAGE__->belongs_to(
  "case_document",
  "Zaaksysteem::Schema::ZaaktypeKenmerken",
  { id => "case_document_id" },
);

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

