package Zaaksysteem::SAML2::Protocol::AuthnRequest;

use Moose;
use MooseX::Types::Moose qw/ Str /;
use MooseX::Types::URI qw/ Uri /;

use MIME::Base64 qw/ encode_base64 decode_base64 /;
use IO::Compress::RawDeflate qw/ rawdeflate /;
use IO::Uncompress::RawInflate qw/ rawinflate /;
use URI;
use URI::QueryParam;
use Crypt::OpenSSL::RSA;
use Zaaksysteem::Constants ':SAML_TYPES';

extends 'Net::SAML2::Protocol::AuthnRequest';
with 'Zaaksysteem::SAML2::Protocol::Role::RandomID';

use XML::Generator;

=head1 SAML2::Protocol::AuthnRequest

This module wraps L<Net::SAML2::Protocol::AuthnRequest> and relaxes a few
of the constraints that module asserts upon initialization. This is basically
nothing but an object that generates XML, with some parameter validation up
front.

=head1 Example usage

Constructor is called in default L<Moose> style.

    my $authnreq = Zaaksysteem::SAML2::Protocol::AuthnRequest->new(
        issuer => 'http://zaaksysteem.nl/auth/saml',
        nameid_format => 'urn:oasis:names:tc:SAML:2.0:nameid-format:entity'
        auth_protocol => 'urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport'
    );

    my $str = $authnreq->as_xml;

The resulting XML message will look something like this:

    <?xml version="1.0" standalone="yes"?>
    <samlp:AuthnRequest xmlns:samlp="urn:oasis:names:tc:SAML:2.0:protocol"
                        xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion"
                        ID="79db0f1de49412f14e01ece016af9dac"
                        Version="2.0"
                        IssueInstant="2013-07-12T13:05:00Z"
                        ProviderName="Zaaksysteem">
      <saml:Issuer>http://zaaksysteem.nl/auth/saml</saml:Issuer>
      <samlp:NameIDPolicy AllowCreate="1"
                          Format="urn:oasis:names:tc:SAML:2.0:nameid-format:entity" />
      <samlp:RequestedAuthnContext Comparison="minimum">
        <saml:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:MobileTwoFactorContract</saml:AuthnContextClassRef>
      </samlp:RequestedAuthnContext>
    </samlp:AuthnRequest>

=head1 Attributes

=head2 id

See L<Net::SAML2::Role::ProtocolMessage>

=head2 issue_instant

See L<Net::SAML2::Role::ProtocolMessage>

=head2 destination

This attribute is a redefinition of an attribute from
L<Net::SAML2::Protocol::AuthnRequest>, but in that package it is a required
non-undef attribute during XML message composition. Since this field isn't
actually required by the SAML 2.0 standard, we drop the requirement in this
module.

If no destination attribute is specified during object construction, the
field will not be serialized into the final XML message.

=cut

has 'interface' => (
    'is'        => 'rw',
);

=head2 auth_protocol

The level of security enforced by the SP. Meaning varies per IdP.

=cut

has 'auth_protocol' => (
    is          => 'rw',
    required    => 0,
    lazy        => 1,
    default     => sub {
        my $self = shift;
        my $interface_config = $self->interface->get_interface_config;

        return $interface_config->{authentication_level}
            unless $interface_config->{disable_requested_authncontext};

        return;
    }
);

has nameid_policy_format => (
    is => 'rw',
    isa => 'Maybe[Str]'
);

has base_url => (
    is => 'ro',
    isa => 'Str',
    required => 1
);

has service_identifiers => (
    is => 'rw',
    isa => 'HashRef[Str]',
    required => 1
);

has binding => (
    is => 'ro',
    isa => 'Str',
    required => 0,
    predicate => 'has_binding',
);

=head1 Methods

=head2 as_xml

Returns a L<XML::Generator::final> object representing the
C<AuthnRequest> XML message.

=cut

sub as_xml {
    my ($self) = @_;

    my $gen = XML::Generator->new(':std');

    my %ns = (
        saml => [qw[saml urn:oasis:names:tc:SAML:2.0:assertion]],
        samlp => [qw[samlp urn:oasis:names:tc:SAML:2.0:protocol]]
    );


    # The RequestedAuthnContext informs the IdP that we want a specific
    # security level for authentication of the user
    my @requestedAuthnContext;
    if ($self->auth_protocol) {
        @requestedAuthnContext = $gen->RequestedAuthnContext(
            $ns{ samlp },
            { Comparison => 'minimum' },
            $gen->AuthnContextClassRef($ns{ saml },
                $self->auth_protocol
            )
        );
    }

    my $ifconfig = $self->interface->get_interface_config;

    my @scope;
    if ($ifconfig->{saml_type} eq SAML_TYPE_EIDAS && $ifconfig->{idp_scoping}) {
        @scope = $gen->Scoping(
            $ns{samlp},
            $gen->IDPList(
                $ns{samlp}, $gen->IDPEntry($ns{samlp}, { ProviderID => $ifconfig->{idp_scoping} }),
            )
        );
    }

    # Generate a AuthnRequest XML message.
    return $gen->AuthnRequest(
        $ns{ samlp },

        {
            ID              => $self->random_id,
            IssueInstant    => $self->issue_instant,
            Version         => '2.0',

            $self->has_binding
                ? (ProtocolBinding => $self->binding)
                : (),

            # Leaky leaky style, XXX CLEANUP YO
            %{ $self->service_identifiers },

            # Only include the destination if it's set, this is an optional
            # SAML attribute on the AuthnRequest element, but Net::SAML2::*
            # asserts that it is required.
            $self->destination ? (Destination => $self->destination) : ()
        },

        $gen->Issuer($ns{ saml }, $self->issuer),

        $self->nameid_policy_format ? ($gen->NameIDPolicy($ns{ samlp }, {
            AllowCreate => 'true',
            Format => $self->nameid_policy_format
        })) : (),

        @requestedAuthnContext,
        @scope,
    );
}

=head2 finalize

This method allows inheriting modules to modify the AuthnRequest generated
before it is finalized (immutable).

=head3 Parameters

=over 4

=item C<XML::Generator> instance

The generator object that was used to generated the last argument to this method

=item A return value from C<XML::Generator>

The XML document being built, non-finalized.

=back

=cut

sub finalize {
    my $self = shift;

    return shift->xml(shift);
}

__PACKAGE__->meta->make_immutable();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 mangled_id

TODO: Fix the POD

=cut

