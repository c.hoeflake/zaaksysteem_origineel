package Zaaksysteem::Backend::Directory::ResultSet;
use Moose;
use Zaaksysteem::Constants;
use BTTW::Tools;
use BTTW::Tools::File qw(sanitize_filename);

extends 'DBIx::Class::ResultSet';
with 'MooseX::Log::Log4perl';

use Exception::Class (
    'Zaaksysteem::Backend::Directory::ResultSet::Exception' => {fields => 'code'},
    'Zaaksysteem::Backend::Directory::ResultSet::Exception::General' => {
        isa         => 'Zaaksysteem::Backend::Directory::ResultSet::Exception',
        description => 'General exception',
        alias       => 'throw_general_exception',
    },
    'Zaaksysteem::Backend::Directory::ResultSet::Exception::Parameter' => {
        isa         => 'Zaaksysteem::Backend::Directory::ResultSet::Exception',
        description => 'Parameter exception',
        alias       => 'throw_parameter_exception',
    },
);

=head2 directory_create

Create a Directory entry in the database.

=head3 Arguments

=over

=item name [required]

The name of the directory.

=item case_id [required]

The case this directory belongs to

=back

=head3 Returns

The newly created Directory object.

=cut

define_profile directory_create => (
    required => [qw/
        name
        case_id
    /],
    optional => [qw/
        original_name
    /],
    constraint_methods => {
        name => qr{^[^<>:"/\\|?*]+$},
        case_id => qr/^[0-9]+$/,
    },
);

sub directory_create {
    my ($self, $opts) = @_;

    my $valid = assert_profile($opts)->valid;

    # Check if a directory with this name and case already exists
    if ($self->find($opts)) {
        throw_general_exception(
            code  => '/directory/directory_create/directory_exists',
            error => sprintf('Found existing entry with id %d and name %s',
                ($opts->{case_id}, $opts->{name})
            ),
        );
    }

    unless ($self->validate_name($opts->{name})) {
        throw_parameter_exception(
            code  => '/directory/directory_create/invalid_parameters',
            error => 'directory_create: Invalid options given',
        );
    }

    return $self->create({
        case_id         => $opts->{case_id},
        original_name   => $opts->{original_name} || $opts->{name},
        name            => $opts->{name}
    });
}

=head2 validate_name

=head3 DESCRIPTION

a valid name will not be changed by the make_valid_name. We could split this
into two routines, but then the regexps would live on two places and we don't
like split personalities.

=cut

sub validate_name {
    my ($self, $name) = @_;

    my $validated = sanitize_filename($name);
    
    return 1 if $validated eq $name;

    $self->log->debug(sprintf("Given name '%s' does not match validated name '%s'", $name, $validated));

    return;
}

=head2 validify

Validify the directory name.

=cut

sub validify {
    my ($self, $arguments) = @_;

    my $case_id = $arguments->{case_id} or die "need case_id";
    my $name    = $arguments->{name} or die "need name";

    my $valid_name = $self->make_valid_name($name);

    return $self->uniqify($case_id, $valid_name);
}


=head2 uniqify

if the name is unique for the case, return it.

if a match is found, append a number on the end:

test (1)
test (2)
test (...)

until it is not found, in which case were done.

=cut

sub uniqify {
    my ($self, $case_id, $name) = @_;

    return $name unless $self->find({case_id => $case_id, name => $name});

    my ($body, undef, $index) = $name =~ m|^(.*?)(\s+\((\d+)\))?$|is;

    $index ||= 0;

    my $new_candidate = $body . " (" . ($index+1) . ")";

    return $self->uniqify($case_id, $new_candidate);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
