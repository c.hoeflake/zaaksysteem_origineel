package Zaaksysteem::Backend::Sysin::Modules::KoppelAppDocumentMasker;

use Moose;

use BTTW::Tools;
use BTTW::Tools::UA qw[new_user_agent];
use Zaaksysteem::Types qw(UUID);

use JSON;
use JSON::Path qw[];

use Moose::Util::TypeConstraints qw[union];

use Zaaksysteem::ZAPI::Form::Field;
use Zaaksysteem::ZAPI::Form;

use Zaaksysteem::API::v1::Serializer;
use Zaaksysteem::API::v1::Message::Ping;

use LWP::UserAgent;
use HTTP::Headers;
use HTTP::Request;

extends 'Zaaksysteem::Backend::Sysin::Modules';

# I'm cargo-culting the processor role here... anyone know why that's
# realy needed?
with qw[
    MooseX::Log::Log4perl
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
];

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::KoppelAppDocumentMasker;

=cut

use constant INTERFACE_ID => 'koppelapp_document_masker';

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_endpoint',
        type => 'text',
        label => 'Trigger URL',
        description => 'Voer hier een URL in waarnaar het Zaaksysteem document anonimisatieverzoek kan sturen',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_button_label',
        type => 'text',
        label => 'Knoptekst',
        description => 'Voer hier de knoptekst in die voor de knop in de documententab van een zaak gebruikt moet worden',
    )
];

use constant MODULE_SETTINGS => {
    name                          => INTERFACE_ID,
    label                         => 'Koppel.app Documentanonimisatie',
    interface_config              => INTERFACE_CONFIG_FIELDS,
    direction                     => 'outgoing',
    manual_type                   => ['text'],
    module_type                   => ['apiv1', 'api'],
    is_multiple                   => 1,
    is_manual                     => 1,
    retry_on_error                => 0,
    allow_multiple_configurations => 1,
    is_casetype_interface         => 0,
    has_attributes                => 0,
    test_interface  => 1,

    test_definition => {
        description => 'Hier kunt u de verbinding met de externe interface testen.',

        tests => [
            {
                id => 1,
                label => 'Test verbinding',
                name => 'test_connection',
                method => 'test_connection',
                description => 'Zaaksysteem zal proberen verbinding naar de externe partij op te zetten.'
            }
        ]
    },

    trigger_definition  => {
    },
};

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig(%{MODULE_SETTINGS()});
};

=head2 test_connection

This test case tests a configured trigger endpoint for connectability and
SSL certificate checks.

=cut

sub test_connection {
    my $self = shift;
    my $interface = shift;

    my $url = $interface->jpath('$.endpoint');

    unless ($url) {
        throw('api/v1/trigger/test_connection', 'Geen trigger URL geconfigureerd.');
    }

    return $self->test_host_port($url);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2020 Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
