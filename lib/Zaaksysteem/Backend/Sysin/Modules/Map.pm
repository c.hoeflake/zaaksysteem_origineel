package Zaaksysteem::Backend::Sysin::Modules::Map;
use Moose;
use namespace::autoclean;

use BTTW::Tools;
use Zaaksysteem::ZAPI::Form::Field;

use URI;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
/;

use Zaaksysteem::Object::Types::MapConfiguration;
use Zaaksysteem::Object::Types::WMSLayer;
use Zaaksysteem::Object::Types::XMLNamespace;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::Map - Map settings and WMS layer module

=head1 TESTS

    ./zs_prove -v t/lib/TestFor/General/Backend/Sysin/Modules/Map.pm

=head1 DESCRIPTION

In Zaaksysteem.nl it is possible to have MAP functionality. This functionality enables users to find addresses,
place coordinates on a map and add extra WMS services to the map.

=cut

use constant INTERFACE_ID => 'map';

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_map_application',
        type => 'select',
        label => 'Kaartapplicatie',
        default => 'builtin',
        data => {
            options => [
                {
                    label => "Intern",
                    value => "builtin",
                },
                {
                    label => "Extern (bijv. Esri)",
                    value => "external"
                },
            ],
        },
        required => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_map_application_url',
        type => 'url',
        when => 'interface_map_application === "external"',
        label => 'URL van de kaartapplicatie',
        description => 'URL waarop de HTML-wrapper van de externe kaartapplicatie staat. Deze HTML-wrapper moet het protocol voor externe (kaart-)applicaties ondersteunen. Zie <a href="https://zaaksysteem.gitlab.io/zaaksysteem-frontend-mono/?path=/story/readme-integrations-guide--map">de documentatie van het protocol</a> voor meer informatie.', 
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_map_use_in_case',
        type => 'checkbox',
        label => 'Gebruik kaart-applicatie voor kaart in zaak',
    ),

    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_wms_layers',
        type        => 'multiple',
        label       => 'WMS Kaartlaag services',
        description => "De Publieke Dienstverlening op de Kaart (PDOK) staat het gebruik van WMS lagen toe om extra functionaliteit aan uw kaart toe te voegen, zoals gemeentegrenzen, bag locaties, etc. Geef de naam van de laag, een label voor de laag, en de url naar de WMS service. Overzicht: https://www.pdok.nl/nl/producten/pdok-services/overzicht-urls/a",
        data        => {
            fields      => [
                {
                    name    => 'label',
                    label   => 'Titel voor de kaart',
                    type    => 'text',
                    data    => {
                        placeholder => 'Mijn Kaartlaag'
                    }
                },
                {
                    name    => 'id',
                    label   => 'Kaartlaag-naam',
                    type    => 'text',
                    description => "Naam van de kaartlaag die gebruikt moet worden, zoals beschreven in de &lt;label&gt;&lt;name&gt; tag in de GetCapabilities XML.",
                },
                {
                    name    => 'url',
                    label   => 'WMS basis-URL',
                    type    => 'text',
                    description => "URL van de WMS-service. Let op! Zonder het 'GetCapabilities'-deel",
                    data => {
                        pattern => 'https:\/\/.+',
                        placeholder => 'https://map.datapunt.amsterdam.nl/maps/parkeervakken?VERSION=1.1.0&SERVICE=wms'
                    }
                },
                {
                    name    => 'feature_info_xpath',
                    type    => 'text',
                    label   => 'Locatieattribuut XPath',
                    description => 'XPath (1.0) expressie waarmee voor exacte locaties extra informatie opgehaald kan worden uit de kaartlaag.',
                    data => {
                        placeholder => q"/table[@class='featureInfo']/tr[2]/td[2]/text()",
                    }
                },
                {
                    name    => 'active',
                    label   => 'Actief',
                    type    => 'checkbox',
                }
            ]
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_xml_namespaces',
        type        => 'multiple',
        label       => 'XML-namespaces',
        description => 'Voor de verwerking van XPath-expressies moeten prefixes gedefinieerd worden voor <a target="_blank" href="https://en.wikipedia.org/wiki/XML_namespace">XML-namespaces</a>. Deze prefixes worden in XPath-expressies gebruikt om aan te geven dat een element uit een bepaalde namespace gezocht wordt.',
        data        => {
            fields      => [
                {
                    name    => 'prefix',
                    label   => 'Prefix',
                    type    => 'text',
                    description => "Prefix",
                    data    => {
                        placeholder => 'gml'
                    }
                },
                {
                    name    => 'namespace_uri',
                    label   => 'Namespace-URI',
                    type    => 'text',
                    description => "URI van de XML-namespace",
                    data => {
                        placeholder => 'http://www.opengis.net/gml',
                    },
                },
            ]
        }
    ),
];

use constant MODULE_SETTINGS => {
    name                          => INTERFACE_ID,
    label                         => 'Kaartconfiguratie',
    interface_config              => INTERFACE_CONFIG_FIELDS,
    direction                     => 'incoming',
    manual_type                   => ['text'],
    is_multiple                   => 0,
    is_manual                     => 0,
    retry_on_error                => 0,
    allow_multiple_configurations => 0,
    is_casetype_interface         => 0,
    has_attributes                => 0,
    attribute_list => [
    ],
    trigger_definition => {
        get_ol_settings => {
            method  => 'get_ol_settings',
            api     => 1
        },
    },
    test_interface  => 1,
    test_definition => {
        description => qq{
            Om te controleren of de applicatie goed geconfigureerd is, kunt u
            hieronder een aantal tests uitvoeren. Hiermee controleert u de verbinding
            van uw profiel.
        },
        tests => [
            {
                id          => 1,
                label       => 'Test verbinding',
                name        => 'connection_test',
                method      => 'maps_test_connection',
                description => 'Test verbinding naar kaart-URLs'
            }
        ],
    },
};

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};

=head1 METHODS

=head2 get_ol_settings

=over 4

=item Arguments: \%params [, $INTEFACE_ROW ]

=item Return Value: L<Zaaksysteem::Object::Types::MapConfiguration>

=back

    $obj->get_ol_settings(undef, $interface)

Returns an instance of L<Zaaksysteem::Object::Types::MapConfiguration> for API v1

=cut

sub get_ol_settings {
    my ($self, $params, $interface) = @_;

    my $interface_config = $interface->get_interface_config;
    my %new_params;

    $new_params{map_application} = $interface_config->{map_application} // 'builtin';
    $new_params{map_application_url} = $interface_config->{map_application_url} // '';
    $new_params{use_object_map_in_case} = $interface_config->{map_use_in_case} // JSON::false;

    if (my $wms_layers = $interface->jpath('$.wms_layers')) {
        my @list_of_wms_layers;
        for my $layer (@$wms_layers) {
            try {
                my $uri = URI->new($layer->{url});
                my %wms_layer_args = (
                    uri        => $uri,
                    layer_name => $layer->{id},
                );

                $wms_layer_args{ $_ } = $layer->{ $_ } for qw[
                    label active
                ];

                if (defined $layer->{ feature_info_xpath }) {
                    $wms_layer_args{ feature_info_xpath } = $layer->{ feature_info_xpath };
                }

                push @list_of_wms_layers, Zaaksysteem::Object::Types::WMSLayer->new( %wms_layer_args );
            } catch {
                $self->log->info('sysin/modules/map: Could not instantiate a map layer: invalid config: ' . $_);
            };
        }

        $new_params{wms_layers} = \@list_of_wms_layers;
    }

    if (my $xml_namespaces = $interface->jpath('$.xml_namespaces')) {
        my @list_of_namespaces;

        for my $namespace (@$xml_namespaces) {
            push @list_of_namespaces, Zaaksysteem::Object::Types::XMLNamespace->new(
                prefix        => $namespace->{prefix},
                namespace_uri => $namespace->{namespace_uri},
            );
        }
        $new_params{xml_namespaces} = \@list_of_namespaces;
    }

    my $schema = $interface->result_source->schema;
    if (
        (my $lat = $schema->resultset('Config')->get('customer_info_latitude')) &&
        (my $lng = $schema->resultset('Config')->get('customer_info_longitude'))
    ) {
        $new_params{map_center} = "$lat,$lng";
    }

    my $settings;
    try {
        $settings = Zaaksysteem::Object::Types::MapConfiguration->new( %new_params );
    } catch {
        $self->log->info('sysin/modules/map: Could not instantiate map configuration: invalid config: ' . $_);
    };

    return $settings;
}

=head2 maps_test_connection

Runs a connection test on every configured map URL.

=cut

sub maps_test_connection {
    my ($self, $interface) = @_;

    for my $layer (@{ $interface->jpath('$.wms_layers') }) {
        # Not specifying a CA certificate means "use system CA store"
        $self->test_host_port_ssl($layer->{url});
    }

    return;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
