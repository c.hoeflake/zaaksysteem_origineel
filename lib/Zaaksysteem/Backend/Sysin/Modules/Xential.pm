package Zaaksysteem::Backend::Sysin::Modules::Xential;
use Moose;

use Try::Tiny;
# This will not work on precise unless we start with a cpanfile
# use JSON::MaybeXS;
use JSON::XS;

use BTTW::Tools;
use Zaaksysteem::Xential;
use Zaaksysteem::ZAPI::Form::Field;
use Zaaksysteem::ZAPI::Form;
use BTTW::Tools::File qw(sanitize_filename);

use Zaaksysteem::Types qw/UUID/;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    Zaaksysteem::Backend::Sysin::Modules::Roles::BaseURI
/;


=head1 INTERFACE CONSTANTS

=head2 INTERFACE_ID

=head2 INTERFACE_CONFIG_FIELDS

=head2 MODULE_SETTINGS

=cut

=head1 Interface Properties

Below a list of interface properties, see
L<Zaaksysteem::Backend::Sysin::Modules> for details.

=cut

use constant INTERFACE_ID => 'xential';

use constant INTERFACE_CONFIG_FIELDS    => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_infoservice',
        type        => 'text',
        label       => 'InfoService API',
        required    => 1,
        description => 'De URL van de InfoService API van xential. Let op, dit moet een beveiligde (HTTPS) url zijn.',
        data        => { pattern => '^https:\/\/.+' },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_buildservice',
        type        => 'text',
        label       => 'BuildService API',
        required    => 1,
        description => 'De URL van de BuildService API van xential. Let op, dit moet een beveiligde (HTTPS) url zijn.',
        data        => { pattern => '^https:\/\/.+' },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_contextservice',
        type        => 'text',
        label       => 'ContextService API',
        required    => 1,
        description => 'De URL van de ContextService API van xential. Let op, dit moet een beveiligde (HTTPS) url zijn.',
        data        => { pattern => '^https:\/\/.+' },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_editservice',
        type        => 'text',
        label       => 'Edit API',
        required    => 0,
        description => 'Vul in wanneer u tevens documenten via xential wilt bewerken.',
        data        => { pattern => '^https:\/\/.+' },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_contextservice_require_base_id',
        type        => 'checkbox',
        label       => 'BaseID moet verzonden worden voor ContextService',
        required    => 0,
        description => 'Heeft Xential de BaseID nodig'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_contextservice_base_id',
        type        => 'text',
        label       => 'BaseID',
        when        => 'interface_contextservice_require_base_id === true',
        required    => 1,
        description => 'De baseID om de context mee op te zetten',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_contextservice_username',
        type        => 'text',
        label       => 'Gebruikersnaam',
        required    => 1,
        description => 'De gebruikersnaam om de context mee op te zetten',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_contextservice_password',
        type        => 'password',
        label       => 'Wachtwoord',
        required    => 1,
        description => 'Het wachtwoord om de context mee op te zetten',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_uri',
        type => 'display',
        label => 'Pingback URI',
        description => 'De genoteerde URI is de locatie waar de externe partij met het Zaaksysteem kan communiceren',
        required => 0,
        data => {
            template => '<a href="<[field.value]>" target="_blank"><[field.value]></a>'
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_timeout',
        type        => 'text',
        label       => 'Timeout',
        default     => '15',
        required    => 1,
        description => 'Timeout',
        data        => { pattern => '^\d+$' },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_spoofmode',
        type        => 'checkbox',
        label       => 'Spoofmode',
        required    => 0,
        description => 'Laat Zaaksysteem xential antwoorden spoofen. Handig voor development.',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'attribute_export_uri',
        type => 'display',
        label => 'Kenmerkschema',
        description => 'Het kenmerkschema wordt door Xential gebruikt om zaak-kenmerken te koppelen in sjablonen.',
        required => 0,
        data => {
            template => '<a href="<[field.value]>" target="_blank">Exporteer kenmerkschema</a>'
        }
    ),
];

use constant MODULE_SETTINGS            => {
    name                            => INTERFACE_ID,
    label                           => 'xential documentgenerator',
    interface_config                => INTERFACE_CONFIG_FIELDS,
    direction                       => 'incoming',
    manual_type                     => ['text'],
    is_multiple                     => 1,
    is_manual                       => 0,
    allow_multiple_configurations   => 0,
    is_casetype_interface           => 0,
    has_attributes                  => 0,
    attribute_list                  => [ ],
    retry_on_error                  => 1,
    trigger_definition  => {
        api_post_file   => {
            method  => 'api_post_file',
            api     => 1,
        },
        api_get_file   => {
            method  => 'api_get_file',
            api     => 1,
        },
        create_file_from_template   => {
            method  => 'create_file_from_template',
        },
        spoofmode   => {
            method  => 'spoof_mode',
        },
        request_edit_file       => {
            method  => 'request_edit_file',
        }
    },
};

has uri_values => (
    is      => 'ro',
    isa     => 'HashRef',
    default => sub {
        {
            interface_uri => "/api/v1/sysin/interface/UUID/trigger/api_post_file",
            attribute_export_uri => "/beheer/bibliotheek/export/xml",
        }
    },
);

=head2 BUILDARGS

Settings for module, see

=cut

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};

=head2 create_file_from_template

Creates a document based on a template which is hosted at Xential

=cut

sub create_file_from_template {
    my ($self, $params, $interface) = @_;

    return $interface->process({
            external_transaction_id => 'unknown',
            input_data              => 'external_template',
            processor_params        => {
                processor => '_create_file_from_template',
                %$params,
            },
        },
    );
}


=head2 api_post_file

Post a file via API system

B<request_params>

=over

=item transaction_id

=item application_uuid

=back

=cut

define_profile api_post_file => (
    required            => {
        transaction_uuid        => UUID,
        case_uuid               => UUID,
    },
    optional            => {
        file_uuid               => UUID,
    }
);

sub api_post_file {
    my ($self, $options, $interface) = @_;
    my $params  = assert_profile($options->{request_params})->valid;
    my $uploads = $options->{uploads};

    throw(
        'sysin/modules/xential/no_post',
        'Invalid HTTP method, needs to be post' . $options->{method}
    ) unless $options->{method} eq 'post';

    throw(
        'sysin/modules/xential/no_uploads',
        'No uploads set'
    ) unless ($uploads && keys %{ $uploads });

    my $schema      = $interface->result_source->schema;
    my $transaction = $schema->resultset('Transaction')
        ->search({ uuid => $params->{transaction_uuid} })->first;

    throw('sysin/modules/xential/transaction_not_found',
        'No transaction found matching the given transaction_uuid')
        unless ($transaction);

    my $user_id             = $transaction->get_processor_params->{subject};
    my $case_id             = $transaction->get_processor_params->{case};
    my $filename            = $transaction->get_processor_params->{document_title};
    my $case_document_id    = $transaction->get_processor_params->{case_document_ids};

    # When updating a file (e.g.: creating a new version for it), make sure we do not get
    # a set of files...this won't work (obviously)
    my $file;
    if ($params->{file_uuid}) {
        if (scalar values %{ $options->{uploads} } > 1) {
            throw(
                'sysin/modules/xential/cannot_update_multiple_files',
                'Cannot update multiple files when file_uuid is given'
            );
        }

        $file = $self->_get_file_from_transaction({
            filestore_uuid      => $params->{file_uuid},
            transaction_uuid    => $transaction->uuid,
            interface           => $interface,
        });

        $case_id    = $file->get_column('case_id');
        $filename   = $file->filename;
    }

    ### Simple AUTHORIZATION: Match application_uuid from processor_params
    my $case = $self->_get_case(schema => $schema, case => $case_id);
    throw(
        'sysin/modules/xential/invalid_request',
        'Request invalid, no match with case_uuid'
    ) unless ($case->get_column('uuid') eq $params->{case_uuid});

    my $subject = $self->_get_subject(schema => $schema, id => $user_id);

    my @ids;
    for my $upload (values %{ $options->{uploads} }) {
        if ($file) {
            my $newfile = $file->update_file({
                subject       => $subject->as_object,
                original_name => $upload->filename,
                new_file_path => $upload->tempname,
            });

            ### Update file_id in processor params, so we can edit this file again.
            {
                my $proparams = $transaction->get_processor_params;

                $proparams->{request} = {
                    filestore_uuid => $newfile->filestore_id->uuid
                };
                $transaction->processor_params($proparams);
                $transaction->update;
            }

            push(@ids, $newfile->id);
        } else {
            my $schema = $interface->result_source->schema;

            my $fs = $schema->resultset('Filestore')->filestore_create(
                {
                    original_name => $upload->filename,
                    file_path     => $upload->tempname,
                }
            );

            my %create_args = (
                db_params => {
                    created_by   => $subject->betrokkene_identifier,
                    accepted     => 1,
                    case_id      => $case->id,
                    generator    => "xential",
                    filestore_id => $fs->id,
                },
                name      => sprintf('%s%s', $filename, $fs->extension),
                file_path => $upload->tempname,
                $case_document_id ? (case_document_ids => [ $case_document_id ]) : (),

            );

            my $file = $schema->resultset('File')->file_create(\%create_args);

            ### Update interface_config on "sjabloon generation", so we can edit this specific file
            {
                my $proparams = $transaction->get_processor_params;

                $proparams->{request} = {
                    filestore_uuid => $file->filestore_id->uuid
                };
                $transaction->processor_params($proparams);
                $transaction->update;
            }
            push(@ids, $file->id);
        }
    }

    ### Make sure we place a notification
    if ($file) {
        ### Updated document
        ### TODO No message yet

    } else {
        ### Created sjabloon
        $schema->resultset('Message')->message_create(
            {
                event_type  => 'xential/sjabloon',
                case_id     => $case_id,
                subject_id  => $subject->betrokkene_identifier,
                message     => (@ids > 1 ? 'Sjablonen' : 'Sjabloon') . " toegevoegd aan zaak: " . $filename,
                data        => {
                    file_ids         => \@ids,
                    filename         => $filename,
                }
            }
        );
    }

    return $schema->resultset('File')->search({ 'id' => { in => \@ids} });
}

=head2 request_edit_file

    $interface->process_trigger('request_edit_file',
        {
            file_id             => $file->id,           # required
            current_username    => $c->user->username,  # required
        }
    );

B<Return value>: \%RESULT

Registers a file for editing, returns the resume url for Xential.

B<\%RESULT>

=over

=item redirect_url

B<Type>: URL

The URL to redirect to.

=back

=cut

define_profile request_edit_file => (
    required    => {
        file_id             => 'Int',
        subject             => 'Str',
    }
);

sub request_edit_file {
    my ($self, $params, $interface) = @_;

    my $editservice = $interface->get_interface_config->{editservice};

    unless ($editservice) {
        throw('/sysin/modules/xential/no_edit_service', 'No edit service configured');
    }

    my $file        = $interface->result_source->schema->resultset('File')->search({ 'me.id' => $params->{file_id} }, { prefetch => [ 'case_id','filestore_id' ] })->first;
    unless ($file) {
        throw('/sysin/modules/xential/file_not_found', sprintf("No case associated or file with id %d not found", $params->{file_id}));
    }

    $params->{filestore_uuid}   = $file->filestore_id->uuid;
    $params->{case_uuid}        = $file->case_id->get_column('uuid');

    ### We leave an empty transaction: this way we can accept files on this transaction later
    my $transaction = $interface->process({
        external_transaction_id => 'unknown',
        input_data              => 'user_requested_edit',
        processor_params        => {
            processor   => '_process_request_edit_file',
            request     => $params,
            subject     => $params->{subject},
        },
    });

    # Refresh, make sure the UUID is set;
    $transaction = $transaction->discard_changes();

    my %params = (
        transaction_uuid    => $transaction->uuid,
        interface_uuid      => $interface->uuid,
        file_uuid           => $params->{filestore_uuid},
        case_uuid           => $params->{case_uuid},
        mimetype            => $file->filestore_id->mimetype,
    );

    ### Generate URL
    my $uri = URI->new($editservice);

    my %query_params = (
        $uri->query_form,
        %params,
    );

    $uri->query_form(\%query_params);

    return {
        redirect_url => $uri->as_string,
    };

}

=head2 _process_request_edit_file

Processor for an edit request, see L<request_edit_file>

=cut

sub _process_request_edit_file {
    my ($self, $record) = @_;

    my $transaction = $self->process_stash->{transaction};
    my $params      = $transaction->get_processor_params()->{request};

    my $rv          = {
        transaction_id  => $record->get_column('transaction_id'),
    };

    $transaction->processor_params({
        %{ $transaction->get_processor_params },
        result  => $rv,
    });

    $transaction->input_data("Gebruiker wil document bewerken");

    return 1;
}

=head2 api_get_file

    $interface->process_api_trigger('api_get_file',
        {
            file_uuid           => $filestore->uuid,    # required
            transaction_uuid    => $transaction->uuid,  # required
        }
    );

B<Return value>: $FILESTORE_ROW

Returns a file for download. Make sure you registered the file for downloading
via L<request_edit_file>

=cut

define_profile api_get_file => (
    required    => {
        file_uuid           => UUID,
        transaction_uuid    => UUID,
    }
);

sub api_get_file {
    my ($self, $params, $interface) = @_;

    my $file = $self->_get_file_from_transaction(
        {
            transaction_uuid    => $params->{request_params}->{transaction_uuid},
            filestore_uuid      => $params->{request_params}->{file_uuid},
            interface           => $interface,
        }
    );

    return $file;
}

=head2 _get_file_from_transaction

=cut

define_profile _get_file_from_transaction => (
    required    => {
        transaction_uuid    => UUID,
        interface           => 'Zaaksysteem::Model::DB::Interface',
    },
    optional    => {
        filestore_uuid      => UUID,
    }
);

sub _get_file_from_transaction {
    my $self        = shift;
    my $params      = assert_profile(shift || {})->valid;
    my $interface   = $params->{interface};

    my $schema = $interface->result_source->schema;
    my $transaction     = $schema->resultset('Transaction')->search(
        {
            "me.uuid"               => $params->{transaction_uuid},
            "interface_id.module"   => 'xential',
        },
        {
            join    => "interface_id"
        }
    )->first;

    throw('/sysin/modules/xential/invalid_file', 'Given params compare false with our data: transaction')
        unless $transaction;

    my $trans_params    = $transaction->get_processor_params->{request};

    ## Matches file_uuid?
    if (($trans_params->{filestore_uuid}// '') ne ($params->{filestore_uuid} // '')) {

        $self->log->info(
            sprintf(
                "Xential file upload UUID '%s' does not match transaction UUID '%s'",
                $params->{filestore_uuid} // '',
                $trans_params->{filestore_uuid} // '',
            )
        );

        throw('/sysin/modules/xential/invalid_file', 'Given params compare false with our data: file');
    }

    my $file = $schema->resultset('File')->search(
        {
            'filestore_id.uuid' => $trans_params->{filestore_uuid}
        },
        { join => 'filestore_id' }
    )->first;

    return $file if $file;

    throw('/sysin/modules/xential/file_not_found',
        "File with id $trans_params->{filestore_uuid} not found");
}

=head2 spoof_mode

A spoofmode like trigger so we can test some functionality of the interface without having to rely on Xential

=cut

sub spoof_mode {
    my ($self, $params, $interface) = @_;

    return $interface->process(
        {
            external_transaction_id => 'unknown',
            input_data              => 'external_template',
            processor_params        => {
                processor => '_spoof_xential_result',
                %$params,
            }
        }
    );
}

sub _log_params {
    my ($self, $params) = @_;
    if ($self->log->is_trace) {
        $self->log->trace(dump_terse($params));
    }
}

=head2 set_record

Helper method to set the in and output of a transaction record.

=cut

sub set_record {
    my ($self, $record, $input, $output) = @_;

    if ($input) {
        $input = encode_json($input);
        $self->log->trace("Setting record input to '$input'");
        $record->input($input);
    }

    if ($output) {
        my $status = (defined $output->{status} && $output->{status} eq 'done')
            ? 'silent'
            : 'resumable';

        $self->set_record_output(
            $record, $output,
            sprintf(
                "Xential document '%s' built as '%s' for user %s in case %d",
                ($output->{document}{title} || ''),
                ($output->{status} || '') eq 'done' ? 'silent' : 'resumable',
                ($output->{username} || ''), ($output->{case} || 0),
            )
        );
    }
    return 1;
}

=head2 set_record_output

Helper method to set the output of a transaction record.

=cut

sub set_record_output {
    my ($self, $record, $msg, $preview) = @_;

    my $ref = ref $msg;
    if ($ref =~ /^(?:HASH|ARRAY)$/) {
        $msg = encode_json($msg);
    }
    $self->log->trace("Setting record output to '$msg'");

    $record->output($msg);

    if (length($preview //'')) {
        $msg = $preview;
        $self->log->trace("Setting preview to '$msg'");
    }
    if (length($msg) > 195) {
        $msg = substr($msg, 0, 195) . "...";
        $self->log->trace("Cutting preview short to '$msg'");
    }
    $record->preview_string($msg);
    return 1;
}

sub _spoof_xential_result {
    my ($self, $record) = @_;

    my $transaction = $self->process_stash->{transaction};
    my $params      = $transaction->get_processor_params();

    try {

        $self->_log_params($params);

        my $interface = $transaction->interface;
        my $schema = $interface->result_source->schema;

        my $case = $self->_get_case(schema => $schema, case => $params->{case});

        my $uuid = $params->{template_external_name};

        my $want = {
            case          => $case->id,
            template_uuid => $uuid,
            document      => {
                id           => $transaction->id,
                title        => $params->{document_title},
                ticket_uuid  => $uuid,
                xential_uuid => $uuid,
                start_url    => $self->new_uri(
                    $interface->get_interface_config->{uri}, 'StartURI'
                ),
                resume_url => $self->new_uri(
                    $interface->get_interface_config->{uri}, 'ResumeURI'
                ),
            },
            status   => 'done',
            build_id => $transaction->id,
            transaction_id => $transaction->id,
        };

        $self->set_record($record, $params, $want);
    }
    catch {
        $self->_catch_error($record, $_);
    };
}

sub _get_case {
    my ($self, %opts) = @_;

    my $case = $opts{schema}->resultset('Zaak')->search_extended({'me.id' => $opts{case}})->first;
    return $case if $case;
    throw('xential/case/not_found', "No case with id $opts{case} found");
}

sub _get_subject {
    my ($self, %opts) = @_;

    my $s = $opts{schema}->resultset('Subject')->find($opts{id});
    return $s if $s;
    throw('xential/subject/not_found', "No subject with id $opts{id} found");
}

sub _create_file_from_template {
    my ($self, $record) = @_;

    my $transaction = $self->process_stash->{transaction};
    my $params      = $transaction->get_processor_params();

    try {

        $self->_log_params($params);

        my $xential = Zaaksysteem::Xential->new_from_interface($transaction->interface);
        my $schema = $xential->schema;

        my $case = $self->_get_case(
            schema => $schema,
            case   => $params->{case}
        );

        my $subject = $self->_get_subject(
            schema => $schema,
            id     => $params->{subject}
        );

        my $result = $xential->create_document_from_template(
            case           => $case,
            template_uuid  => $params->{template_external_name},
            document_title => $params->{document_title},
            transaction    => $transaction,
            username       => $subject->username,
        );

        $self->set_record($record, $params, $result);
    }
    catch {
        $self->_catch_error($record, $_);
    };
}

sub _catch_error {
    my ($self, $record, $e) = @_;
    if (eval {$e->isa('Throwable::Error')}) {
        $self->set_record_output($record, $e->as_string);
    }
    # ClamAv::Error::Client errors
    elsif (eval {$e->isa('Error::Simple')}) {
        $self->set_record_output($record, $e->stringify);
    }
    else {
        $self->set_record_output($record, $e);
    }
    die $e;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
