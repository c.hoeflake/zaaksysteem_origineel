package Zaaksysteem::Backend::Object::Data::Component;

use Moose;

extends 'Zaaksysteem::Backend::Component';
with 'Zaaksysteem::Backend::Object::Roles::ObjectComponent';

use Moose::Util qw/apply_all_roles/;
use Moose::Util::TypeConstraints qw[enum];

use BTTW::Tools;
use Encode qw/encode_utf8/;
use JSON;
use Zaaksysteem::Object::Constants;
use Zaaksysteem::Types qw[UUID];
use List::Util qw(any);

=head1 NAME

Zaaksysteem::Backend::Object::Data::Component - Returns a DBIx::Class row with Object principals

=head1 SYNOPSIS

    my $row = $resultset->first;

    print $row->uuid;
    print $row->object_class;
    print $row->object_id;
    print $row->properties;
    print $row->hstore_index;

    ### Prints every attribute name available in this object
    print $_->name for @{$row->object_attributes}

    ### Updates $row->hstore_index.
    $row->reload_index;
    $row->update

    ### Or: quicker
    $row->update_index

=head1 DESCRIPTION

Row returned when row is requested from L<Zaaksysteem::Backend::Object::Data::ResultSet>. It
is inflated with the principles of our Object Management system.

=head1 DATABASE COLUMNS

=head2 uuid

Return value: UUID

Returns the UUID of this row

=head2 object_class

Return value: $STRING_OBJECT_CLASS_NAME

Returns the name of this object class, like C<case>

=head2 object_id

Return value: $INTEGER_FOR_OBJECT_CLASS

For some object classes we need a incrementing serial, object_id will contain this value. For
instance, for C<case> this will be the case number.

=head2 properties

Return value: HashRef of object properties

Will contain an HashRef of object properties

=head2 hstore_index

Return value: Pg HSTORE string

Will return the PostgreSQL C<hstore> representation of this object

=head2 text_vector

This is a C<tsvector> column used for full-text searches on the object data.

I have no idea yet what it's gonna return. Some list of tokens in the best case.

=head2 date_created

Return value: $DATETIME

Will return the timestamp of row creation

=head2 date_modified

Return value: $DATETIME

Will return the timestamp of the last time this row had been updated

=head1 ATTRIBUTES

=head2 acl_capabilities

An ArrayRef of capabilities for this row, choose from:

    read
    write
    manage
    search

=cut

has 'acl_capabilities' => (
    is  => 'rw',
    isa => 'ArrayRef',
    lazy => 1,
    default => sub { [] },
);


=head2 object_attributes

Return value: ArrayRef[L<Zaaksysteem::Object::Attribute>]

Returns an ArrayRef of L<Zaaksysteem::Object::Attribute> objects,
defining the attributes for this object.

B<Note>: this attribute should be regarded as read-only, or pay the price of
runtime corruption of object-data!

=cut

has object_attributes => (
    is      => 'rw',
    lazy    => 1,
    isa     => 'ArrayRef[Zaaksysteem::Object::Attribute]',
    builder => '_build_attribute_list',
    clearer => '_clear_object_attributes',
);

sub _build_attribute_list {
    my $self                    = shift;
    my @rv;

    return \@rv unless ($self->properties && $self->properties->{values});

    for my $value (values %{ $self->properties->{values} }) {
        push @rv, Zaaksysteem::Object::Attribute->new(
            %$value,

            # Don't put parent_object in $value, as that will create a
            # circular reference that can't be reaped by Perl's GC.
            $value->{dynamic_class}
                ? (parent_object => $self)
                : (),
        );
    }

    # Special attributes for easy querying
    push @rv, Zaaksysteem::Object::Attribute->new(
        name           => 'object.id',
        label          => 'Herkomstobjectnummer',
        attribute_type => 'text',
        value          => $self->object_id,
    );

    push @rv, Zaaksysteem::Object::Attribute->new(
        name           => 'object.uuid',
        label          => 'Objectidentificatie',
        attribute_type => 'text',
        value          => $self->uuid,
    );

    push @rv, Zaaksysteem::Object::Attribute->new(
        name           => 'object.date_created',
        label          => 'Aanmaakdatum',
        attribute_type => 'timestamp',
        value          => $self->date_created,
    );

    push @rv, Zaaksysteem::Object::Attribute->new(
        name           => 'object.date_modified',
        label          => 'Aanpassingsdatum',
        attribute_type => 'timestamp',
        value          => $self->date_modified,
    );

    push @rv, Zaaksysteem::Object::Attribute->new(
        name           => 'object.invalid',
        label          => 'Corrupt',
        attribute_type => 'integer',
        value          => $self->invalid
    );

    return \@rv;
}

=head1 METHODS

=head2 update_mutations

=cut

sub update_mutations {
    my ($self, $object_type, $user, @mutations) = @_;

    my @retval;

    my @updated_mutations = grep { defined $_->{ id }     } @mutations;
    my @other_mutations   = grep { not defined $_->{ id } } @mutations;

    my $mutations = $self->object_mutation_lock_object_uuids->search({
        object_type => $object_type
    });

    while (my $frame = $mutations->next) {
        # Find the exising mutation in the posted list
        my ($mutation) = grep { $frame->id eq $_->{ id } } @updated_mutations;

        unless (defined $mutation) {
            warn "pruning $frame";
            $frame->delete;

            next;
        }

        $frame->values($mutation->{ values });

        push @retval, $frame->update;
    }

    for my $mutation (@other_mutations) {
        push @retval, $self->object_mutation_lock_object_uuids->create({
            lock_object_uuid => $self->uuid,
            type             => $mutation->{ type },
            object_uuid      => $mutation->{ object_uuid },
            object_type      => $object_type,
            values           => $mutation->{ values },
            subject_id       => $user->id,
        });
    }

    return @retval;
}

=head2 add_mutation

=cut

define_profile add_mutation => (
    required => { type => 'Str' },
    optional => {
        id => UUID,
        object_uuid => UUID,
        values => 'HashRef'
    },
);

sub add_mutation {
    my ($self, $user, $object_type, $mutation_args) = @_;

    my $opts = assert_profile($mutation_args)->valid;

    unless($opts->{ type } eq 'create') {
        my $clean_mutations = $self->object_mutation_lock_object_uuids->result_source->resultset;

        if($clean_mutations->search({ object_uuid => $opts->{ object_uuid } })->count) {
            throw('object/mutation/target_locked', sprintf(
                'Unable to add mutation, the target object is already locked'
            ));
        }
    }

    $self->_assert_mutation_values($opts->{values}) if exists $opts->{values};

    return $self->object_mutation_lock_object_uuids->create({
        lock_object_uuid => $self->uuid,
        object_type      => $object_type,
        subject_id       => $user->id,

        %{ $opts }
    });
}

=head2 update_mutation

This method updates an existing mutation.

    my $mutation = $object->update_mutation($c->user, { id => '...', values => { } });

=head3 Return value

Returns an updated L<Zaaksysteem::Schema::ObjectMutation> object if succesful.

=head3 Parameters

This method expects positional arguments.

=over 4

=item user

A reference to the L<user object|Zaaksysteem::Schema::Subject> responsible
for the update.

=item mutation_args

This argument is expected to be a hashref that would validate as arguments for
a L<Zaaksysteem::Schema::ObjectMutation> row.

=back

=head3 Exceptions

This method does not try/catch exceptions from further along the callstack
in addition to the exceptions thrown directly from this method, as listed
below.

=over 4

=item object/mutation/update

This exception is thrown when no mutation could be resolved by the provided
identifier.

=back

=cut

define_profile update_mutation => (
    required => {
        id => UUID,
        type => 'Str'
    },
    optional => {
        object_uuid => UUID,
        values => 'HashRef'
    },
);

sub update_mutation {
    my ($self, $user, $mutation_args) = @_;

    my $opts = assert_profile($mutation_args)->valid;
    my $id = delete $opts->{ id };

    $opts->{ subject_id } = $user->id;

    my $mutations = $self->object_mutation_lock_object_uuids->search({ id => $id });

    unless ($mutations->count) {
        throw('object/mutation/update', sprintf(
            'Unable to find mutation by ID "%s"',
            $id
        ));
    }

    $self->_assert_mutation_values($opts->{values}) if exists $opts->{values};

    $mutations->first->update($opts);
}

=head2 delete_mutation

=cut

sub delete_mutation {
    my ($self, $id) = @_;

    $self->object_mutation_lock_object_uuids->search({ id => $id })->delete;
}

=head2 replace_object_attributes

This method replaces the value of the L</properties> hash with data from the
provided L<Zaaksysteem::Object::Attribute>(s) provided.

    my $success = $object->replace_object_attributes(@attrs);

=cut

sub replace_object_attributes {
    my $self = shift;
    my @object_attributes = @_;

    my %values = map { $_->name, $_ } @object_attributes;

    my %properties = %{ $self->properties };
    $properties{values} = \%values;

    $self->properties(\%properties);

    return 1;
}

=head2 grant

This method modifies the ACLs linked to this object by granting the entity
specific capabilities through a positive permission.

    $object->grant($subject, qw[read write]);

The above example would grant the C<read> and C<write> capabilities for the
provided entity.

B<< This method is deprecated in favor of the
L<Zaaksysteem::Object::Roles::Security> mechanism. >>

=cut

define_profile grant => (
    required => {
        capabilities => 'Str'
    },
    optional => {
        groupname => 'Str',
        scope => enum([qw[instance type]])
    }
);

sub grant {
    my ($self, $entity, %params) = @_;

    my $opts = assert_profile(\%params)->valid;

    return unless $entity;

    my $acl = $self->result_source->schema->resultset('ObjectAclEntry');

    unless ((blessed $entity && $entity->can('security_identity')) || ref $entity eq 'HASH') {
        throw(
            'object/acl/entity',
            'Provided entity (%s) doesn\'t implement security identity'
        );
    }

    my %sec_id = ref $entity eq 'HASH' ? %{ $entity } : $entity->security_identity;

    my @ret;

    for my $capability (@{ $opts->{ capabilities } }) {
        for my $type (keys %sec_id) {
            push @ret, $acl->create({
                object_uuid => $self->uuid,
                entity_type => $type,
                entity_id => $sec_id{ $type },
                capability => $capability,
                groupname => $opts->{ groupname },
                scope => $opts->{ scope } // 'instance'
            });
        }
    }

    return @ret;
}

=head2 revoke

This method modified the ACLs linked to this object by revoking the entity
specific capabilities

    $object->revoke($subject, $verdict, qw[read write]);

The above example would revoke the C<read> and C<write> capabilities for
the entity provided.

B<< This method is deprecated in favor of the
L<Zaaksysteem::Object::Roles::Security> mechanism. >>

=cut

sub revoke {
    my $self = shift;
    my ($entity, $verdict, @capabilities) = @_;

    return unless $entity;

    unless ($entity->can('security_identity')) {
        throw(
            'object/acl/entity',
            'Provided entity doesn\'t implement security identity'
        );
    }

    # Play nicely here, revoking no capabilities is safe
    return unless scalar @capabilities;

    my ($type, $id) = $entity->security_identity;

    return $self->result_source->schema->resultset('ObjectAclEntry')->search({
        object_uuid => $self->uuid,
        entity_type => $type,
        entity_id => $id,
        verdict => $verdict,
        capability => [ @capabilities ]
    })->delete_all;
}

=head2 has_object_attribute

=over 4

=item Arguments: $ATTRIBUTE_NAME

=item Return Value: $TRUE_ON_SUCCESS

=back

    if ($row->has_object_attribute('attribute.kenteken')) {
        print "GOT IT!"
    }

Checks whether the given attribute exists in this object row

=cut

sub has_object_attribute {
    my $self = shift;
    my $attr = shift;

    return (exists $self->properties->{ values }->{ $attr } ? 1 : 0);
}

=head2 $row->get_object_attribute($name)

Retrieve a specific object attribute by its name.

=cut

sub get_object_attribute {
    my $self = shift;
    my ($attribute_name) = @_;

    my $values = $self->properties->{ values };

    unless ($self->has_object_attribute($attribute_name)) {
        #
        # Allow missing object attributes to be missing
        #
        my %could_be_missing = (
            'case.current_deadline' => {
                label => 'Current phase deadline',
                value => {},
            },
            'case.deadline_timeline' => {
                label => 'Phase deadline timeline',
                value => [],
            },
            'case.result_id' => {
                attribute_type => 'integer',
                human_label    => 'Resultaat ID',
                human_value    => '',
                value          => undef,
            }

        );
        if (any { $_ eq $attribute_name } keys %could_be_missing) {
            return Zaaksysteem::Object::Attribute->new(
                name => $attribute_name,
                attribute_type => 'text',
                %{$could_be_missing{$attribute_name}},
            );
        }

        throw('object/data/attribute', sprintf(
            'Unable to find attribute "%s" in object "%s"',
            $attribute_name,
            $self->TO_STRING
        ));
    }

    my $args = $values->{ $attribute_name };

    # Some deep code may replace the values with attribute instances
    # before code gets here, if blessed assume that's happened and return
    # the existing object
    return $args if blessed $args;

    my %params = %{ $args };

    if($params{ dynamic_class }) {
        $params{ parent_object } = $self;
    }

    if(!$params{ label } && $params{ human_label }) {
        $params{ label } = $params{ human_label };
    }

    return Zaaksysteem::Object::Attribute->new(%params);
}

=head2 relationships_data

Returns an array of object relation data for serialization processes.

The L<Zaaksysteem::Schema::ObjectRelationships> table is somewhat verbose and
ambiguous when it comes to which side of the relationship is encoded in which
fields. This method discards the side coming to the instance it's called on
and only returns data regarding the 'other' side of the relationship.

=cut

sub relationships_data {
    my $self = shift;

    my $relation_rs = $self->result_source->schema->resultset('ObjectRelationships')->search(
        {
            -or => [
                { object1_uuid => $self->id },
                { object2_uuid => $self->id }
            ]
        },
        {
            join => [qw[object1_uuid object2_uuid]],
            '+select' => [qw[object1_uuid.object_id object2_uuid.object_id]],
            '+as' => [qw[object1_id object2_id]]
        }
    );

    my @relations;

    while (my $relation = $relation_rs->next) {
        if ($relation->get_column('object1_uuid') eq $self->id) {
            push @relations, {
                related_object_id => $relation->get_column('object2_id'),
                related_object_uuid => $relation->get_column('object2_uuid'),
                related_object_type => $relation->get_column('object2_type'),
                relation_type => $relation->get_column('type2'),
            };
        } else {
            push @relations, {
                related_object_id => $relation->get_column('object1_id'),
                related_object_uuid => $relation->get_column('object1_uuid'),
                related_object_type => $relation->get_column('object1_type'),
                relation_type => $relation->get_column('type1')
            }
        }
    }

    return @relations;
}

=head2 $row->reload_index()

Return value: $TRUE_ON_SUCCESS

Will reload the hstore_index according to the object properties. PLEASE make sure you
call update after this, or it won't be saved in the database. Use C<<$row->update_index>>
if you want to update the index on the database.

=cut


sub reload_index {
    my $self                        = shift;

    # Clear "object_attributes", so they'll be regenerated from next time around.
    $self->_clear_object_attributes();

    $self->load_hstore;
    $self->load_text_vector unless $self->is_column_changed('text_vector');

    # Hardcoded behavior required for ZS-2353. Valid until we rewrite the
    # catalogue code into being Zaaksysteem::Object-native.
    if ($self->can('bibliotheek_entry_args')) {
        $self->result_source->schema->resultset('ObjectBibliotheekEntry')->create_or_update($self);
    }

    return 1;
}

=head2 $row->load_hstore()

Return value: $row->hstore_index

Will set C<hstore_index> with the correct hstore_representation of C<<$row->object_attributes>>

=cut

sub load_hstore {
    my $self = shift;

    my $hr = {
        map {
            my $index_value = $_->index_value;
            if (ref($index_value) eq 'ARRAY') {
                # 0x1E is "RS" (record separator) in ASCII
                $index_value = join("\x1E", map { defined($_) ? $_ : '' } @{$index_value});
            }

            # XXX: PostgreSQL cries if an index value is longer than 2712 bytes
            if(defined $index_value && !ref($index_value)) {
                $index_value = substr($index_value, 0, 1000);
            }

            my $key = $_->name //
                throw(
                    'hstore/empty_key',
                    'Attempt to store a value in hstore with an empty key'
                );

            ($key => $index_value);
        } grep {
            !$_->dynamic_class
        } @{ $self->object_attributes }
    };

    $self->index_hstore($hr);
}

=head2 load_text_vector

Updates the C<text_vector> column with 'smartly' gathered strings.

This method uses C<text_vector_terms> to retrieve terms to set the
column to.

Takes a list of terms to include in the C<TSVECTOR> column (can be empty).

Does B<not> automatically C<< $row->update >>.

=cut

sub load_text_vector {
    my $self = shift;

    $self->text_vector(
        join(
            ' ',
            grep { length > 2 } (
                $self->text_vector_terms,
                $self->object_class,
                @_
            )
        )
    );
}

=head2 text_vector_terms

This method is mainly here to seperate the logic of collecting the terms
from the logic of updating the text vector. Extend and override all ye
want, just make sure it does something like this:

    my @strings = $row->text_vector_terms

=cut

sub text_vector_terms { }

=head2 inherited_acl_entries

Retrieve all inherited L<Zaaksysteem::Schema::ObjectAclEntry> objects. This
method checks for type-level objects in the current C<acl_groupname> owned by
this row's L</class_uuid>, if it has any.

Return value is deferred from L<DBIx::Class::ResultSet/search>.

=cut

sub inherited_acl_entries {
    my $self = shift;

    my $class_uuid = $self->get_column('class_uuid');

    return unless $class_uuid;

    return $self->object_acl_entries->result_source->resultset->search({
        object_uuid => $class_uuid,
        scope => 'type',
        groupname => $self->acl_groupname
    })
}

=head2 trigger_logging

Wrapper for L<Zaaksysteem::DB::ResultSet::Logging/trigger>.

=cut

sig trigger_logging => 'Str, ?HashRef';

sub trigger_logging {
    my $self = shift;
    my $type = shift;
    my $fields = shift // {};

    $fields->{ object_uuid } = $self->uuid;
    $fields->{ component } = $self->object_class;

    return $self->loggings->trigger($type, $fields);
}

=head2 $row->get_source_object()

Retrieve the source object. Should be implemented in the object_class-specific
roles.

=cut

sub get_source_object { }

sub TO_STRING {
    my $self = shift;

    my $id = $self->uuid ?
        sprintf('...%s', substr($self->uuid, -6)) :
        'unsynched';

    return sprintf('%s(%s)', $self->object_class, $id);
}

=head1 INTERNAL METHODS

=head2 inflate_result

Return value: $result

Will apply roles to this Row object according to the object_class.

See L<Zaaksysteem::Object::Constants> for a list of available
extensions/object types.

=over

=item * case

Will load role L<Zaaksysteem::Backend::Object::Data::Roles::Case> when
object_class is C<case>

=item * domain

Will load role L<Zaaksysteem::Backend::Object::Data::Roles::Domain> when
object_class is C<domain>

=back

=cut

sub inflate_result {
    my $self                        = shift;
    my $row                         = $self->next::method(@_);

    return $row unless $row->object_class;

    if (my $role = OBJECT_TYPES->{ $row->object_class }) {
        apply_all_roles($row, $role);
    }

    return $row;
};

=head2 update

Return value: L<DBIx:Class::Row>::update

This method will update the row in the database, and makes sure the hstore_index keeps up to date.

=cut

sub update {
    my $self = shift;
    my $properties = shift;

    if(defined $properties && ref $properties eq 'HASH' && exists $properties->{ properties }) {
        $self->properties($properties->{ properties });
    }

    $self->reload_index;

    return $self->next::method(@_);
};

=head2 insert

Return value: L<DBIx:Class::Row>::insert

This method will insert the row in the database, and makes sure the
hstore_index keeps up to date.

=cut

sub insert {
    my $self                        = shift;

    my $row                         = $self->next::method(@_);

    if (my $role = OBJECT_TYPES->{ $row->object_class }) {
        apply_all_roles($row, $role);
    }

    $row->reload_index;
    $row->update;

    return $row;
}



sub has_permission {
    my $self = shift;
    my $want = shift;

    if (any { $_ eq $want } @{$self->acl_capabilities}) {
        return 1;
    }
    return 0;
}

sub _assert_mutation_values {
    my ($self, $values) = @_;

    foreach my $k (%$values) {
        next if ref($values->{$k}) ne 'ARRAY';
        foreach (@{ $values->{$k} }) {
            next if defined $_;
            throw("object/mutation/values/array/undef",
                "Unable to use undef values within an array on value: $k"
            );
        }
    }
}


1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, 2018 Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
