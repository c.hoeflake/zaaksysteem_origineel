package Zaaksysteem::Backend::Object::Queue::ResultSet;

use Moose;

use BTTW::Tools;
use Moose::Util::TypeConstraints qw[enum];
use Zaaksysteem::Types qw[UUID];

extends 'Zaaksysteem::Backend::ResultSet';

with qw[
	MooseX::Log::Log4perl
];

=head1 NAME

Zaaksysteem::Backend::Object::Queue::ResultSet - Additional behavior for the
C<queue> table

=head1 DESCRIPTION

=head1 METHODS

=head2 create_item

Wrapper for L<DBIx::Class::ResultSet/create>. This method mostly exists to
provide a stable API for the module should the underlying storage change.

    my $item = $queue_rs->create_item('my_queued_item_type_name', {
        object_id => UUID,
        label => 'Str',
        data => { ... }
    });

=cut

define_profile create_item => (
    required => {
        label => 'Str'
    },
    optional => {
        object_id    => UUID,
        data         => 'HashRef',
        priority     => 'Int',
        status       => enum([qw[pending waiting postponed]]),
        subject_id   => 'Int',
        metadata     => 'HashRef',
        singleton    => 'Bool',
    },
    defaults => {
        priority     => 1000,
        status       => 'pending',
        singleton    => 0,
    },
);

sig create_item => 'Str, HashRef';

sub create_item {
    my $self = shift;
    my $type = shift;
    my $opts = assert_profile(shift)->valid;

    $opts->{ type } = $type;

    if ($opts->{subject_id}) {
        $opts->{metadata}{subject_id} = delete $opts->{subject_id};
    }
    $opts->{metadata}{require_object_model} //= 1;
    $opts->{metadata}{target} //= "backend";

    if (delete $opts->{ singleton }) {
        my $data;

        if (exists $opts->{ data }) {
            $data = $self->_deflate_column_value('data', $opts->{ data });
        }

        my $compare_rs = $self->search({
            type => $opts->{ type },
            object_id => $opts->{ object_id },
            priority => $opts->{ priority },
            status => 'pending',
            data => $data
        });

        if ($compare_rs->count) {
            $self->log->info(sprintf(
                'Not creating %s queue item, a waiting item of the same content already exists',
                $opts->{ type },
            ));

            return;
        }
    }

    $self->create($opts)->discard_changes;
}

=head2 create_items

Wrapper for L<DBIx::Class::ResultSet/populate>. This method mostly exists to
provide a stable API for the module should the underlying storage change.

    my $item = $queue_rs->create_items(
        'my_queued_item_type_name',
        [
            {
                object_id => UUID,
                label     => 'Str',
                data      => {...}
            }
        ]
    );

=cut

=head2 create_items

    my @items = ( { label => "foo", data => { foo => bar }, object_id => UUID }, {} );
    $resultset->create_items($type, @items);

Creates queue items in bulk (think thousands).

=cut

define_profile create_items => (
    required => {
        label => 'Str'
    },
    optional => {
        object_id => UUID,
        data => 'HashRef',
        priority => 'Int',
    },
    defaults => {
        priority => 1000,
    },
);

sig create_items => 'Str, @HashRef';

sub create_items {
    my $self = shift;
    my $type = shift;

    # Force to scalar, otherwise you get the keys from the valid hashref
    my @rows = map { scalar assert_profile($_)->valid } @_;

    @rows = map { [$type, $_->{object_id}, $self->_deflate_column_value('data', $_->{data}), $_->{label}, $_->{priority}] } @rows;
    $self->populate([
        [qw(type object_id data label priority)],
        @rows,
    ]);

    return;
}

=head2 search_pending

Returns a queue resultset with 'pending' queue items since a specific
L<DateTime>.

    # Search for queue items pending since before yesterday.
    my $pending_rs = $queue_rs->search_pending(DateTime->yesterday());

=cut

sig search_pending => 'DateTime';

sub search_pending {
    my $self = shift;
    my $since = shift;

    # search_rs required because sig() borks our wantarray() state
    return $self->search_rs(
        {
            status => 'pending',
            date_created => { '<=' => $since->iso8601 }
        },
        {
            order_by => [
                { -desc => 'priority' },
                { -asc => 'date_created' }
            ]
        }
    );
}

=head2 queue_item

Adds a queue item to the broadcast queue. The queue will be broadcast, in
bulk, at the end of a request cycle using
L<Zaaksysteem::Controller::Root/broadcast_queued_items>.

=cut

sig queue_item => 'Zaaksysteem::Backend::Object::Queue::Component';

sub queue_item {
    my $self = shift;
    my $item = shift;

    $self->log->trace(sprintf('Queueing item for broadcast: %s', $item->stringify));

    my $schema = $self->result_source->schema;

    unshift @{ $schema->default_resultset_attributes->{ queue_items } }, $item;

    return;
}

=head2 queued_items

Returns a list of queue items to be broadcast.

=cut

sub queued_items {
    my $self = shift;

    my $schema = $self->result_source->schema;
    my $items = $schema->default_resultset_attributes->{ queue_items };

    return $items ? @{ $items } : ();
}

=head2 reset_queue

Removes the current queue of items to be broadcast.

=cut

sub reset_queue {
    my $self = shift;

    $self->log->trace('Resetting broadcast queue');

    my $schema = $self->result_source->schema;

    $schema->default_resultset_attributes->{ queue_items } = [];

    return;
}

=head1 PRIVATE METHODS

=head2 _deflate_column_value

Deflates a value as described by the schema file

It is a similar, simplified, function as
L<DBIx::Class::InflateColumn/_deflate_column>. So we
don't need to maintain two seperate decoding/deflating mechanisms.

=cut

sub _deflate_column_value {
    my $self = shift;
    my $col = shift;
    my $val = shift;

    my $info = $self->result_source->column_info($col);

    return $val unless $info;
    return $val unless exists $info->{_inflate_info}{deflate};

    return $info->{_inflate_info}{deflate}->($val, $self);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
