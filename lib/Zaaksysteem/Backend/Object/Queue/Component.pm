package Zaaksysteem::Backend::Object::Queue::Component;

use Moose;

extends 'DBIx::Class';
with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Backend::Object::Queue::Component - Behaviors for queue item
instances

=head1 DESCRIPTION

=cut

use BTTW::Tools;
use JSON::XS;
use URI;
use Zaaksysteem::StatsD;

=head1 METHODS

=head2 insert

Forward to the next "insert" in the component chain, after logging the insert
using StatsD.

=cut

sub insert {
    my $self = shift;

    Zaaksysteem::StatsD->statsd->increment('insert_queue_item.count');
    return $self->next::method(@_);
}


=head2 is_finished

Returns true if the item has succesfully been run.

=cut

sub is_finished {
    return shift->status eq 'finished';
}

=head2 is_failed

Returns true if the item has met a failure while being run.

=cut

sub is_failed {
    return shift->status eq 'failed';
}

=head2 is_pending

Returns true if the item is still pending to be run.

=cut

sub is_pending {
    return shift->status eq 'pending';
}

=head2 is_running

Returns true if the item is currently being run.

=cut

sub is_running {
    return shift->status eq 'running';
}

=head2 is_waiting

Returns true if the item is currently waiting to be run. This implies that
another queue item or process is governing the item, and it should not be run
independently.

=cut

sub is_waiting {
    return shift->status eq 'waiting';
}

=head2 is_ready

Returns true if the item is ready to be run. (C<pending> or C<waiting>).

=cut

sub is_ready {
    my $self = shift;

    return $self->is_pending || $self->is_waiting;
}

=head2 object_data

Convenience method for retrieving a
L<Zaaksysteem::Backend::Object::Data::Component> instance.

=head3 Exceptions

=over 4

=item queue/run/object_data_required

Thrown when queue item has no related object_data row

=back

=cut

sub object_data {
    my $self = shift;

    unless ($self->get_column('object_id')) {
        throw('queue/run/object_data_required', sprintf(
            'Running queue item "%s" requires a related object_data instance',
            $self->stringify
        ));
    }

    return $self->object_id;
}

=head2 subject

Convenience method for retrieving a
L<Zaaksysteem::Backend::Subject::Component> instance.

=cut

sub subject {
    my $self = shift;

    my $id = $self->subject_id;

    return unless $id;

    return $self->result_source->schema->resultset('Subject')->find($id);
}

=head2 subject_id

Retrieves the item's subject association identifier.

=cut

sub subject_id {
    my $self = shift;

    if (exists $self->metadata->{ subject_id }) {
        return $self->metadata->{ subject_id };
    }

    return unless exists $self->data->{ _subject };

    $self->log->debug(sprintf(
        'Deprecation: queue_item->data->{ _subject } used in item "%s"',
        $self->stringify
    ));

    return $self->data->{ _subject };
}

=head2 has_disable_acl

Convenience method for knowing if the queue component must use a object_model without a user

=cut

sub has_disable_acl {
    my $self = shift;
    return $self->metadata->{disable_acl} ? 1 : 0 ;
}

=head2 requires_object_model

Convenience method for knowing if the queue component must use a object model

=cut

sub requires_object_model {
    my $self = shift;

    # Can be removed once all the queue items have been processed after
    # this release.
    return 1 unless exists $self->metadata->{require_object_model};

    return $self->metadata->{require_object_model} ? 1 : 0 ;
}

=head2 target

Convenience method for knowing what the target is of the item
=cut

sub target {
    my $self = shift;
    return $self->metadata->{target};
}


=head2 stringify

Returns a human readable(-ish) string with most identifiying information of
the queued item, formatted like C<type(id): label>.

=cut

sub stringify {
    my $self = shift;

    return sprintf(
        '%s(%s): %s',
        $self->type,
        $self->id // '<not synced>',
        $self->label
    );
}

=head2 TO_JSON

JSON Serialization helper for the Queue ZAPI controller.

=cut

sub TO_JSON {
    my $self = shift;

    my %fields = map { $_ => $self->get_column($_) } qw[
        id
        label
        object_id
        status
        type
    ];

    $fields{ data } = $self->data;

    return \%fields;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
