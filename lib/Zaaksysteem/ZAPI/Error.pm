package Zaaksysteem::ZAPI::Error;

use Moose;

with qw/
    Zaaksysteem::ZAPI::Error::Profile
/;

=head1 NAME

Zaaksysteem::ZAPI::Error - Generates a correct ZAPI Error

=head1 SYNOPSIS

    my $error    = Zaaksysteem::ZAPI::Response->new(
        resultset       => $c->model('DB::Zaak')->search_extended(),
        uri_prefix      => $c->uri_for('/zaak'),
        page_size    => $c->req->params->{next} || 1,
    )

    $c->res->body($response->response);


=head1 DESCRIPTION

To make sure every response is valid, this object validates the input
parameters and implements a correct return HASHREF.

=cut

use Zaaksysteem::ZAPI::Response;
use BTTW::Tools;

=head1 CONSTRUCTORS

=head2 new_from_error

=cut

sub new_from_error {
    my $class = shift;
    my $error = shift;
    my $type = shift || 'unknown';

    if (blessed $error && $error->isa('BTTW::Exception::Base')) {
        return $class->new(
            type => $error->type,
            messages => [ $error->message ],
            data => [ $error->object ],
            stacktrace => [ $error->trace_frames ],
            debug => $error->debug
        );
    }

    if (blessed $error && $error->isa('Throwable::Error')) {
        return $class->new(
            type => $type,
            messages => [ $error->message ],
            stacktrace => [
                map { $_->as_string } $error->stack_trace->frames
            ]
        );
    }

    return $class->new(
        type => $type,
        messages => [ "$error" ]
    );
}

=head1 CONSTANTS

=head2 ZAPI_ERROR_KEYS

Declares the fieldnames/keys available in error instances

    messages, type, stacktrace, data

=cut

use constant    ZAPI_ERROR_KEYS => [
    qw/
        messages
        type
        stacktrace
        data
    /
];

=head2 ZAPI_ERROR_REQUIRED

Declares required fieldnames/keys.

    type, messages

=cut

use constant    ZAPI_ERROR_REQUIRED  => [
    qw/
        type
        messages
    /
];

=head1 ATTRIBUTES

=head2 debug

Debug state attribute, initialized from C<$ENV{ CATALYST_DEBUG} || $ENV{ ZS_TEST }>.

=cut

has debug => (
    is => 'ro',
    default => sub { $ENV{ CATALYST_DEBUG } || $ENV{ ZS_TEST } }
);

=head2 status_code

Holds the HTTP status code for the error instance. Defaults to C<500>.

=cut

has status_code => (
    is => 'ro',
    default => '500'
);

=head2 messages

Holds an array of contextual error messages.

=head2 type

Holds the type of the error.

=head2 stacktrace

Optionally holds a stacktrace from where the error occurred

=head2 data

Optionally holds additional context-sensitive data.

=cut

has [@{ ZAPI_ERROR_KEYS() }] => (
    is      => 'rw'
);

=head1 METHODS

=head2 response

Builder for L<Zaaksysteem::ZAPI::Response> instances.

=cut

sub response {
    my $self        = shift;

    $self->_validate_response;

    my $rep_object  = {
        map { $_ => $self->$_ }
            @{ ZAPI_ERROR_KEYS() }
    };

    if(blessed $self->stacktrace) {
        $rep_object->{ stacktrace } = [ map { $_->as_string } $self->stacktrace->frames ];
    }

    ### Do we want to expose a stack trace? Guess not
    $rep_object->{stacktrace} = [] unless $self->debug;

    return Zaaksysteem::ZAPI::Response->new(
        status_code => $self->status_code,
        unknown     => [ $rep_object ],
    )->response;
}

sub _validate_response {
    my $self        = shift;

    if (
        (my @filled_attrs = grep { $self->$_ } @{ ZAPI_ERROR_REQUIRED() })
            != scalar( @{ ZAPI_ERROR_REQUIRED() })
    ) {
        throw(
            'zapi/error/validation_failure',
            'Missing required attributes: ' . join(',',
                grep { !$self->$_ } @{ ZAPI_ERROR_REQUIRED() }
            )
        );
    }

    if ($self->data && !UNIVERSAL::isa($self->data, 'ARRAY')) {
        die('Not a valid ARRAY for data attribute: ' . Data::Dumper::Dumper($self->data));
    }

    if ($self->messages && !UNIVERSAL::isa($self->messages, 'ARRAY')) {
        $self->messages([$self->messages]);
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, 2017 Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
