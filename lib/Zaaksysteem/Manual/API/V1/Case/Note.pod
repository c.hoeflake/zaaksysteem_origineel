=head1 NAME

Zaaksysteem::Manual::API::V1::Case::Note - Case note CRUD

=head1 Description

This document describes the usage of the JSON API for personal notes on cases
("case/note" objects). Using this API, you can create, retrieve, update and delete
such notes.

=head2 API

This API is based on the Zaaksysteem "v1" API. More information about the way this
API works can be found in L<Zaaksysteem::Manual::API::V1>. Please read that document
before continuing here.

=head2 URL

The base URL for this API is:

    /api/v1/case/[UUID]/note

Use the HTTP C<GET> method for retrieving data, and C<POST> to submit changes.

=head1 Retrieval

=head2 list

    /api/v1/case/[UUID]/note

Returns a list of all notes for the specified case that are visible for the current
user. The C<result> property will contain a pageable B<set> of C<case/note>
objects.

B<Example call>

    GET https://localhost/api/v1/case/3011eef2-b994-47c1-b813-6bc1eb1806be/note

B<Request JSON>

As this is a GET request, a request body is not required/allowed.

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "development" : true,
   "request_id" : "mintlab-063581-0de4a6",
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 2,
            "total_rows" : 2
         },
         "rows" : [
            {
               "instance" : {
                  "case" : {
                     "instance" : null,
                     "reference" : "48aab6f4-d561-4a9c-a880-e65b31892e25",
                     "type" : "case"
                  },
                  "content" : "This is a test note",
                  "date_created" : "2017-03-02T10:14:22Z",
                  "date_modified" : "2017-03-02T10:14:22Z",
                  "owner_id" : "61d6851c-6c7a-4d0d-9b90-b2639283d6e5"
               },
               "reference" : "8ba8fac4-9a06-457b-825f-ef1b3579fa35",
               "type" : "case/note"
            },
            {
               "instance" : {
                  "case" : {
                     "instance" : null,
                     "reference" : "48aab6f4-d561-4a9c-a880-e65b31892e25",
                     "type" : "case"
                  },
                  "content" : "This is another note to test case note functionality",
                  "date_created" : "2017-03-02T10:14:07Z",
                  "date_modified" : "2017-03-02T10:35:45Z",
                  "owner_id" : "61d6851c-6c7a-4d0d-9b90-b2639283d6e5"
               },
               "reference" : "f88d4300-0c93-4cbc-b4bd-08143db3e96e",
               "type" : "case/note"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=head2 get

    /api/v1/case/[UUID]/note/[UUID]

Returns a specific note for a case, identified by its UUID.

The C<result> property will contain a single C<case/note> object.

B<Example call>

    GET https://localhost/api/v1/case/3011eef2-b994-47c1-b813-6bc1eb1806be/note/f88d4300-0c93-4cbc-b4bd-08143db3e96e

B<Request JSON>

As this is a GET request, a request body is not required/allowed.

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "development" : true,
   "request_id" : "mintlab-063581-0de4a6",
   "result" : {
       "instance" : {
           "case" : {
               "instance" : null,
               "reference" : "48aab6f4-d561-4a9c-a880-e65b31892e25",
               "type" : "case"
           },
           "content" : "This is another note to test case note functionality",
           "date_created" : "2017-03-02T10:14:07Z",
           "date_modified" : "2017-03-02T10:35:45Z",
           "owner_id" : "61d6851c-6c7a-4d0d-9b90-b2639283d6e5"
       },
       "reference" : "f88d4300-0c93-4cbc-b4bd-08143db3e96e",
       "type" : "case/note"
   },
   "status_code" : 200
}

=end javascript

=head1 Modify data

Modifications should be sent using HTTP C<POST>. The input data for these changes
is a JSON object containing the required parameters. When no input is needed,
make sure to send an empty object (C< {} >).

Mutation calls return the changed object, or the complete list of objects in case
of C<delete>.

=head2 create

    /api/v1/case/[UUID]/note/create

Creates a new note for the specified case, owned by the current user.

B<Example call>

    GET https://localhost/api/v1/case/3011eef2-b994-47c1-b813-6bc1eb1806be/note/create

B<Request JSON>

=begin javascript

{
    "content": "Content of the note goes here"
}

=end javascript

B<Response JSON>

See L</get>

=head2 update

    /api/v1/case/[UUID]/note/[UUID]/update

Creates a new note for the specified case, owned by the current user.

B<Example call>

    POST https://localhost/api/v1/case/3011eef2-b994-47c1-b813-6bc1eb1806be/note/19d6fe7b-2989-4f52-850f-73b95894521b/update

B<Request JSON>

=begin javascript

{
    "content": "New content"
}

=end javascript

B<Response JSON>

See L</get>

=head2 delete

    /api/v1/case/[UUID]/note/delete/[UUID]

Removes a note.

B<Example call>

    POST https://localhost/api/v1/case/3011eef2-b994-47c1-b813-6bc1eb1806be/note/19d6fe7b-2989-4f52-850f-73b95894521b/delete

B<Request JSON>

=begin javascript

{}

=end javascript

B<Response JSON>

See L</list>

=head1 Support

The data in this document is supported by the following test. Please make sure
you use the API as described in this test. Any use of this API outside the
scope of this test is B<unsupported>

L<TestFor::Catalyst::API::V1::Case>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
