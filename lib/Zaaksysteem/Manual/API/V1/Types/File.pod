=head1 NAME

Zaaksysteem::Manual::API::V1::Types::File - Type definition for C<file> objects

=head1 DESCRIPTION

This page documents the serialization of
L<C<file>|Zaaksysteem::Object::Types::File> object instances.

The C<file> type is a low-level API, which mostly functions as a 'blob'
storage mechanism, do not expect high-level/file-type specific operations
here.

=head1 JSON

=begin javascript

{
    "reference": "11deb02f-d001-46c4-b9df-f4c5a1fb2d03",
    "type": "file",
    "instance": {
        "name": "some_file_name.ext",
        "size": 31137,
        "mimetype": "text/plain",
        "md5": "e5199316748f31141d21498a29b25a7c"
    }
}

=end javascript

=head1 INSTANCE ATTRIBUTES

=head2 name E<raquo> L<C<string>|Zaaksysteem::Manual::API::V1::ValueTypes/string>

Filesystem-friendly file of the original file. This field is usually set to
the 'original' name of the file, as Zaaksysteem received it during an upload
operation.

=head2 size E<raquo> L<C<size>|Zaaksysteem::Manual::API::V1::ValueTypes/size>

Content length of the file in bytes.

=head2 mimetype E<raquo> L<C<string>|Zaaksysteem::Manual::API::V1::ValueTypes/string>

L<Mimetype|https://www.iana.org/assignments/media-types/media-types.xhtml> of
the file content.

This field is derived from the uploaded data, and must not be taken as
guaranteed truth (OOXML files have been known to show up as C<application/zip>
for example).

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
