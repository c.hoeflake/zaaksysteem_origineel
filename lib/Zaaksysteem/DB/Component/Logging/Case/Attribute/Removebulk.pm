package Zaaksysteem::DB::Component::Logging::Case::Attribute::Removebulk;

use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


sub onderwerp {
    my $self = shift;

    my $onderwerp = sprintf(
        'Kenmerken "%s" verwijderd: %s.',
        $self->data->{ attributes },
        $self->data->{ reason }
    );

    # db field = varchar(255)
    # we could make a larger field in the db but it has a big impact,
    # need to consider performance, disk space and migrate.
    # for a limit.
    #
    # also this fix should be applied to the parent.
    #
    if (length $onderwerp > 255) {
        $onderwerp = substr($onderwerp, 0, 252) . '...';
    }

    return $onderwerp;
}

sub event_category { 'case-mutation'; }

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 event_category

TODO: Fix the POD

=cut

=head2 onderwerp

TODO: Fix the POD

=cut

