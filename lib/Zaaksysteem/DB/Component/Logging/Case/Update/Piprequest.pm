package Zaaksysteem::DB::Component::Logging::Case::Update::Piprequest;

use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


sub onderwerp {
    my $self = shift;

    my $new_values = $self->data->{ new_values };

    my $description = "Behandelaar heeft wijzigingsvoorstel " .
        ($self->data->{ action } eq 'approve' ? 'goedgekeurd' : 'afgekeurd') .
        " voor kenmerk '" .
        $self->data->{kenmerk} . "' met de volgende waarde: '".
        (ref $new_values ? join (", ", @$new_values) : $new_values) . "'";

    return $description;
}


1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 onderwerp

TODO: Fix the POD

=cut

