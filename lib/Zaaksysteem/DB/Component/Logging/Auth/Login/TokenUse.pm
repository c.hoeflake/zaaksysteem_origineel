package Zaaksysteem::DB::Component::Logging::Auth::Login::TokenUse;
use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


=head1 NAME

Zaaksysteem::DB::Component::Logging::Auth::Login::TokenUse - Log line processor for "login token used" log lines

=head1 SYNOPSIS

    my $log = $logging_rs->trigger(
        'auth/login/token_use',
        {
            data => { subject_id => 'XXX', invitation_id => 'YYY' }
        }
    )

=head1 ATTRIBUTES

=head2 subject

Attribute that creates a L<Zaaksysteem::Backend::Subject::Component> instance
given the subject-id in the log data.

=cut

has subject => ( is => 'ro', lazy => 1, default => sub {
    my $self = shift;

    $self->rs('Subject')->find($self->data->{ subject_id });
});

=head1 METHODS

=head2 onderwerp

Formats the data contained in this log item in a human-readable way.

=cut

sub onderwerp {
    my $self = shift;

    return sprintf(
        'Medewerker van Zaaksysteem.nl ingelogd als gebruiker "%s". Id: "%s"',
        ($self->subject ? $self->subject->username : ''),
        substr($self->data->{invitation_id}, 0, 6) . "..."
    );
}

=head2 event_category

As this is a system-level event, always returns "system".

=cut

sub event_category { 'system'; }

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
