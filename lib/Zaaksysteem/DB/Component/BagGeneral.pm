package Zaaksysteem::DB::Component::BagGeneral;

use strict;
use warnings;

use base qw/DBIx::Class/;

sub TO_JSON {
    my $self                = shift;

    my $json                = ($self->next::can ? $self->next::method(@_) : { $self->get_columns });

    # For lack of a better place to 'join' this information: look up the Object Subscription
    # here.
    $json->{object_subscription}    = undef;

    if ($self->can('subscription_id') && self->subscription_id) {
        $json->{object_subscription} = {
            'id' => $self->subscription_id->id,
            'external_id' => $self->subscription_id->external_id,
        };
    }

    return $json;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 TO_JSON

TODO: Fix the POD

=cut

