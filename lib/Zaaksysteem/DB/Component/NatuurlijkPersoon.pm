package Zaaksysteem::DB::Component::NatuurlijkPersoon;
use Moose;
use utf8;

use base qw/Zaaksysteem::DB::Component::GenericNatuurlijkPersoon/;

use Zaaksysteem::Backend::Subject::Naamgebruik qw/naamgebruik voorletters/;
use Zaaksysteem::Zaken;

extends 'Zaaksysteem::Backend::Component';
with qw/
    MooseX::Log::Log4perl
    Zaaksysteem::BR::Subject::Component
    Zaaksysteem::Backend::Roles::NatuurlijkPersoon::ObjectSubscription
    Zaaksysteem::Roles::ContactDetails
/;

use BTTW::Tools;
use Zaaksysteem::Types qw(BSN);

=head2 insert

TODO: Fix the POD

=cut

sub insert {
    my ($self) = @_;

    # we need the database to generate the new searchable_id param.
    # if we don't supply it, it will take its chance. so get rid
    # of whatever the app thinks it should pass.
    # is there a way to put this in the Schema? that's be less hacky.
    delete $self->{_column_data}->{searchable_id};

    $self->_database_triggers();

    return $self->next::method(@_);
}

=head2 update

TODO: Fix the POD

=cut

has _dirty => (
    is => 'rw',
    isa => 'Bool',
    default => 0,
);

sub update {
    my $self    = shift;
    my $params  = shift;

    if ($params && UNIVERSAL::isa($params, 'HASH')) {
        for my $key (keys %{ $params }) {
            $self->$key($params->{ $key });
        }
    }

    if ($self->get_dirty_columns) {
        $self->_database_triggers();
        $self->_dirty(1);
    }

    return $self->next::method(@_);
}

after update => sub {
    my $self = shift;
    return unless $self->_dirty;

    $self->queue_contact_details_for_cases;
    return;
};

sub _database_triggers {
    my $self = shift;

    $self->_format_bsn;

    $self->naamgebruik($self->achternaam) if !$self->deleted_on;
    $self->voorletters(voorletters($self->voornamen)) if !$self->authenticated;

    $self->_set_search_term();
    $self->_update_object_subscription;
}

sub get_cases {
    my ($self, %opts) = @_;
    my $z = Zaaksysteem::Zaken->new(dbic => $self->result_source->schema);

    my %args = (
        show_all_betrokkene_cases => 1,
        betrokkene_type           => 'natuurlijk_persoon',
        gegevens_magazijn_id      => $self->id,
        type_zaken => [qw(new open resolved closed stalled)],
        %opts,
    );

    # Force search_rs behaviour
    my $cases = $z->zaken_pip(\%args);
    return $cases;
}

=head2 involved_with_cases

Boolean value if the natuurlijk persoon is involved in cases

=cut

sub involved_with_cases {
    my $self = shift;

    return 1 if $self->has_cases;
    return 1 if $self->has_concept_cases;
    return 0;
}

=head2 has_cases

Boolean value if the user has a case

=cut

sub has_cases {
    my $self = shift;

    my $cases = $self->get_cases(all_cases => 1, rows => 1, page => 1);
    return 1 if $cases->first;
    return 0;
}

=head2 has_concept_cases

Boolean value if the user has a concept case

=cut

sub has_concept_cases {
    my $self = shift;
    my $cases = $self->result_source->schema->resultset('ZaakOnafgerond')
        ->search_rs(
        { betrokkene => 'betrokkene-natuurlijk_persoon-' . $self->id });
    return 1 if $cases->first;
    return 0;
}

=head2 disable_natuurlijk_persoon

Apply logic for deleting a natural person

=cut

sub disable_natuurlijk_persoon {
    my $self = shift;
    my $date = shift // DateTime->now;

    my $inactive = $self->involved_with_cases;

    if ($inactive) {
        $self->deleted_on(undef);
    }
    else {
        $self->deleted_on($date);
    }
    $self->active(0);

    $self->update;

    my $schema = $self->result_source->schema;
    $schema->resultset('Logging')->trigger("natuurlijk_persoon/disable",
        {
            component => 'betrokkene',
            data => {
                id  => $self->id,
                bsn => $self->bsn,
                $inactive ? (inactive => 1) : (deleted => 1),
                date => $date->iso8601,
            },
        }
    );

    return 1;
}

sub enable_natuurlijk_persoon {
    my $self = shift;

    my %args = (
        deleted_on => undef,
        active     => 1,
    );

    if (my $os = $self->subscription_id) {
        $os->update({ date_deleted => undef });
        $args{authenticated}   = 1;
        $args{authenticatedby} = 'gba';
    }

    $self->update(\%args);


    my $schema = $self->result_source->schema;
    $schema->resultset('Logging')->trigger("natuurlijk_persoon/enable",
        {
            component => 'betrokkene',
            data => {
                id     => $self->id,
                bsn    => $self->bsn,
                enable => 1,
                date   => DateTime->now->iso8601,
            },
        }
    );

    return 1;
}

sub _update_object_subscription {
    my $self = shift;

    return 0 if $self->deleted_on;

    my $os = $self->subscription_id;
    return 0 unless $os;

    my @t;
    if ($self->datum_overlijden) {
        push(@t, "†")
    }
    foreach (qw(bsn voornamen voorvoegsel achternaam)) {
        if ($self->$_) {
            push(@t, $self->$_) if length($self->$_ // '');
        }
    }
    my $str = join(" ", @t);
    $os->update({object_preview => $str});
    return 1;
}

=head2 bid

Convenience method to get the betrokkene ID of a natural person.

=cut

sub bid {
    my $self = shift;
    return 'betrokkene-natuurlijk_persoon-' . $self->id;
}

=head2 bsn

Convenience method to get the 9 digit BSN from a burgerservice number.
Returns 0 if there is no burgerservicenummer found.

=cut

sub bsn {
    my $self = shift;
    if ($self->burgerservicenummer) {
        return sprintf('%09d', int($self->burgerservicenummer));
    }
    return 0;
}

sub _format_bsn {
    my $self = shift;
    if (!BSN->check($self->burgerservicenummer)) {
        $self->burgerservicenummer(undef);
    }
    else {
        $self->burgerservicenummer(int($self->burgerservicenummer));
    }
}

sub _set_search_term {
    my ($self) = @_;

    my @search_terms;

    if ($self->deleted_on) {
        $self->search_term('');
        $self->search_order('');
        return;
    }

    if ($self->datum_overlijden) {
        push(@search_terms, "†");
    }

    foreach (qw(voornamen voorvoegsel achternaam)) {
        push(@search_terms, $self->$_) if $self->$_;
    }

    if($self->burgerservicenummer) {
        push(@search_terms, $self->bsn);
    }

    if($self->geboortedatum) {
        my $geboortedatum = $self->geboortedatum->clone->set_time_zone('Europe/Amsterdam');
        push(@search_terms, $geboortedatum, $geboortedatum->dmy);
    }

    if(my $address = $self->adres_id) {

        if ($address->huisnummer && $address->huisnummertoevoeging) {
            push(@search_terms, $address->straatnaam) if $address->straatnaam;
            push(@search_terms,
                join('', $address->huisnummer, $address->huisnummertoevoeging)
            );
            foreach (qw(huisletter woonplaats postcode)) {
                push(@search_terms, $address->$_) if $address->$_;
            }
        }
        else {
            foreach (qw(straatnaam huisnummer huisnummertoevoeging huisletter woonplaats postcode)) {
                push(@search_terms, $address->$_) if $address->$_;
            }
        }

    }

    push(@search_terms, $self->get_contact_data_search_term);

    $self->search_term(join(' ', @search_terms));
    $self->search_order(join(' ', $self->naamgebruik, @search_terms));
    return;
}

=head2 get_contact_data_search_term

TODO: Fix the POD

=cut

sub get_contact_data_search_term {
    my $self = shift;

    my $contact_data = $self->result_source->schema->resultset('ContactData')->search({
        gegevens_magazijn_id => $self->id,
        betrokkene_type => 1 # 1 for natuurlijk_persoon, 2 for bedrijf
    })->first;

    return $contact_data && $contact_data->email || '';
}

=head2 achternaam

Returns the achternaam based on naamsgebruik.

=cut


sub achternaam {
    my ($self) = @_;

    return naamgebruik({
        aanduiding => $self->aanduiding_naamgebruik || '',
        partner_voorvoegsel   => $self->partner_voorvoegsel,
        partner_geslachtsnaam => $self->partner_geslachtsnaam,
        voorvoegsel           => $self->voorvoegsel,
        geslachtsnaam         => $self->geslachtsnaam,
    });
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
