package Zaaksysteem::Event::Model;
use Moose;

with 'MooseX::Log::Log4perl';

use UUID4::Tiny qw(create_uuid_string);

has amqp => (
    isa => 'Net::AMQP::RabbitMQ',
    is => 'ro',
    required => 1,
);

has channel => (
    isa => 'Int',
    is => 'ro',
    required => 1,
);

has exchange => (
    isa => 'Str',
    is => 'ro',
    required => 1,
);

has context => (
    isa => 'Str',
    is => 'ro',
    required => 1,
);

has json => (
    is => 'rw',
    isa => 'JSON::XS',
    default => sub { JSON::XS->new->canonical(1) }
);

sub get_base_event_parameters {
    my $domain = shift;

    my $parameters = {
        id => create_uuid_string(),
        created_date => DateTime->now->iso8601 . 'Z',
        correlation_id => create_uuid_string(),
        domain => $domain,
        entity_data => {},
    };

    return $parameters;
}

sub _publish {
    my $self = shift;
    my ($routing_key, $parameters) = @_;

    $self->amqp->publish(
        $self->channel,
        $routing_key,
        $self->json->encode($parameters),
        { exchange => $self->exchange },
        { content_type => "application/json" },
    );

    return;
}

sub publish_queued_events {
    my $self = shift;
    my $user_uuid = shift;
    my $events = shift;

    while(@$events) {
        my $event = shift @$events;

        $event->{parameters}{context} = $self->context;
        $event->{parameters}{user_uuid} = $user_uuid;

        $self->_publish($event->{routing_key}, $event->{parameters});
    }

    return;
}

sub publish_incoming_email_event {
    my $self = shift;
    my $filestore_uuid = shift;

    $self->log->info(
        sprintf(
            "Broadcasting event for '%s' in context '%s'",
            $filestore_uuid,
            $self->context,
        )
    );

    my $routing_key = 'zsnl.v2.communication.IncomingEmail.EmailReceived';

    my $parameters = get_base_event_parameters('communication');

    $parameters->{context} = $self->context;
    $parameters->{user_uuid} = undef; # System event: not executed by a user
    $parameters->{entity_type} = 'IncomingEmail';
    $parameters->{entity_id} = create_uuid_string();
    $parameters->{event_name} = 'EmailReceived';
    $parameters->{changes} = [
        {"key" => "file", "old_value" => undef, "new_value" => "$filestore_uuid"}
    ];

    $self->_publish($routing_key, $parameters);
}

sub create_case_custom_fields_updated_event {
    my $case_uuid = shift;
    my $field_values = shift;

    my $routing_key = 'zsnl.v2.legacy.Case.CaseCustomFieldsUpdated';

    my $parameters = get_base_event_parameters('legacy');

    $parameters->{entity_type} = 'Case';
    $parameters->{entity_id} = $case_uuid;
    $parameters->{event_name} = 'CaseCustomFieldsUpdated';
    $parameters->{changes} = [
        {
            "key" => "custom_fields",
            "old_value" => undef,
            "new_value" => $field_values
        }
    ];

    return { "routing_key" => $routing_key, "parameters" => $parameters };
}

__PACKAGE__->meta->make_immutable();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019-2020, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
