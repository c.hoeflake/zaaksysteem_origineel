package Zaaksysteem::Event::Context;

use Data::Dumper;

use Exporter 'import';

@EXPORT_OK = qw[get_context_constraints get_type_constraints];

use constant LOGLINE_MATCHERS => {
    'case/create'                   => 'Zaak (%) aangemaakt',
    'case/close'                    => 'Zaak gesloten op (%)',
    'case/attribute/update'         => 'Kenmerk "%" gewijzigd naar: "%"',
    'case/document/create'          => 'Document "%" [%] succesvol aangemaakt',
    'case/update/purge_date'        => 'Vernietigingsdatum gewijzigd: %',
    'case/update/target_date'       => 'Streefafhandeldatum voor zaak: "%" gewijzigd naar: %',
    'case/update/registration_date' => 'registratiedatum voor zaak: "%" gewijzigd naar: %',
    'case/update/status'            => 'Status gewijzigd naar: %',
    'case/update/subject'           => 'Betrokkene "aanvrager" gewijzigd naar: "%"',
    'case/checklist/item/update'    => 'Antwoord voor vraag "%" gewijzigd naar "%"',
    'case/note/create'              => 'Notitie toegevoegd'
};

use constant CONTEXTS => {
    none => { type_matchers => [ '^.+$' ] },

    case => {
        type_matchers => [
            '^case\/', # case/*
            '^subject\/contactmoment\/',
            '^subject\/message\/',
            '^file\/component',
            '^document\/(create|accept|remove)',
            '^email\/',
            '^scheduler\/',
            '^object\/'
        ]
    },

    subject => {
        type_matchers => [
            '^case\/(create|close|note|checklist|document)\/',
            '^case\/update\/(purge|target|registration|status|subject)',
            '^subject/(inspect|update|update_contact_data)$',
            '^object\/view',
            '^(case|subject)\/(note|contactmoment)\/create$',
            '^document\/',
            '^email\/',
            '^auth\/alternative',
        ],
        other_matchers => [
            { 'me.restricted' => 0 },
        ],
    }
};

sub get_type_constraints {
    my @constraints;

    for my $type (@_) {
        push(@constraints, {
            'me.event_type' => { '~' => quotemeta $type }
        });

        for my $full_type (keys %{ LOGLINE_MATCHERS() }) {
            next unless $full_type =~ m[\Q$type];

            push(@constraints, {
                'me.onderwerp' => { like => LOGLINE_MATCHERS->{ $full_type } }
            });
        }
    }

    return scalar(@constraints) ? { -or => \@constraints } : { };
}

sub get_context_constraints {
    my $context_name = shift || 'all';

    my $context = CONTEXTS->{ $context_name };

    return unless $context;

    my @constraints;

    for my $rx (@{ $context->{ type_matchers } }) {
        push(@constraints, { 'me.event_type' => { '~' => $rx }});

        for my $type (keys %{ LOGLINE_MATCHERS() }) {
            next unless($type =~ /$rx/);

            push(@constraints, {
                'me.onderwerp' => { like => LOGLINE_MATCHERS->{ $type } }
            });
        }
    }


    return {
        -and => [
            @{ $context->{other_matchers} // [] },
            { -or => \@constraints },
        ]
    };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, 2018 Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 get_context_constraints

TODO: Fix the POD

=cut

=head2 get_type_constraints

TODO: Fix the POD

=cut

