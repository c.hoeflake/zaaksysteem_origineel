package Zaaksysteem::Event::DocumentAddedToCase;
use Moose;

with 'Zaaksysteem::Event::EventInterface';

=head1 NAME

Zaaksysteem::Event::DocumentAddedToCase - Event handler for DocumentAddedToCase

=head1 DESCRIPTION

=head1 SYNOPSIS

=cut

sub event_type { 'DocumentAddedToCase' }

sub emit_event {
    my $self = shift;

    my $ok = $self->queue->emit_case_event(
        {
            case       => $self->case,
            event_name => 'DocumentAddedToCase',
            description => 'Document toegevoegd aan zaak',
            changes    => $self->event->{ changes }
        }
    );
    return $self->new_ack_message('Event processed') if $ok;
    return $self->new_ack_message('Event processed: nothing sent');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

