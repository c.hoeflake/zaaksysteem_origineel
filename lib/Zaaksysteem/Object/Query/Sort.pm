package Zaaksysteem::Object::Query::Sort;

use Moose;

use Moose::Util::TypeConstraints qw[role_type];
use BTTW::Tools;

=head1 NAME

Zaaksysteem::Object::Query::Sort - Sort declarations for object queries

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 expression

A L<Zaaksysteem::Object::Query::Expression> that should be used for sorting
the results of the query, after the condition has been applied to the
resultset.

=cut

has expression => (
    is => 'rw',
    isa => role_type('Zaaksysteem::Object::Query::Expression'),
    required => 1
);

=head2 reverse

Boolean attribute that indicates if the resultset should be sorted in reverse
order.

=cut

has reverse => (
    is => 'rw',
    isa => 'Bool',
    traits => [qw[Bool]],
    default => 0,
);

=head1 METHODS

=head2 stringify

Implements logic required by
L<Zaaksysteem::Object::Query::Expression/stringify>.

=cut

sub stringify {
    my $self = shift;

    my $ret = $self->expression->stringify;

    if ($self->reverse) {
        $ret = sprintf('%s, reverse order', $ret);
    }

    return $ret;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
