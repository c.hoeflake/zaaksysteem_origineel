package Zaaksysteem::Object::Queue::Model::Sysin;

use Moose::Role;

use BTTW::Tools;


=head1 NAME

Zaaksysteem::Object::Queue::Model::Sysin - Handlers for deferred sysin actions

=head1 METHODS

=head2 trigger_sysin_interface

Deferred event for triggering the instance's event sink interfaces

=cut

sig trigger_sysin_interface => 'Zaaksysteem::Backend::Object::Queue::Component';

sub trigger_sysin_interface {
    my $self = shift;
    my $item = shift;

    my $case = $self->_rs_zaak->find({ uuid => $item->get_column('object_id') });

    if ($self->has_event_sinks) {
        for my $sink (@{ $self->event_sinks }) {
            $sink->process_trigger('post_event', { %{ $item->data }, case => $case });
        }
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
