package Zaaksysteem::Object::Queue::Model::Object;

use Moose::Role;

use BTTW::Tools;

=head1 NAME

Zaaksysteem::Object::Queue::Model::Object - L<Zaaksysteem::Object>
synchronisation handlers

=head1 DESCRIPTION

This queue model role implements handlers for object synchronisation.

=head1 METHODS

=head2 index_object

Upserts the object's full text declaration in an Elasticsearch index.

=cut

define_profile index_object => (
    required => {
        object_reference => 'HashRef',
        content => 'Str',
    },
    optional => {
        internal_reference => 'HashRef'
    }
);

sub index_object {
    my $self = shift;
    my $item = shift;

    my $data = assert_profile($item->data)->valid;

    unless ($self->has_attribute_index_model) {
        throw('queue/index_object/attribute_index_model_missing', sprintf(
            'This action requires a mapping model instance.'
        ));
    }

    my $ref = $data->{ object_reference };
    my $intref = $data->{ internal_reference };

    my $body = {
        'core.ref.id' => $ref->{ id },
        'core.ref.type' => $ref->{ type },
        'core.keywords' => $data->{ content },
    };

    if (defined $intref) {
        $body->{ 'core.intref.id' } = $intref->{ id };
        $body->{ 'core.intref.table' } = $intref->{ table };
    }

    my $preview;
    my $object_data_row = $item->object_id;

    if (defined $object_data_row) {
        my $object = $self->object_model->inflate_from_row($object_data_row);

        $body->{ 'core.preview' } = "$object";
    } elsif (exists $ref->{ preview }) {
        $body->{ 'core.preview' } = $ref->{ preview };
    } else {
        # Anything is better than nothing
        $body->{ 'core.preview' } = sprintf(
            '%s(...%s)',
            $ref->{ type },
            substr($ref->{ id }, -6)
        );
    }

    my $model = $self->attribute_index_model;

    # Ensure our index exists, create in place if not.
    $model->initialize;

    $model->elasticsearch->index(
        index => sprintf('%s_object', $model->index_name),
        type => 'object',
        id => $ref->{ id },
        body => $body,
    );

    return;
}

=head2 delete_object

Deletes an object from the search index.

=cut

define_profile delete_object => (
    required => {
        object_reference => 'HashRef'
    }
);

sub delete_object {
    my $self = shift;
    my $item = shift;

    my $data = assert_profile($item->data)->valid;

    my $model = $self->attribute_index_model;

    $model->elasticsearch->delete(
        index => sprintf('%s_object', $model->index_name),
        type => 'object',
        id => $data->{ object_reference }{ id }
    );

    return;
}

=head2 sync_object

Synchronizes the row as configured by the provided data.

Requires the C<table>, and C<primary_key> fields.

=cut

define_profile sync_object => (
    required => {
        table => 'Str',
        primary_key => 'Defined'
    }
);

sub sync_object {
    my $self = shift;
    my $item = shift;

    my $data = assert_profile($item->data)->valid;

    my $rs = $self->object_model->schema->resultset($data->{ table });
    my $row = $rs->find($data->{ primary_key });

    unless (defined $row) {
        throw('queue/sync_object/object_source_row_not_found', sprintf(
            'Could not find "%s" by id "%s"',
            $data->{ table },
            $data->{ primary_key }
        ));
    }

    unless ($row->can('as_object')) {
        throw('queue/sync_object/object_source_not_supported', sprintf(
            'The component class for row %d in table "%s" does not implement the "as_object" interface',
            $data->{ primary_key },
            $data->{ table }
        ));
    }

    $self->object_model->save(object => $row->as_object);

    return;
}

=head2 store_object_label

In the past, object labels were not stored in their properties, making it had to
work with them outside the Perl system.

This queue method adds the new field to existing objects.

=cut

define_profile store_object_label => (
    required => {
        object_uuid => 'Str',
    }
);

sub store_object_label {
    my $self = shift;
    my $item = shift;

    my $data = assert_profile($item->data)->valid;

    my $object_row = $self->schema->resultset("ObjectData")->find($data->{object_uuid});
    my $object = $self->object_model->inflate_from_row($object_row);

    my $properties = $object_row->properties;

    $properties->{label} = $object->TO_STRING;

    $object_row->update(
        { properties => $properties }
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
