package Zaaksysteem::Object::Attribute::Deadline;

use Moose::Role;

use Scalar::Util qw(blessed);

=head2 _build_human_value

Return the human-readable format for this field.

=cut

sub _build_human_value {
    my $self = shift;

    my $ref = ref $self->value;
    if ($ref eq 'ARRAY') {
        # Timeline is array
        my @val = map { _build_single_value($_) } @{$self->value};
        return join("; ", @val);
    }
    elsif($ref eq 'HASH') {
        return _build_single_value($self->value);
    }

    return $self->value;
}

sub _build_single_value {
    my $value = shift;

    return sprintf("Fase %s: %d van de %d",
        $value->{phase_no}, $value->{current}, $value->{days});
}

sub _build_index_value {
    my $self = shift;
    my ($value) = @_;

    return $value;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

