package Zaaksysteem::Object::Attribute::Filestore;

use Moose::Role;

=head2 _build_human_value

On our Filestore role, the human_value is the value of the filenames, comma seperated

=cut

sub _build_human_value {
    my $self  = shift;
    my $value = $self->value;

    return '' unless $value;

    return join ',', map {
        $_->{ filename } || $_->{ original_name } || $_->{ md5 }
    } @{ $value };
}

=head2 index_value

Returns the indexable value.

=cut

sub _build_index_value {
    my $self = shift;
    my ($value) = @_;

    return '' unless $value;

    return $value->{ filename };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

