package Zaaksysteem::Object::Roles::Interfaceable;

use Moose::Role;

use BTTW::Tools;

=head1 NAME

Zaaksysteem::Object::Roles::Interfaceable - Add implied relationship between
object and 0+ interfaces

=head1 DESCRIPTION

This role is used on objects that have an associated sysin interface.

=head1 ATTRIBUTES

=head2 interfaces

This attribute contains an array of interface relationship definitions. The
data is not strictly validated aside from asserting the individual definitions
are C<HashRef> values.

Exposes C<add_interface>, C<all_interfaces>, and C<has_interfaces>.

=cut

has interfaces => (
    is => 'rw',
    isa => 'ArrayRef[HashRef]',
    label => 'interfaces',
    required => 1,
    default => sub { [] },
    traits => [qw[OA Array]],
    handles => {
        add_interface => 'push',
        all_interfaces => 'elements',
        has_interfaces => 'count'
    }
);

=head1 METHODS

=head2 remove_interface

Removes a provided interface_id from the L</interfaces> registry.

=cut

sub remove_interface {
    my $self = shift;
    my $id = shift;

    my @interfaces = grep { $_->{ id } ne $id } $self->all_interfaces;

    $self->interfaces(\@interfaces);

    return;
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
