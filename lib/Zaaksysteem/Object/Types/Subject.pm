package Zaaksysteem::Object::Types::Subject;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';
with qw(Zaaksysteem::Object::Roles::Relation Zaaksysteem::BR::Subject::Types::Subject);

use BTTW::Tools;
use Zaaksysteem::Types qw(ObjectSubjectType Betrokkene);

use Zaaksysteem::Object::Types::Person;
use Zaaksysteem::Object::Types::Company;
use Zaaksysteem::Object::Types::Employee;
use Zaaksysteem::Object::Types::Authentication;
use Zaaksysteem::Object::Types::ExternalSubscription;

=head1 NAME

Zaaksysteem::Object::Types::Subject - Built-in object type implementing a class for a generic Subject

=head1 DESCRIPTION

Subject object, where bedrijven, medewerkers and natuurlijk personen are stored.

=head1 PERSON ATTRIBUTES

=head2 subject_type

The type of this subject

=cut

has subject_type => (
    is       => 'rw',
    isa      => ObjectSubjectType,
    traits   => [qw[OA]],
    label    => 'The type of this subject',
    required => 1,
    unique   => 1,
);

=head2 subject

The actual subject object, .e.g L<Zaaksysteem::Object::Types::Person>, L<Zaaksysteem::Object::Types::Company>, L<Zaaksysteem::Object::Types::Employee>

=cut

has 'subject'   => (
    is      => 'rw',
    traits   => [qw(OR)],
    label    => 'Related subject',
    embed    => 1,
    required => 1,
);

=head2 authentication

The related authentication object, containing username etc: L<Zaaksysteem::Object::Types::Authentication>

=cut

has 'authentication'   => (
    is      => 'rw',
    isa     => 'Zaaksysteem::Object::Types::Authentication',
    traits   => [qw(OR)],
    embed    => 1,
    label    => 'Related authentication object',
);

=head2 external_subscription

A L<Zaaksysteem::Object::Types::ExternalSubscription> object.

=cut

has external_subscription => (
    is     => 'rw',
    isa    => 'Zaaksysteem::Object::Types::ExternalSubscription',
    embed    => 1,
    traits => [qw(OR)],
    label  => 'Related subscription object (subscription on other systems)',
    predicate => 'has_external_subscription',
);

=head1 CONVENIENCE ATRRIBUTES

=cut

has 'display_name'  => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Name',
    lazy     => 1,
    builder  => '_build_display_name',
    required => 0,
);

=head2 old_subject_identifier

    print $subject->old_subject_identifier

    # Prints e.g.:
    # betrokkene-mederwerker-44
    # betrokkene-natuurlijk_persoon 66
    # betrokkene-bedrijf-55

B<IMPORTANT NOTE>:
Old style identifier for a subject. We really do not want to use this identifier anymore. When you need this
identifier, please look at a way to rewrite the API call to use the UUID instead.

=cut

has 'old_subject_identifier'   => (
    is      => 'ro',
    traits  => [qw(OA)],
    isa     => 'Maybe[Str]',
    label   => 'Old style subject identifier.',
    lazy    => 1,
    default => sub {
        my $self = shift;

        my %map = ('person' => 'natuurlijk_persoon', 'company' => 'bedrijf', 'employee' => 'medewerker');

        return unless $self->subject->_table_id;

        return 'betrokkene-' . $map{$self->subject_type} . '-' . $self->subject->_table_id;
    }
);

=head1 METHODS

=head2 TO_STRING

Overrides L<Zaaksysteem::Object/TO_STRING> and returns L</display_name>.

=cut

override TO_STRING => sub { shift->display_name };

=head1 PRIVATE METHODS

=head2 _build_display_name

=cut

sub _build_display_name {
    my $self            = shift;

    $self->subject->_build_display_name;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
