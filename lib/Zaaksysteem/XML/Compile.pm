package Zaaksysteem::XML::Compile;

use Zaaksysteem::XML::Compile::Backend;

=head1 NAME

Zaaksysteem::XML::Compile - XML::Compile plugin for Catalyst, allowing preloading of XSDs

=head1 SYNOPSIS

    ### Get Z::XML::Compile::Backend object
    ### Somewhere high in Zaaksysteem.pm, but below __PACKAGE__->setup();

    __PACKAGE__->xml_compile->add_class([qw/
        Zaaksysteem::StUF::0204::Instance
        Zaaksysteem::StUF::0301::Instance
    /
    ]);

    ### Within a controller, get Z::XML::Compile::Backend object
    my $backend     = $c->xml_compile

    ### Within a different module
    my $backend     = Zaaksysteem::XML::Compile->xml_compile;

    ### Load StUF 0204 instance
    my $stuf0204    = $c->xml_compile->stuf0204

    ### Get schema from it
    my $schema      = $stuf0204->schema;

    ### Short, e.g. from modules
    Zaaksysteem::XML::Compile->xml_compile->stuf0204->schema;

=head1 DESCRIPTION

This object makes sure all XSD's are compiled before catalyst preforks. This way all XSD's
will be compiled on Zaaksysteem startup. Any memory leaks only happen once, and memory gets
better spread.

=head1 METHODS

=head2 $c->xml_compile

Arguments: none

Return value: L<Zaaksysteem::XML::Compile::Backend>

    my $backend = $c->xml_compile

Returns a L<Zaaksysteem::XML::Compile::Backend> object. Different XSD schema's can
be loaded below this object as instances L<Zaaksysteem::XML::Compile::Instance>

=cut

my $xml_compile_abstraction;

sub xml_compile {
    shift->_load_xml_compile;

    return $xml_compile_abstraction;
}

=head1 CATALYST METHODS

=head2 setup_actions

Overrides C<setup_actions> within catalyst, to load the xml_compile instances.

=cut

sub setup_actions {
    my $class   = shift;

    $class->_load_xml_compile(
        {
            home => $class->config->{home}
        }
    );

    return $class->next::method(@_);
}

=head1 INTERNAL METHODS

=head2 _load_xml_compile

Arguments: \%options

Return value: L<Zaaksysteem::XML::Compile::Backend>

    my $backend = Zaaksysteem::XML::Compile->_load_xml_compile({ home => './' });

Called from C<setup_actions>, loads the actual backend object into C<< $c->xml_compile >>

B<options>

=over 4

=item home

Location of home directory, normally the root of catalyst.

C<setup_actions> will pass C<< $c->config->{home} >> to this object

=back

=cut

sub _load_xml_compile {
    my $class   = shift;
    my $opts    = shift || {};

    return if $xml_compile_abstraction;

    $xml_compile_abstraction = Zaaksysteem::XML::Compile::Backend->new(
        home => ($opts->{home} || './'),
    );
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
