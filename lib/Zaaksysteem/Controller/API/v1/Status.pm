package Zaaksysteem::Controller::API::v1::Status;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::Status - API v1 Status controller

=head1 DESCRIPTION

This minimalist controller serves as both a "by example" API v1 controller
implementation, as well as hosting a stable endpoint which can be used in
documentation and other examples for API interactions.

=cut

use Zaaksysteem::API::v1::Status;

sub BUILD {
    my $self = shift;

    $self->add_api_context_permission('extern');
}

=head1 ACTIONS

=head2 status

Returns the status of the Zaaksysteem instance.

=head3 URL Path

C</api/v1/status>

=cut

sub status : Chained('/api/v1/base') : PathPart('status') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ result } = Zaaksysteem::API::v1::Status->new(
        version => $c->config->{ ZS_VERSION }
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
