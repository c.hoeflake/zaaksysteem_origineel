package Zaaksysteem::Controller::API::v1::Role;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }
with "MooseX::Log::Log4perl";

sub BUILD {
    my $self = shift;
    $self->add_api_context_permission('intern');
}

=head1 NAME

Zaaksysteem::Controller::API::v1::Role - API controler for employee roles

=head1 DESCRIPTION

API v1 controller for employee roles

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/role> routing namespace

=cut

sub base : Chained('/api/v1/base') : PathPart('role') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->stash->{base_rs} = $c->model('DB::Roles')->search_rs();
}

=head2 search_base

Search base so ES search syntax can be grabbed in the request

=cut

sub search_base : Chained('base') : PathPart('') : CaptureArgs(0) {
    my ($self, $c) = @_;
    $c->stash->{base_rs} = $self->_get_objects($c);
}

=head2 instance_base

Reserves the C</api/v1/role/[ROLE_UUID]> routing namespace

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $uuid) = @_;

    $c->assert_uuid($uuid);

    my $found = $c->stash->{base_rs}->search_rs({ uuid => $uuid })->first;

    if ($found) {
        $c->stash->{result} = $found;
    }
    else {
        throw(
            'api/v1/role/not_found',
            "The role object with UUID '$uuid' could not be found",
            { http_code => 404 }
        );
    }
}

=head2 list

List all the roles found in the database. You can search against this
controller via ES search syntax.

=head3 URL path

C</api/v1/role>

=cut

sub list : Chained('search_base') : PathPart('') : Args(0) : DB('RO') {
    my ($self, $c) = @_;

    $c->stash->{set} = Zaaksysteem::API::v1::ResultSet->new(
        iterator => $c->stash->{base_rs}
    );
    $self->list_set($c);
}

=head2 get

Get a single role from Zaaksysteem.

=head3 URL path

C</api/v1/role/[UUID]>

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : DB('RO') {
    my ($self, $c) = @_;

    if ($c->stash->{result}) {
        $c->stash->{is_cacheable} = 1;
        $c->res->headers->last_modified(
            $c->stash->{result}->date_modified->epoch
        );
        $c->res->header('Cache-Control' => 'private');
    }
}

sub _get_objects {
    my ($self, $c) = @_;

    my %query;
    my %params = %{$c->req->params};

    delete $params{$_} for qw(page rows_per_page);

    if (keys %params) {
        my $es = $c->parse_es_query_params->{query};

        if (defined $es->{match}{name}) {
            my $like = sprintf("%%%s%%", $es->{match}{name});
            $query{name} = { ilike => $like };
        }

        if (defined $es->{match}{description}) {
            my $like = sprintf("%%%s%%", $es->{match}{description});
            $query{description} = { ilike => $like };
        }
    }
    return $c->stash->{base_rs}->search_rs(%query ? \%query : undef);
}

__PACKAGE__->meta->make_immutable;

__END__


=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
