package Zaaksysteem::Controller::API::Queue;

use Moose;
use namespace::autoclean;

use BTTW::Tools;
use DateTime;
use Zaaksysteem::Types qw[UUID];

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::API::Queue - Action queue controller

=head1 DESCRIPTION

=head1 ACTIONS

=head2 base

Reserves the C</api/queue> URI path namespace.

=head3 Parameters

=over 4

=item context_object_id

C<UUID> reference of the context for which the queue is accessed.

=back

=cut

sub base : Chained('/api/base') : PathPart('queue') : CaptureArgs(0) {
    my ($self, $c) = @_;

    my $rs = $c->model('DB::Queue');

    $c->stash->{ queue } = $rs;
}

=head2 item_base

Reserves the C</api/queue/[UUID]> path namespace.

=cut

define_profile item_base => (
    optional => { context_object_id => UUID }
);

sub item_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $uuid) = @_;

    my $params = assert_profile($c->req->params)->valid;

    if (exists $params->{ context_object_id }) {
        $c->stash->{ queue } = $c->stash->{ queue}->search(
            { object_id => $params->{ context_object_id } }
        );
    }

    my $item = $c->stash->{ queue }->find($uuid);

    unless ($item) {
        throw('api/queue/item_not_found', sprintf(
            'Could not find queue item by UUID "%s"',
            $uuid
        ));
    }

    $c->stash->{ item } = $item;
}

=head2 pending_base

Reserves the C</api/queue/pending> routing namespace.

=cut

define_profile pending_base => (
    optional => {
        minimum_age_seconds => 'Int'
    }
);

sub pending_base : Chained('base') : PathPart('pending') : CaptureArgs(0) : DB('RO') {
    my ($self, $c) = @_;

    my $opts = assert_profile($c->req->params)->valid;

    my $since = DateTime->now();

    if (exists $opts->{ minimum_age_seconds }) {
        $since->subtract(seconds => $opts->{ minimum_age_seconds });
    }

    $c->stash->{ queue_pending } = $c->stash->{ queue }->search_pending($since);
}

=head2 pending

View a list of pending queue items.

=head3 URL

C</api/queue/pending>

=head3 Response

Responds to a request with a list of L<Zaaksysteem::Schema::Queue> rows.

=cut

sub pending : Chained('pending_base') : PathPart('') : Args(0) : ZAPI {
    my ($self, $c) = @_;

    # Only admins can view *all* queue-items
    $c->assert_any_user_permission('admin');

    # Set max items to 200 for some sane limits on the scheduler
    my @items = $c->stash->{ queue_pending }->search(undef, { rows => 200 })->all;

    $c->stash->{ zapi_no_pager } = 1;
    $c->stash->{ zapi } = \@items;
}

=head2 run_next

Runs the next pending queue item.

=head3 URL

C</api/queue/pending/run_next>

=head3 Response

See L</run_item>.

=cut

sub run_next : Chained('pending_base') : PathPart('run_next') : Args(0) : ZAPI {
    my ($self, $c) = @_;

    # Normal users should never run this routine, sanity guard against queue
    # run spam from arbitrary connected clients
    $c->assert_any_user_permission('admin');

    # Set rows to single result so DBD::Pg doesn't complain about unfinished
    # statements during storage->disconnect.
    my $item = $c->stash->{ queue_pending }->search(undef, { rows => 1 })->first;

    if ($item) {
        $c->stash->{ item } = $item;
        $c->detach('run_item');
    }

    $c->stash->{ zapi } = [ 'nothing to run' ];
}

=head2 item

View a specific queued item.

=head3 URL

C</api/queue/[UUID]>

=head3 Response

Response with a single L<Zaaksysteem::Schema::Queue> row instance, if one
could be found.

=cut

sub item : Chained('item_base') : PathPart('') : Args(0) : ZAPI {
    my ($self, $c, $item) = @_;

    $c->stash->{ zapi } = [ $c->stash->{ item } ];
}

=head2 run_item

Runs a specific queued item.

=head3 URL

C</api/queue/[UUID]/run>

=head3 Response

Response with a single L<Zaaksysteem::Schema::Queue> row instance.

=cut

sub run_item : Chained('item_base') : PathPart('run') : Args(0) : ZAPI : DB('ROW') : Access(*) {
    my ($self, $c, $uuid) = @_;

    my $item = $c->stash->{item};

    # emulated a logged in user here, so the underlying code for case item
    # handlers can use the should-be-replaced $zaak_rs->current_user stuff
    if (my $subject = $item->subject) {
        $c->model('DB')->schema->set_current_user($subject);
        $c->user($subject);
    }

    my $queue;
    if (!$item->requires_object_model) {
        $self->log->debug("Disable object model for Queue");
        $queue = $c->model('Queue', DisableObjectModel => 1);
    }
    elsif ($item->has_disable_acl) {
        $self->log->debug("ACL disabled object model for Queue");
        $queue = $c->model('Queue', DisableACL => 1);
    }
    else {
        $self->log->debug("Full fledged object model");
        $queue = $c->model('Queue');
    }

    $c->stash->{ zapi } = [ $queue->run_item($item->id) ];
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
