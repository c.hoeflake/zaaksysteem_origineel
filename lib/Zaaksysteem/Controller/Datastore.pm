package Zaaksysteem::Controller::Datastore;

use Moose;
use namespace::autoclean;

use Encode qw(encode);
use BTTW::Tools;
use Zaaksysteem::Constants qw/STUF_SUBSCRIPTION_VIEWS/;
use List::Util qw(pairmap);

use Zaaksysteem::Datastore::Model;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::Datastore - ZAPI Controller

=head1 SYNOPSIS

See L<Zaaksysteem::Controller::Sysin>

=head1 DESCRIPTION

Zaaksysteem API Controller for Datastore (Gegevensmagazijn).

=head1 INSTRUCTIONS

=head2 FORM LAYOUT

You can use the C<zapi_form=1> query parameter to retrieve more information
from a function regarding the needed form parameters.

=head1 METHODS

=cut

use constant DATASTORE_CLASSES => [
    qw/
        NatuurlijkPersoon
        Bedrijf
        BagLigplaats
        BagNummeraanduiding
        BagOpenbareruimte
        BagPand
        BagStandplaats
        BagVerblijfsobject
        BagWoonplaats
        /
];

=head2 index

=head3 URI: /datastore [GET READ]

Returns the index of /datastore

=cut

sub index : Chained('/') : PathPart('datastore') : Args(0) : ZAPI {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');

    $c->stash->{zapi} = [];
}

=head2 base

Base action of datastore/search

=cut

sub base : Chained('/') : PathPart('datastore/search') : Args(1) {
    my ($self, $c, $class) = @_;

    # Module/auth allowed check
    $c->assert_any_user_permission('admin');

    my $model;
    try {
        $model = Zaaksysteem::Datastore::Model->new(
            schema => $c->model('DB')->schema,
            type   => $class,
        );
    }
    catch {
        $self->log->info("$_");
        throw 'datastore/class_not_allowed',
            "Class $class is not allowed for datastore viewing/exporting";
    };

    if (exists $c->req->params->{zapi_crud}) {
        my @columns = $model->get_columns();

        my $actions = [];
        my $filters = [];
        if ($class eq 'NatuurlijkPersoon') {
            my $gm = $c->model("Gegevensmagazijn::NatuurlijkPersoon");
            $actions = [
                Zaaksysteem::ZAPI::CRUD::Interface::Action->new(
                    id      => "object-subscription-delete",
                    type    => "delete",
                    label   => "Verwijderen",
                    data    => {
                        url => '/gegevensmagazijn/np/delete'
                    }
                ),
                Zaaksysteem::ZAPI::CRUD::Interface::Action->new(
                    id      => "object-subscription-create",
                    type    => "update",
                    label   => "Aanmaken",
                    data    => {
                        url => '/gegevensmagazijn/np/create'
                    }
                ),
            ];

            $filters = [
                    Zaaksysteem::ZAPI::CRUD::Interface::Filter->new(
                        name    => 'object_subscription',
                        type    => 'select',
                        data    => {
                            options => [
                                pairmap {
                                    {
                                        value    => $a,
                                        label    => $b
                                    }
                                } @{$gm->filter_afnemerindicatie}
                            ],
                        },
                        value   => '',
                        label   => ''
                    ),
                    Zaaksysteem::ZAPI::CRUD::Interface::Filter->new(
                        name    => 'active',
                        type    => 'select',
                        data    => {
                            options => [
                                pairmap {
                                    {
                                        value    => $a,
                                        label    => $b
                                    }
                                } @{$gm->filter_active}
                            ],
                        },
                        value   => '',
                        label   => ''
                    ),
                    Zaaksysteem::ZAPI::CRUD::Interface::Filter->new(
                        name    => 'cases',
                        type    => 'select',
                        data    => {
                            options => [
                                pairmap {
                                    {
                                        value    => $a,
                                        label    => $b
                                    }
                                } @{$gm->filter_cases}
                            ],
                        },
                        value   => '',
                        label   => ''
                    ),
                    Zaaksysteem::ZAPI::CRUD::Interface::Filter->new(
                        name    => 'concepts',
                        type    => 'select',
                        data    => {
                            options => [
                                pairmap {
                                    {
                                        value    => $a,
                                        label    => $b
                                    }
                                } @{$gm->filter_concepts}
                            ],
                        },
                        value   => '',
                        label   => ''
                    ),
                    Zaaksysteem::ZAPI::CRUD::Interface::Filter->new(
                        name    => 'address',
                        type    => 'select',
                        data    => {
                            options => [
                                pairmap {
                                    {
                                        value    => $a,
                                        label    => $b
                                    }
                                } @{$gm->filter_address}
                            ],
                        },
                        value   => '',
                        label   => ''
                    ),
                    Zaaksysteem::ZAPI::CRUD::Interface::Filter->new(
                        name    => 'deceased',
                        type    => 'select',
                        data    => {
                            options => [
                                pairmap {
                                    {
                                        value    => $a,
                                        label    => $b
                                    }
                                } @{$gm->filter_deceased}
                            ],
                        },
                        value   => '',
                        label   => ''
                    ),
                    Zaaksysteem::ZAPI::CRUD::Interface::Filter->new(
                        name    => 'muncipality',
                        type    => 'select',
                        data    => {
                            options => [
                                pairmap {
                                    {
                                        value    => $a,
                                        label    => $b
                                    }
                                } @{$gm->filter_muncipality}
                            ],
                        },
                        value   => '',
                        label   => ''
                    ),
                ];
        }

        push @{ $filters }, Zaaksysteem::ZAPI::CRUD::Interface::Filter->new(
            name    => 'freeform_filter',
            type    => 'text',
            value   => '',
            label   => 'Zoeken'
        );

        $c->stash->{zapi} = [
            Zaaksysteem::ZAPI::CRUD::Interface->new(
                options => { select => 'all', },
                actions => $actions,
                filters => $filters,
                columns => \@columns,
                url     => '<[cl]>'
            )
        ];
        $c->detach();
    }

    $c->session->{datastore} = $c->req->params;
    $c->stash->{zapi} = $model->get_data_for_class($c->req->params);
}

=head2 classes

Shows datastore classes

=head3 URI /datastore/classes

=cut

sub classes
    : Chained('/')
    : PathPart('datastore/classes') {
    my ($self, $c) = @_;
    my $classes = DATASTORE_CLASSES;
    $c->stash->{json} = \@$classes;
    $c->forward('Zaaksysteem::View::JSON');
}

=head2 csv

Export the data to CSV

=head3 URI /datastore/csv

=cut

sub csv : Chained('/') : PathPart('datastore/csv') : Args(1) {
    my ($self, $c, $class) = @_;

    my $queue = $c->model('Queue');
    my $item = $queue->create_item(
        {
            label   => "Export datastore for $class",
            type    => "datastore_export",
            subject => $c->user,

            # Make sure the user ID is there, because the ignore existing will
            # otherwise prevent two users to export the same class.
            data => {
                user_id       => $c->user->id,
                class         => $class,
                export_params => $c->session->{datastore} // {},
            },
            metadata => {
                request_id    => $c->get_zs_session_id,
            },
            requires_object_model => 0,
        }
    );

    $queue->queue_item($item) if $item;

    $c->stash->{zapi} = [ { success => \1 } ];
    return;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, 2020 Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
