package Zaaksysteem::Controller::Sysin::Interface::SOAP;
use Moose;

use XML::LibXML;
use Zaaksysteem::Backend::Sysin::Modules;
use BTTW::Tools;

use MIME::Base64 qw(encode_base64 decode_base64);

BEGIN { extends 'Zaaksysteem::General::SOAPController' }

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Controller::Sysin::Interface::SOAP - SOAP API endpoint for interfaces

=head1 USAGE

=over

=item * Create an interface that supports incoming SOAP calls

=item * Use the endpoint C</sysin/interface/ID_HERE/soap> (replace "ID_HERE" with the id of the interface)

=back

=head2 enter

Main SOAP endpoint code. Starts a transaction with the incoming SOAP XML
message as its input on the specified interface.

URL: /sysin/interface/INTERFACE_ID/soap

=cut

sub enter :Chained('/sysin/interface/base'): PathPart('soap'): Args(0): SOAP('DocumentLiteral')  {
    my ($self, $c) = @_;

    $c->forward('process_soap');

}

sub process_soap : Private {
    my ($self, $c) = @_;

    my $interface = $c->stash->{entry};
    my $module = $interface->module_object;

    if ($c->stash->{client_certificate_valid}) {
        $c->log->debug("Client certificate checked by ZS::Controller::API::Soap");
    }
    elsif ($module->does('Zaaksysteem::Backend::Sysin::Modules::Roles::ClientCertificate')) {
        $c->log->debug("Interface requires client certificates");
        $c->forward('assert_authorized_client');
    }
    else {
        $c->log->debug("Interface does not require client certificates");
    }

    unless ($interface->active) {
        $c->res->code(404);
        $c->stash->{soap}->fault(
           {
               code     => 'Client',
               reason   => 'Not found',
               detail   => 'Given interface not active'
           }
        );
        return;
    }

    my $xml = $c->stash->{soap}->envelope();

    my $soapaction = try {
        return $c->assert_soap_action;
    } catch {
        $c->log->info("SOAP action assertion failed, legacy behaviour pass through: $_");
        return;
    };

    my $transaction    = $interface->process({
        defined $soapaction ? (soapaction     => $soapaction) : (),
        input_data     => $xml,
        transaction_id => 'unknown'
    });

    unless ($transaction->transaction_records->count) {
        $c->res->code(500);
        $c->stash->{soap}->fault(
           {
               code   => 'Server',
               reason => 'No response from Sysin',
               detail => 'Error from Sysin, see logfiles for transaction id: '
                            . $transaction->id
           }
        );

        return;
    }

    my $record = $transaction->transaction_records->first;

    if ($record->is_error) {
        $c->res->code(500);

        if ($interface->module_object->can('soap_error')) {
            my $error = $interface->module_object->soap_error($record);

            my $error_soap = try {
                return XML::LibXML->load_xml(string => $error)->documentElement;
            } catch {
                $self->log->info("Error parsing response XML: $_");
                return $error;
            };

            $c->stash->{soap}->fault(
                {
                    code => 'Server',
                    reason => 'Error while processing record. See logs.',
                    detail => $error_soap,
                }
            );
        }
        else {
            $c->stash->{soap}->fault(
               {
                   code   => 'Server',
                   reason => 'Failed processing record, see logfile.',
                   detail => 'Transaction id: ' . $transaction->id
               }
            );
        }
        return;
    }

    $c->forward('/execute_post_request_actions');

    my $output = $record->output;
    if (defined $output && length($output) > 0) {
        $self->_set_soap_stash($c, $output);
    }
    else {
        $c->stash->{soap}->string_return("Request processed");
    }

    return;
}

sub _set_soap_stash {
    my $self = shift;
    my $c    = shift;
    my $msg  = shift;

    try {
        my $responsexml = XML::LibXML->load_xml(
            string => $msg,
        );

        $c->stash->{soap}->literal_return($responsexml->documentElement());
    }
    catch {
        $c->stash->{soap}->string_return($msg);
    }
}

=head2 assert_authorized_client

Asserts that the client certificate supplied by the client is the same as the
one configured in the interface.

=cut

sub assert_authorized_client : Private {
    my ($self, $c) = @_;

    my $interface = $c->stash->{entry};
    my $module = $interface->module_object;

    my $certificate_match = try {

        if ($ENV{ZS_NO_CLIENT_CERT_CHECK}) {
            $c->log->debug('Client certificate check skipped due to developer settings');
            return 1;
        }

        my $client_certs = $c->get_client_certs;
        if (!$client_certs) {
            throw(
                "assert_authorized_client/client_cert/missing",
                "Unable to check client certificate, missing from client!"
            );
        }

        if ($module->does('Zaaksysteem::Backend::Sysin::Modules::Roles::SOAPAPI')) {
            return $interface->check_client_certificate(
                client_s_dn => $client_certs->{dn},
                client_cert => $client_certs->{cert},
            );
        }
        else {
            $module->assert_client_certificate(
                dn                 => $client_certs->{dn},
                client_cert        => $client_certs->{cert} || '',

                get_certificate_cb => sub {
                    my $certificate = $c->model('DB::Filestore')->find(
                        $interface->jpath('$.client_certificate[0].id')
                    );

                    return $certificate;
                },
            );
        }

    } catch {
        $c->log->error("Error validating client certificate: $_");

        $interface->process_generic_error("$_", {
            short_error => 'Client-certificaat validatie fout'
        });

        return;
    };

    if (!$certificate_match) {
        $c->res->code(403);
        $c->stash->{soap}->fault(
            {
                code   => 'Client',
                reason => 'Forbidden',
                detail => 'Client certificate verification failed.',
            },
        );
        $c->detach();
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
