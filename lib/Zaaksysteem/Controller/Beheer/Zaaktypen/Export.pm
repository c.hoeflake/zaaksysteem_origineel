package Zaaksysteem::Controller::Beheer::Zaaktypen::Export;

use Moose;
use namespace::autoclean;

use Hash::Merge::Simple qw( clone_merge );

use Data::Dumper;
$Data::Dumper::Sortkeys = 1;

use Archive::Tar::Stream;
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );
use Data::Visitor::Callback;
use Encode;
use File::Spec::Functions qw(catfile);
use File::stat;
use Params::Profile;
use XML::Dumper;
use XML::Simple;
use Zaaksysteem::Constants;
use Zaaksysteem::Tie::CallingHandle;
use Zaaksysteem::Zaaktypen::DateTimeWrapper;
use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::Controller' }

sub base : Chained('/') : PathPart('beheer/zaaktypen'): CaptureArgs(1) {
    my ( $self, $c, $zaaktype_id ) = @_;

    if ($zaaktype_id =~ /^[0-9]+$/) {
        $c->stash->{zaaktype_id} = $zaaktype_id;
    } else {
        my $zaaktype = $c->model('DB::Zaaktype')->search_rs(
            { uuid => $zaaktype_id }
        )->single;
        $c->stash->{zaaktype_id} = $zaaktype->id;
    }
}


sub export : Chained('base') : PathPart('export') {
    my ( $self, $c ) = @_;

    $self->{zaaktypen_model} = $c->model('Zaaktypen');

    my $zaaktype = $self->{zaaktypen_model}->export(
        id => $c->stash->{zaaktype_id},
    );

    $c->stash->{is_array} = sub { my $arg = shift; return ref $arg && ref $arg eq 'ARRAY' };

    $c->stash->{'zaaktype'} = $zaaktype;

    try {
        $self->_export_zaaktype($c, $c->stash->{zaaktype_id});
    } catch {
        $self->log->info("Unable to export casetype: $_");
        $c->stash->{export_error} = $_;
        $c->stash->{template} = 'beheer/zaaktypen/export.tt';
    };
}

# TODO Evacuate zaaktype export code to a model

sub _export_zaaktype {
    my ($self, $c, $zaaktype_id, $out_filename) = @_;

    # flush accumulated stuff
    delete $c->stash->{zaaktype_export};

    my $zaaktype = $c->model('Zaaktypen')->export(
        id => $zaaktype_id,
    );

    $self->_preprocess_zaaktype({c => $c, data => $zaaktype});

    $zaaktype->{zaaktype}->{id} = $zaaktype_id;
    $zaaktype->{db_dependencies} = $c->stash->{zaaktype_export}->{db_dependencies};
    $zaaktype->{origin} = $c->req->base->as_string;
    $c->stash->{zaaktype} = $zaaktype;

    $self->encode_notificatie_newlines($zaaktype);

    # Header: download, include file name
    {
        my $filename = $zaaktype->{node}->{titel};

        my $safe_filename_characters = "a-zA-Z0-9_.-";
        $filename =~ tr/ /_/;
        $filename =~ s/[^$safe_filename_characters]/_/g;

        $filename = $zaaktype_id . "_" . $filename . ".ztb";

        $c->res->headers->header(
            'Content-Disposition' => "attachment; filename=\"$filename\""
        );
        $c->res->content_type('application/zip');
    }

    my $zip = Archive::Zip->new();

    # DateTime's internals can be a bit strange.
    my $visitor = Data::Visitor::Callback->new(
        'DateTime' => sub {
            my ($v, $obj) = @_;

            $_ = Zaaksysteem::Zaaktypen::DateTimeWrapper->new_from_datetime($obj);
        },
    );
    $visitor->visit($zaaktype);

    my $xml_output = XML::Dumper::pl2xml($zaaktype);
    $xml_output = encode("utf-8", $xml_output);

    # main xml body
    $zip->addString($xml_output, 'zaaktype.xml');

    # attachments
    my $attached_files = $self->_attached_files($c);
    foreach my $attached_file (keys %$attached_files) {
        my $filepath = $attached_files->{$attached_file};

        if($filepath && -e $filepath) {
            my $file_member = $zip->addFile( "$filepath", $attached_file );
        } else {
            warn "Could not find filestore file, maybe because of test environment?\n";
            $zip->addString("Dummy file", $attached_file);
        }
    }

    # Write the output to the caller (browser).
    my $handle = Zaaksysteem::Tie::CallingHandle->create(
        write_cb => sub { $c->res->write($_[0]); },);

    unless ( $zip->writeToFileHandle($handle) == AZ_OK ) {
        die "Error while writing to '$out_filename': '$!'";
    }

    # Streaming downloads don't mix well with views.
    $c->res->body('');

    return;
}


=head2 encode_notificatie_newlines

for bibliotheek_notificaties a match will be made on a multi-line field
our xml serialization-deserialization system does a few little tricks with newlines
e.g. /r/n => /n
to prevent any confusion on the other side, the newline chars are encoded.
on the receiving end the reverse process is done.

=cut

sub encode_notificatie_newlines {
    my ($self, $zaaktype) = @_;

    if(my $bibliotheek_notificaties = $zaaktype->{db_dependencies}->{BibliotheekNotificaties}) {
        foreach my $id (keys %$bibliotheek_notificaties) {
            my $bibliotheek_notificatie = $bibliotheek_notificaties->{$id};
            $bibliotheek_notificatie->{message} =~ s|\n|&#xA;|gis;
            $bibliotheek_notificatie->{message} =~ s|\r|&#xD;|gis;
        }
    }
}






{
    Params::Profile->register_profile(
        method  => '_preprocess_zaaktype',
        profile => {
            required        => [ qw/
                c
                data
            /],
            'optional'      => [ qw/
                parent
            /],
            'constraint_methods'    => {
            },
        }
    );


    sub _preprocess_zaaktype {
        my ($self, $params) = @_;

        my $dv = Params::Profile->check(params  => $params);
        die "invalid options" unless $dv->success;

        my $c       = $params->{c};
        my $data   = $params->{data};
        my $parent  = $params->{parent} || '';

        foreach my $key (keys %$data) {
            my $child_data = $data->{$key};

            if($key eq 'zaaktype_node_id') {
                #look up the title, substitute that
                my $zaaktype_node_id = $data->{$key};
                die "need zaaktype_node_id" unless $zaaktype_node_id;
                my $zaaktype_node = $c->model('DB::ZaaktypeNode')->find($zaaktype_node_id);

                $data->{zaaktype_titel} = $zaaktype_node->zaaktype_id->zaaktype_node_id->titel;
                delete $data->{$key};
            }

            if( $key eq 'zaaktype_definitie_id' || $key eq 'zaak_status_id' || $key eq 'zaaktype_categorie_id') {
                delete $data->{$key};
            }

            if ($key eq 'standaard_betrokkenen') {
                for my $sb (keys %{ $data->{$key} }) {
                    # Delete sensitive fields
                    delete $data->{$key}{$sb}{betrokkene_identifier};
                    delete $data->{$key}{$sb}{naam};
                    # Don't copy the UUID
                    delete $data->{$key}{$sb}{uuid};
                }
            }

            if(ref $child_data && ref $child_data eq 'HASH') {
                # skip number when deciding who's the parent
                my $parent = $key =~ m|^\d+$| ? $parent : $key;
                $self->_preprocess_zaaktype({c => $c, data => $child_data, parent => $parent});
            } elsif(ref $child_data && ref $child_data eq 'ARRAY') {
                #$c->log->debug("arrray found: " . Dumper $child_data);
            } else {
                if($parent eq 'regels' && $key eq 'settings') {
                    delete $data->{$key};
                } else {
                    $self->_lookup_id_field({c => $c, data => $data, key => $key});
                }
            }
        }
    }
}



{
    Params::Profile->register_profile(
        method  => '_include_reference',
        profile => {
            required        => [ qw/
                c
                data
                key
            /],
            'optional'      => [ qw/
                from_can_id
            /],
            'constraint_methods'    => {
            },
        }
    );


    sub _include_reference {
        my ($self, $params) = @_;

        my $dv = Params::Profile->check(params  => $params);
        die "invalid options" unless $dv->success;

        my $c               = $params->{c};
        my $data            = $params->{data};
        my $key             = $params->{key};
        my $from_can_id     = $params->{from_can_id};


        my $id = $data->{$key};

        if($key eq 'role_id') {
            my $role = $c->user->find_role_by_id($id);

            $role ||= 'Rol niet gevonden ('. $id . ')';
            $self->_db_dependency({
                c       => $c,
                table   =>'LdapRole',
                id      => $id,
                db_row  => { short_name => (ref $role ? $role->name : $role) }
            });
            return;
        } elsif($key eq 'ou_id') {
            my $group = $c->user->find_group_by_id($id);

            $group ||= 'Organisatorische eenheid niet gevonden ('. $id . ')';
            $self->_db_dependency({
                c       => $c,
                table   => 'LdapOu',
                id      => $id,
                db_row  => { ou => (ref $group ? $group->name : $group) }
            });
            return;
        } elsif($key eq 'object_id') {
            my $object = $c->model('Object')->retrieve(uuid => $id);

            # XXX This will break for all object types that don't have a "name"
            $self->_db_dependency({
                c      => $c,
                table  => 'ObjectData',
                id     => $id,
                db_row => { naam => $object->name },
            });

            return;
        }

        $self->_find_and_include_ref(key => $key, id => $id, c => $c, data => $data);

    }
}

sub _find_and_include_ref {
    my $self = shift;
    my $args = {@_};

    my $c    = $args->{c};
    my $data = $args->{data};
    my $id   = $args->{id};
    my $key  = $args->{key};

    my $table = $self->_tablename({ id_field => $key }) or return;
    my $db_row = $c->model("DB::".$table)->find($id);

    die "incorrect zaaktype, could not find $table $id" . Dumper $data unless($db_row);

    my $hash = $self->_retrieve_db_row_as_hash({
        c       => $c,
        table   => $table,
        db_row  => $db_row
    });

    if ($hash->{pid}) {
        $self->_find_and_include_ref(key => $key, id => $hash->{pid}, c => $c, data => $data);
    }

    if ($c->stash->{zaaktype_export}->{db_dependencies}->{$table}->{$id}) {
        return;
    }


    $self->_db_dependency({
        c       => $c,
        table   => $table,
        id      => $id,
        db_row  => $hash
    });

    # this stuff should be in the model layer, so i'm starting to move.
    # on the next overhaul, all these functions should be moved downward.
    # will save quite a bit of code, no more passing around $c for starters.
    if (defined $self->{zaaktypen_model}) {
        if ($table eq 'BibliotheekKenmerken') {
            $self->{zaaktypen_model}->process_bibliotheek_kenmerken($hash, $db_row)
        }
        elsif ($table eq 'BibliotheekNotificaties') {
            $self->{zaaktypen_model}->process_bibliotheek_notificaties($hash, $db_row)
        }
    }
}


{
    Params::Profile->register_profile(
        method  => '_retrieve_db_row_as_hash',
        profile => {
            required        => [ qw/
                c
                table
                db_row
            /],
            'optional'      => [ qw/
            /],
            'constraint_methods'    => {
            },
        }
    );


    sub _retrieve_db_row_as_hash {
        my ($self, $params) = @_;

        my $dv = Params::Profile->check(params  => $params);
        die "invalid options" unless $dv->success;

        my $c       = $params->{c};
        my $table   = $params->{table};
        my $db_row  = $params->{db_row};

        my $result = {};
        my @columns = $db_row->result_source->columns;

        for my $column (@columns) {
            next if($column eq 'id');

            if (blessed($db_row->$column) && $db_row->$column->can('id')) {
                my $id = $result->{$column} = $db_row->$column->id;

                $self->_find_and_include_ref(key => $column, id => $id, c => $c, data => {});
            } elsif (!ref($db_row->$column)) {
                $result->{$column} = $db_row->$column;
            }

        }

        if(exists $result->{zaaktype_node_id}) {
            #look up the title, substitute that
            my $zaaktype_node_id = $result->{zaaktype_node_id};
            my $zaaktype_node = $c->model('DB::ZaaktypeNode')->find($zaaktype_node_id);
            $result->{zaaktype_titel} = $zaaktype_node->zaaktype_id->zaaktype_node_id->titel;
            delete $result->{zaaktype_node_id};
        }

        # Add references to the files
        if($table eq 'Filestore') {
            my $filestore_path = $db_row->get_path;
            $self->_attached_files($c)->{$db_row->id} = $filestore_path;
        }

        if ($table eq 'BibliotheekSjablonen') {
            $result->{interface_module_name} = $db_row->interface_id->module if $db_row->interface_id;
        }

        return $result;
    }
}



{
    Params::Profile->register_profile(
        method  => '_lookup_id_field',
        profile => {
            required        => [ qw/
                c
                data
                key
            /],
            'optional'      => [ qw/
            /],
            'constraint_methods'    => {
            },
        }
    );

    sub _lookup_id_field : Private {
        my ($self, $params) = @_;

        my $dv = Params::Profile->check(params  => $params);
        die "invalid options" unless $dv->success;

        my $c       = $params->{c};
        my $data    = $params->{data};
        my $key     = $params->{key};

        return unless($key =~ m|_id$| || $key =~ m|_kenmerk$|);

        return unless($data->{$key});

        if ((($data->{$key} =~ m|^\d+$|) && int($data->{$key}) > 0) || ($data->{$key} =~ /^[0-9a-z]{8}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{12}$/i)
        ) {
            $self->_include_reference({c => $c, data => $data, key => $key});
        }

        return;
    }
}




{
    Params::Profile->register_profile(
        method  => '_tablename',
        profile => {
            required        => [ qw/
                id_field
            /],
            'optional'      => [ qw/
            /],
            'constraint_methods'    => {
            },
        }
    );

    sub _tablename {
        my ($self, $params) = @_;

        my $dv = Params::Profile->check(params  => $params);
        die "invalid options" unless $dv->success;

        my $id_field = $params->{id_field};

        my $config = ZAAKTYPE_DEPENDENCY_IDS;
        foreach my $regexp (keys %$config) {
            next unless($id_field =~ m|$regexp|);

            return $config->{$regexp};
        }

        return undef;
    }
}




{
    Params::Profile->register_profile(
        method  => '_db_dependency',
        profile => {
            required        => [ qw/
                c
                table
                id
            /],
            'optional'      => [ qw/
                db_row
            /],
            'constraint_methods'    => {
            },
        }
    );

    sub _db_dependency : Private {
        my ($self, $params) = @_;

        my $dv = Params::Profile->check(params  => $params);
        die "invalid options" unless $dv->success;

        my $c       = $params->{c};
        my $table   = $params->{table};
        my $id      = $params->{id};
        my $db_row  = $params->{db_row};


        if($db_row) {
            $c->stash->{zaaktype_export}->{db_dependencies}->{$table}->{$id} ||= $db_row;
        } else {
            return $c->stash->{zaaktype_export}->{db_dependencies}->{$table}->{$id};
        }
    }
}

sub _attached_files : Private {
    my ($self, $c) = @_;

    return $c->stash->{zaaktype_export}->{attached_files} ||= {};
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 AZ_OK

TODO: Fix the POD

=cut

=head2 ZAAKTYPE_DEPENDENCY_IDS

TODO: Fix the POD

=cut

=head2 base

TODO: Fix the POD

=cut

=head2 download

TODO: Fix the POD

=cut

=head2 export

TODO: Fix the POD

=cut

=head2 exportall

TODO: Fix the POD

=cut

