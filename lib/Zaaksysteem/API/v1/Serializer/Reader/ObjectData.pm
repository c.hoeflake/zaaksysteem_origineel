package Zaaksysteem::API::v1::Serializer::Reader::ObjectData;
use Moose;
with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::ObjectData - Read ObjectData rows

=head1 SYNOPSIS

    my $reader = Zaaksysteem::API::v1::Serializer::Reader::ObjectData->grok($object);

    my $data = $reader->($serializer, $object);

=head1 DESCRIPTION

This class implements a serializer reader for
L<Zaaksysteem::Backend::Object::Data::Component> objects.

This class should not be used outside of the
L<Zaaksysteem::API::v1::Serializer> infrastructure.

=cut

use BTTW::Tools;
use Zaaksysteem::Constants qw[ZAAKSYSTEEM_CONSTANTS];

use Zaaksysteem::Object::Model;
use Zaaksysteem::Object::Types::Case::Milestone;

use List::MoreUtils qw[any];
use Encode qw[encode_utf8];

use constant CASE_ATTRIBUTE_WHITELIST => [qw[
    number
    status
    subject
    subject_external
    confidentiality
    number_parent
    number_master
    number_previous

    phase

    result
    result_id

    date_target
    date_of_registration
    date_of_completion
    date_destruction

    aggregation_scope
    channel_of_contact
    payment_status
    stalled_since
    stalled_until
    suspension_rationale
    premature_completion_rationale
    archival_state
    price
    active_selection_list

    current_deadline
    deadline_timeline
]];

=head1 METHODS

=head2 grok

Implements sub required by L<Zaaksysteem::API::v1::Serializer>.

=cut

sub grok {
    my ($class, $object) = @_;

    return unless blessed $object && $object->isa($class->class);

    if ($object->object_class eq 'case') {
        return sub { $class->read_case(@_) };
    }

    return;
}

=head2 class

Implements sub required by L<Zaaksysteem::API::v1::Serializer::ReaderRole>.

=cut

sub class { 'Zaaksysteem::Backend::Object::Data::Component' }

=head2 read_case

Implements sub required by L<Zaaksysteem::API::v1::Serializer::ReaderRole>.


=cut

sig read_case => 'Zaaksysteem::API::v1::Serializer, Object, ?HashRef => HashRef';

my @relation_keys = qw(route assignee requestor coordinator outcome milestone);

sub read_case {
    my $class = shift;
    my ($serializer, $object, $opts) = @_;

    # We need the the object model to process embedded subjects
    my $object_model = Zaaksysteem::Object::Model->new(
        schema => $object->result_source->schema
    );

    my $attributes = $class->_get_case_attributes($object_model, @_);

    ### This is uncommented, why? Because we run it on every request but
    ### $documents is never used. When we do? Uncomment the below code.
    ### and the precache in C::API::v1::Case->list
    #my $documents   = $class->_load_documents(@_);

    my %base_attributes = (
        date_created  => $serializer->read($object->date_created),
        date_modified => $serializer->read($object->date_modified)
    );

    my @base_attrs = map {
        $object->get_object_attribute(sprintf('case.%s', $_))
    } @{ CASE_ATTRIBUTE_WHITELIST() };

    for my $base_attr (@base_attrs) {
        my (undef, $key) = split m[\.], $base_attr->name;
        my $value = $base_attr->value;

        $base_attributes{ $key } = blessed $value ? $serializer->read($value) : $value;
    }

    # Cleanup output
    my $casetype_name = $object->get_object_attribute('case.casetype.name');
    my $casetype_version = $object->get_object_attribute('case.casetype.version');

    my %relations = map {
        $_->name => $_
    } $object->object_relation_object_ids->all;

    foreach my $type (@relation_keys) {
        try {
            if (defined $relations{$type}) {
                my $relation = $object_model->inflate_from_json(encode_utf8(
                        $relations{ $type }->get_column('object_embedding')
                ));
                $base_attributes{ $type } = $serializer->read($relation);
            }
            else {
                $base_attributes{ $type } = undef;
            }
        }
        catch {
            $class->log->info(sprintf("Unable to serialize relation %s for object with id %s: %s", $type, $object->id, $_));
            die $_;
        };
    }

    $base_attributes{ casetype } = {
        type => 'casetype',
        reference => $object->get_column('class_uuid'),
        preview => $casetype_name->value,
        instance => {
            name => $casetype_name->value,
            version => $casetype_version->value
        }
    };

    for my $attr (qw[case_location correspondence_location]) {
        unless (any { defined } values %{$base_attributes{ $attr }}) {
            $base_attributes{ $attr } = undef;
        }
    }

    ### Relationships
    my $relations = $class->_load_relations(@_);

    return {
        type => 'case',
        reference => $object->id,
        preview => $object->TO_STRING,
        instance => {
            id            => $object->id,
            attributes    => $attributes,

            %base_attributes,
            %$relations,
        }
    };
}

sub _get_case_attributes {
    my $class = shift;
    my ($object_model, $serializer, $object, $opts) = @_;

    my $rv = { };

    my $casetype = $object->_init_casetype_relation();

    my @all_attributes;
    for my $phase (@{ $casetype->object_embedding->{values}{instance_phases} }) {
        for my $attribute (@{ $phase->{values}{attributes} }) {
            next unless $attribute->{ catalogue_id };

            my $key = $attribute->{ magic_string };

            my $attrname = sprintf('attribute.%s', $key);

            next unless $object->has_object_attribute($attrname);

            my $value = $object->get_object_attribute($attrname)->value;

            if ($attribute->{ type } eq 'file') {
                $rv->{ $key } = [
                    map { blessed $_ ? $_->uuid : $_->{uuid} } @{ $value }
                ];
            } elsif ($attribute->{ type } eq 'appointment') {
                if ($value) {
                    my $appointment = $object_model->retrieve(uuid => $value);
                    if ($appointment) {
                        $rv->{ $key } = [ $serializer->read($appointment) ];
                    }
                    else {
                        $class->log->warn(
                            sprintf(
                                "Unable to retrieve appointment '%s' for case '%s'",
                                $value, $object->id
                            )
                        );
                        $rv->{ $key } = [];
                    }
                }
                else {
                    $rv->{ $key } = [];
                }
            } else {
                $rv->{ $key } = [
                    $class->_process_case_attribute_value($serializer, $value)
                ];
            }
        }
    }

    return $rv;
}

=head2 _load_relations

    my $relations_serialized = $self->_load_relations

=cut

sub _load_relations {
    my $class   = shift;
    my ($serializer, $object, $opts) = @_;
    my %rv;

    my %mapping = (
        'relations'        => 'case.related_uuids',
    );

    for my $reltype (keys %mapping) {
        $rv{$reltype} = {
            type        => 'set',
            instance    => {
                rows    => [],
            }
        };

        next unless ($object->has_object_attribute($mapping{$reltype}));

        my $value = $object->get_object_attribute($mapping{$reltype})->value;
        $value    = blessed $value ? $serializer->read($value) : $value;

        my @ids = split(/,/, $value);

        for my $id (@ids) {
            push (
                @{ $rv{$reltype}->{instance}->{rows} },
                {
                    reference   => $id,
                    type        => 'case',
                }
            );
        }
    }

    my @relations;

    # Only *our* relations
    if ($opts->{object_relationships}) {
        @relations = grep {
               ($_->get_column('object1_uuid') eq $object->id)
            || ($_->get_column('object2_uuid') eq $object->id)
        } @{ $opts->{object_relationships} };
    } elsif ($opts->{object_relationships_rs}) {
        @relations = grep {
               ($_->get_column('object1_uuid') eq $object->id)
            || ($_->get_column('object2_uuid') eq $object->id)
        } $opts->{object_relationships_rs}->all;
    }

    my @rows = map {
        {
            reference => $_->get_column('object1_uuid') eq $object->id
                ? $_->get_column('object2_uuid')
                : $_->get_column('object1_uuid'),
            type => 'case',
        };
    } grep {
           ($_->get_column('object1_uuid') eq $object->id && $_->type1 eq 'parent')
        || ($_->get_column('object2_uuid') eq $object->id && $_->type2 eq 'parent')
    } @relations;

    my $child_relations = {
        type        => 'set',
        instance    => {
            rows    => \@rows,
        }
    };

    my $parent_relation = undef;
    if ($object->has_object_attribute('case.parent_uuid') && (my $value = $object->get_object_attribute('case.parent_uuid')->value)) {
        $parent_relation = {
            reference   => $value,
            type        => 'case'
        }
    }

    my @other_relations = grep {
           $_->type1 ne 'parent'
        && $_->type1 ne 'child'
    } @relations;

    my $rel = $class->_get_relationships_per_type($object, \@other_relations);

    $rv{case_relationships} = {
        child => $child_relations,
        parent => $parent_relation,
        %$rel,
    };

    return \%rv;
}

=head2 _load_documents

Loads documents, either from C<serializer_opts> or from database

=cut

sub _load_documents {
    my $class = shift;
    my ($serializer, $object, $opts) = @_;

    my @case_documents;
    if ($opts->{ case_documents }) {
        @case_documents = grep { $_->get_column('case_id') == $object->object_id }  @{ $opts->{ case_documents } };
    } else {
        @case_documents = $object->documents->all;
    }

    return {
        type => 'set',
        instance => {
            rows => [
                map { $serializer->read($_) }
                @case_documents
            ]
        }
    };
}

sub _get_relationships_per_type {
    my ($class, $object, $relations) = @_;

    my $relationships = {};
    for my $rel (@$relations) {
        my $relationship_type;
        my $relationship_id;

        if ($rel->get_column('object1_uuid') eq $object->id) {
            $relationship_type = $rel->get_column('type2');
            $relationship_id   = $rel->get_column('object2_uuid');
        }
        else {
            $relationship_type = $rel->get_column('type1');
            $relationship_id   = $rel->get_column('object1_uuid');
        }

        $relationships->{$relationship_type} //= {
            type     => 'set',
            instance => {
                rows => [],
            },
        };

        push(@{$relationships->{$relationship_type}{instance}{rows}}, {
            reference => $relationship_id,
            type      => 'case',
        });
    }

    return $relationships;
}

sub _process_case_attribute_value {
    my $self        = shift;
    my $serializer  = shift;
    my $value       = shift;

    my $processor   = sub {
        my $val         = shift;

        if (blessed($val)) {
            $val = $serializer->read($val);
        }

        return $val;
    };

    if (ref($value) eq 'ARRAY') {
        return [ map { $processor->($_) } @$value];
    }

    return $processor->($value);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
