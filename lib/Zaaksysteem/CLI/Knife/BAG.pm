package Zaaksysteem::CLI::Knife::BAG;

use Moose::Role;
use Zaaksysteem::CLI::Knife::Action;

has knife => (
    is      => 'ro',
    isa     => 'Str',
    default => 'bag',
);

=head1 NAME

Zaaksysteem::CLI::Knife::BAG - BAG CLI actions

=head1 SYNOPSIS

    See USAGE from commandline output zsknife

=cut

register_knife bag => (
    description => "BAG related functions"
);

register_category nummeraanduiding => (
    knife       => 'bag',
    description => "BAG actions"
);

register_action delete_duplicates => (
    knife       => 'bag',
    category    => 'nummeraanduiding',
    description => 'Delete duplicate BAG nummeraanduidingen based on the identification code',
    run         => sub {
        my $self = shift;
        my (@params) = @_;

        my $search = $self->get_knife_params;

        if ($search) {
            $self->log->error("No search params required");
            return;
        }
        $self->deduplicate_bag_nummeraanduidingen;
    }
);

register_action list_duplicates => (
    knife       => 'bag',
    category    => 'nummeraanduiding',
    description => 'List duplicate BAG nummeraanduidingen based on the identification code',
    run         => sub {
        my $self = shift;
        my (@params) = @_;

        my $search = $self->get_knife_params;

        if ($search) {
            $self->log->error("No search params required");
            return;
        }
        $self->list_duplicate_bag_nummeraanduidingen;
    }
);

=head2 deduplicate_bag_nummeraanduidingen

Deduplicates bag nummeraanduidingen.
BAG nummeraanduidingen are basicly addresses in a normalized form.

=cut

sub deduplicate_bag_nummeraanduidingen {
    my $self = shift;

    my $rs = $self->get_duplicate_bag_nummeraanduiding;

    my %dupes = ();
    while(my $bag = $rs->next) {
        if ($dupes{$bag->identificatie}) {
            $self->log->info(
                sprintf("Deleted duplicate BAG nummeraanduiding with ID %d",
                    $bag->id)
            );
            $bag->delete;
        }
        else {
            $dupes{$bag->identificatie} = 1;
        }
    }
}

=head2 list_duplicate_bag_nummeraanduidingen

List on duplicate nummeraanduidingen from the database

=cut

sub list_duplicate_bag_nummeraanduidingen {
    my $self = shift;
    my $rs = $self->get_duplicate_bag_nummeraanduiding;
    while(my $bag = $rs->next) {
        $self->report_duplicate($bag);
    }
}

=head2 report_duplicate

Report on duplicate nummeraanduidingen from the database

=cut

sub report_duplicate {
    my ($self, $bag) = @_;

    $self->log->info(
        sprintf(
            "BAG [%d] %s: %s",
            $bag->id,
            $bag->identificatie,
            $bag->geocode_term
        )
    );
    return 1;
}

=head2 get_duplicate_bag_nummeraanduiding

Get duplicate nummeraanduidingen from the database

=cut

sub get_duplicate_bag_nummeraanduiding {
    my $self = shift;

    my $sql = "select identificatie from bag_nummeraanduiding where einddatum is null group by identificatie having count(identificatie) > 1";
    return $self->schema->resultset('BagNummeraanduiding')->search(
        {
            identificatie => { in => \"($sql)" }
        },
        {
            order_by => { -asc => [ 'me.identificatie', 'me.id' ] }
        }
    );
}

1;

=head1 SEE ALSO

L<Zaaksysteem::CLI::Knife>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
