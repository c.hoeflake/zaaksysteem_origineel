=head1 NAME

Zaaksysteem::ZAPI - General ZAPI documentation

=head1 SYNOPSIS

    package Zaaksysteem::Controller::Sysin::Interface;

    use Moose;
    use namespace::autoclean;

    use BTTW::Tools;

    BEGIN {extends 'Zaaksysteem::General::ZAPIController'; }

    sub index
        : Chained('/')
        : PathPart('interesting/table')
        : Args(0)
        : ZAPI
    {
        my ($self, $c) = @_;

        $c->stash->{zapi}   = $c->model('DB::InterestingTable')->search();
    }

    sub create
        : Chained('/')
        : PathPart('interesting/table/create')
        : Args(0)
        : ZAPI
    {
        my ($self, $c) = @_;

        $c->stash->{zapi} =
            $c->model('DB::InterestingTable')->interesting_create($c->req->params);
    }

    __PACKAGE__->meta->make_immutable;

    1;

=head1 DESCRIPTION

ZAPI is a selection of helpers for creating a maintainable API for Angular. It is
mainly a CRUD structure with ways to tell Angular how to display her data by
implementing a default CRUD interface and Form Generator.

=head1 COMPONENTS

=over 4

=item L<Zaaksysteem::ZAPI::Form> - A description about form generation

=item L<Zaaksysteem::ZAPI::CRUD::Interface> - A template for creating a CRUD
interface

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
