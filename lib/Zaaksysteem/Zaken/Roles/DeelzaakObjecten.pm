package Zaaksysteem::Zaken::Roles::DeelzaakObjecten;

use Moose::Role;

with 'MooseX::Log::Log4perl';

use BTTW::Tools;
use Zaaksysteem::Types qw[CaseNumber];
use Zaaksysteem::Constants qw(BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION);
use Zaaksysteem::Zaken::RelatedObjects;

=head2 copy_betrokkene_role_to_case

Copy all betrokkene of a role to another case. Returns a true-ish value if the
role was found in the case, otherwise false.

=cut

sig copy_betrokkene_role_to_case => 'Zaaksysteem::Zaken::ComponentZaak, Str';

sub copy_betrokkene_role_to_case {
    my ($self, $case, $role) = @_;

    my $rs = $case->get_zaak_betrokkenen({rol => $role});
    my $ok;
    while (my $involved = $rs->next) {

        $ok ||= 1;
        my $magic_string = BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION->([], '', $role);

        # Messing with the private parts
        my $object = $case->_load_betrokkene_object($involved);

        if ($self->log->is_trace) {
            $self->log->trace(
                sprintf(
                    "Copy betrokkene %s from case %d to case %d with role %s",
                    $object->betrokkene_identifier,
                    $case->id,
                    $self->id,
                    $role,
                )
            );
        }

        $self->betrokkene_relateren({
            betrokkene_identifier  => $object->betrokkene_identifier,
            magic_string_prefix    => $magic_string,
            rol                    => $role,
            pip_authorized         => $involved->pip_authorized // 0,
            send_auth_confirmation => 0,
        });
    }
    return $ok;
}


=head2 set_relatie

Set a relation to another case

=cut

define_profile set_relatie => (
    required => {
        relatie      => 'Str',
        relatie_zaak => 'Zaaksysteem::Zaken::ComponentZaak',
    },
    optional => {
        actie_kopieren_kenmerken => 'Bool',
        copy_related_cases       => 'Bool',
        copy_related_objects     => 'Bool',
        copy_subject_role        => 'Bool',
        subject_role             => 'Str',    # ArrayRef with string
        copy_selected_attributes => 'HashRef',
    },
    defaults => {
        copy_subject_role => 0,
    }
);

sub set_relatie {
    my $self = shift;
    my $opts = assert_profile(shift)->valid;

    my $relatie_zaak = $opts->{relatie_zaak};

    if ($opts->{relatie} eq 'gerelateerd') {
        $self->result_source->schema->resultset('CaseRelation')
            ->add_relation($self->id, $relatie_zaak->id);
    }
    elsif ($opts->{relatie} eq 'vervolgzaak'
        || $opts->{relatie} eq 'vervolgzaak_datum')
    {
        my $relation = $self->result_source->schema->resultset('CaseRelation')
            ->add_relation($self->id, $relatie_zaak->id);

        $relation->view_in_context($self->id)->type('initiator');
        $relation->view_in_context($relatie_zaak->id)->type('continuation');

        $relation->update;

        $self->vervolg_van($relatie_zaak->id);
        $self->update;
    }
    elsif ($opts->{relatie} eq 'deelzaak') {
        $self->pid($relatie_zaak->id);
        $self->update;
        $self->discard_changes;
        $self->update_referential_attributes_from_parent;

    }

    if ($opts->{copy_subject_role} && $opts->{subject_role}) {
        foreach my $role (@{ $opts->{subject_role} }) {
            $self->copy_betrokkene_role_to_case($relatie_zaak, $role);
        }
    }

    if ($opts->{actie_kopieren_kenmerken}) {
        $self->_copy_attributes_without_selection($relatie_zaak);
    }
    elsif ($opts->{copy_selected_attributes}) {
        $self->_copy_attributes_with_selection($relatie_zaak,
            $opts->{copy_selected_attributes});
    }

    if ($opts->{copy_related_cases}) {
        $self->copy_case_relationships($relatie_zaak);
    }

    if ($opts->{copy_related_objects}) {
        $self->copy_case_object_relationships($relatie_zaak);
    }

    $relatie_zaak->touch();
    $self->touch();
}

sub copy_case_object_relationships {
    my ($self, $case) = @_;

    my $schema = $self->result_source->schema;

    my $relation = $schema->resultset('CustomObjectRelationship');
    my $object_relations = $relation->search_rs(
        { related_case_id => $case->id }
    );

    while (my $rel = $object_relations->next) {
        $rel->copy(
            {
                related_case_id => $self->id,
                related_uuid    => $self->get_column('uuid'),
            }
        );
    }

    my $model = Zaaksysteem::Zaken::RelatedObjects->new(
        schema => $schema,
        case   => $case,
    );
    my $related_objects = $model->get_related_objects;

    my $rs = $schema->resultset('ObjectRelationships');
    foreach (@$related_objects) {
        $rs->create(
            {

                type1  => 'related',
                title1 => join(" ", $self->id, $self->zaaktype_node_id->titel),
                object1_type => 'case',
                object1_uuid => $self->get_column('uuid'),

                type2        => 'related',
                title2       => $_->{name},
                object2_type => $_->{type},
                object2_uuid => $_->{uuid},
            }
        );
    }

    return;
}

sub get_plain_relationships {
    my $self = shift;

    my $schema    = $self->result_source->schema;
    my $relations = $schema->resultset('CaseRelation')->search(
        {
            case_id => $self->id,
            type_a  => 'plain',
            type_b  => 'plain'
        }
    );

    my @relationships;
    while (my $cr = $relations->next) {

        my $case = $cr->get_column('case_id_a') == $self->id
                       ? $cr->case_id_b : $cr->case_id_a;

        push(@relationships, $case) if $case;
    }
    return \@relationships;
}

=head2 copy_case_relationships_from_case

Copy all plain relationships from a case to yourself.

=cut

sub copy_case_relationships {
    my ($self, $case) = @_;

    my $related = $case->get_plain_relationships;
    return unless @$related;

    my $rs = $self->result_source->schema->resultset('CaseRelation');
    foreach (@$related) {
        $rs->add_relation($self->id, $_->id);
    }

    $self->update_object_relationships;
    $self->touch();
}

sub _get_document_attribute_mapping {
    my ($self) = @_;

    my $document_attrs
        = $self->zaaktype_node_id->zaaktype_kenmerken->search_rs(
        {
            'bibliotheek_kenmerken_id.value_type' => 'file',
            $self->pid ? ('me.referential' => 0) : (),
        },
        { join => 'bibliotheek_kenmerken_id' },
        );

    my %attr_mapping;
    while (my $attr = $document_attrs->next) {

        my $magic_string = $attr->bibliotheek_kenmerken_id->magic_string;
        my $id           = $attr->id;

        $attr_mapping{ $magic_string } //= $id;
    }
    return \%attr_mapping;
}

sub _get_case_document_files {
    my ($self, $case, @magic_string) = @_;

    my @files = $case->case_documents->search_rs(
        { 'case_documents.magic_string' => { -in => \@magic_string } },
        {
            join => 'case_documents',
        }
    )->get_column('id')->all;

    return $self->result_source->schema->resultset('FileCaseDocument')->search_rs(
        {
            file_id      => { -in => \@files },
            magic_string => { -in => \@magic_string },
        },
        {
            distinct => 1,
            columns  => [qw(magic_string file_id)],
            prefetch => 'file_id',
        }
    );
}

sub _copy_single_document_to_case {
    my ($self, $cid, $file) = @_;

    my $copy = $file->is_present_in_case($self->id);
    if ($copy) {
        my @ids = $copy->get_case_document_ids();
        $copy->set_or_replace_case_documents(@ids, $cid);

        return 1;
    }

    $copy = $file->copy_to_case(
        case     => $self,
        accepted => 1,
    );
    $copy->set_or_replace_case_documents($cid);
    $copy->apply_case_document_defaults();

    return 1;
}

sub _copy_document_attributes_without_selection {
    my ($self, $case) = @_;

    my $mapping      = $self->_get_document_attribute_mapping;
    my @magic_string = keys %$mapping;
    my $documents    = $self->_get_case_document_files($case, @magic_string);

    my %copy;
    while (my $case_document = $documents->next) {

        my $magic_string = $case_document->magic_string;
        my $cid          = $mapping->{$magic_string};

        $self->_copy_single_document_to_case($cid, $case_document->file_id);

    }
    return 1;
}

sub _copy_attributes_without_selection {
    my ($self, $case) = @_;
    my $kenmerken = $case->field_values;

    my @existing_kenmerken = $self->zaaktype_node_id->zaaktype_kenmerken->search(
        {
            'bibliotheek_kenmerken_id.value_type' => { '!=' =>  'file' },
            'me.bibliotheek_kenmerken_id' => { -in => [ keys %$kenmerken ] }
        },
        {
            prefetch => 'bibliotheek_kenmerken_id'
        }
    )->get_column('bibliotheek_kenmerken_id')->all;

    my %filtered = map {
        ($_ => $kenmerken->{$_})
    } @existing_kenmerken;

    $self->_copy_attributes_selection(\%filtered);
    $self->_copy_document_attributes_without_selection($case);

}

sub _get_attributes {
    my ($case, $magic_strings) = @_;

    my $attrs =  $case->zaaktype_node_id->zaaktype_kenmerken->search(
        {
            'bibliotheek_kenmerken_id.value_type' => { '!=' =>  'file' },
            'bibliotheek_kenmerken_id.magic_string' => { -in => $magic_strings },
        },
        {
            prefetch => 'bibliotheek_kenmerken_id'
        }
    );
    my %mapping;
    while(my $attr = $attrs->next) {
        $mapping{ $attr->bibliotheek_kenmerken_id->magic_string }
            = $attr->get_column('bibliotheek_kenmerken_id');
    }
    return \%mapping;
}

sub _copy_attributes_with_selection {
    my ($self, $case, $selection) = @_;

    my $dest = _get_attributes($self, [ values %$selection ]);
    my $src  = _get_attributes($case, [ keys %$selection ]);

    my $src_values = $case->field_values;
    my %dst_values;

    foreach my $src_magic_string (keys %{$src}) {


        # selection: foo => bar
        # src: magic_string => id
        # dst_values: id => src_values->{id}
        my $dst_magic_string = $selection->{$src_magic_string};
        my $src_id = $src->{$src_magic_string};
        my $dst_id = $dest->{$dst_magic_string};

        $dst_values{$dst_id} = $src_values->{$src_id};

    }

    $self->_copy_attributes_selection(\%dst_values);
    $self->_copy_document_attributes_with_selection($case, $selection);
}

sub _get_document_attributes {
    my ($case, $magic_strings) = @_;

    my $attrs = $case->zaaktype_node_id->zaaktype_kenmerken->search(
        {
            'bibliotheek_kenmerken_id.value_type' => 'file',
            'bibliotheek_kenmerken_id.magic_string' =>
                { -in => $magic_strings },
            $case->pid ? ('me.referential' => 0) : (),
        },
        { prefetch => 'bibliotheek_kenmerken_id' }
    );
    my %mapping;
    while (my $attr = $attrs->next) {
        $mapping{ $attr->bibliotheek_kenmerken_id->magic_string } = $attr->id;
    }
    return \%mapping;
}

sub _copy_document_attributes_with_selection {
    my ($self, $case, $selection) = @_;

    my $dest = _get_document_attributes($self, [values %$selection]);
    my $src  = _get_document_attributes($case, [keys %$selection]);
    my $documents = $self->_get_case_document_files($case, keys %$src);

    my %copy;
    while (my $case_document = $documents->next) {

        my $magic_string = $selection->{$case_document->magic_string};

        next unless defined $magic_string; # referential document in the main case

        my $cid = $dest->{$magic_string};

        next unless $cid; # referential, not defined in case, etc

        $self->_copy_single_document_to_case($cid, $case_document->file_id);
    }
    return 1;
}


sub _copy_attributes_selection {
    my ($self, $values) = @_;

    $self->zaak_kenmerken->update_fields({
        zaak                        => $self,
        new_values                  => $values,
    });
    return;
}

define_profile register_required_subcase => (
    required => {
        subcase_id => CaseNumber,
        required => 'Bool'
    }
);

sub register_required_subcase {
    my ($self, $args) = @_;

    die "need subcase_id" unless $args->{subcase_id};
    die "need required"   unless $args->{required}; # name should be required_fase

    $self->log->info(sprintf(
        'Setting subcase transition dependency on case %d for subcase %d',
        $self->id,
        $args->{ subcase_id }
    ));

    $self->zaak_subcases->create({
        relation_zaak_id        => $args->{subcase_id},
        required                => $args->{required},
    });
}

around can_volgende_fase => sub {
    my $orig    = shift;
    my $self    = shift;

    my $advance_result = $self->$orig(@_);

    my $unfinished = $self->required_subcases_unfinished();

    if (scalar keys %$unfinished) {
        $advance_result->fail('subcases_complete');
    } else {
        $advance_result->ok('subcases_complete');
    }

    return $advance_result;
};

=head2 required_subcases_unfinished

Find out wether this case has any subcases that still have to be finished.
This is only active if for the relationship the 'required' attribute
has been set.

=cut

sub required_subcases_unfinished {
    my $self = shift;

    # list with related subcases
    my $related_cases = {};

    ### Check if this is afhandelfase
    my $afhandelfase = 0;
    $afhandelfase++ if $self->is_volgende_afhandel_fase;

    # required casetypes
    my $required_casetypes = {};

    foreach my $relation (qw/zaak_children/) {
        my $rs = $self->$relation->search({
            'me.status' => { -not_in => ['resolved', 'deleted'] },
        });

        while(my $row = $rs->next()) {
            my $zaaktype_id = $row->get_column('zaaktype_id');

            push @{
                $related_cases->{$zaaktype_id} ||= []
            }, $row->id;

            ### Afhandelfase, EVERY child is required
            if ($afhandelfase && $relation eq 'zaak_children') {
                $required_casetypes->{$zaaktype_id} = 1;
            }
        }
    }

    my $is_afhandel_fase = $self->is_afhandel_fase;

    # get child cases
    my $rs = $self->zaak_children->search({
        'me.status' => { -not_in => ['resolved', 'deleted'] },
    });

    while(my $row = $rs->next()) {
        my $zaaktype_id = $row->get_column('zaaktype_id');

        push @{
            $related_cases->{$zaaktype_id} ||= []
        }, $row->id;

        ### Child case is ALWAYS required when in last phase
        $required_casetypes->{$zaaktype_id} = 1 if $is_afhandel_fase;
    }

    my $zaaktype_required_relaties = $self->zaaktype_id->zaaktype_node_id->zaaktype_relaties->search({
        required => $self->milestone + 1,
    });

    while(my $row = $zaaktype_required_relaties->next()) {
        my $zaaktype_id = $row->get_column('relatie_zaaktype_id');

        if (!$zaaktype_id) {
            throw("zaaktype/relaties/deelzaken", "Zaaktype heeft een deelzaak. Deze relatie is niet gedefinieerd in zaaktypebeheer.");
        }
        if(exists $related_cases->{$zaaktype_id}) {
            $required_casetypes->{$zaaktype_id} = 1;
        }
    }

    # required subcases
    my $required_zaak_subcases_rs = $self->zaak_subcases->search({
        required => $self->milestone + 1
    });

    while(my $row = $required_zaak_subcases_rs->next()) {
        my $zaaktype_id = $row->relation_zaak_id->zaaktype_node_id->get_column('zaaktype_id');

        unless(
            grep(
                { $row->relation_zaak_id->status eq $_ }
                qw/
                resolved
                deleted
                /
            )
        ) {
            $required_casetypes->{$zaaktype_id} = 1;
        }
    }

    return $required_casetypes;
}

sub hierarchy {
    my $self = shift;

    my $iter = $self;

    # After this loop, $iter will be the topmost parent,
    # from which we'll unroll the entire structure.
    while($iter->get_column('pid')) {
        $iter = $iter->pid;
    }

    return $self->unroll_hierarchy_level($iter);
}

sub unroll_hierarchy_level {
    my $self = shift;

    my $node = shift;

    return {
        case => $node,
        children => [
            map { $self->unroll_hierarchy_level($_) } $node->zaak_pids
        ]
    };
}

=head2 master_number

Walk up the chain of "pid" or "vervolg_van" until a case is found without
either of those fields.

Returns the id of the found case.

=cut

sub master_number {
    my $self = shift;

    my $master = $self;

    while ($master->pid || $master->vervolg_van) {
        $master = $master->pid || $master->vervolg_van;
    }

    return $master->id;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 hierarchy

TODO: Fix the POD

=cut

=head2 register_required_subcase

TODO: Fix the POD

=cut

=head2 set_relatie

TODO: Fix the POD

=cut

=head2 unroll_hierarchy_level

TODO: Fix the POD

=cut

