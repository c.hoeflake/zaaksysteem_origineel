export default [
    {
        name: 'admin',
        id: '1',
        uuid: '8f2859ba-9eea-4197-8d59-49ee45bef615'
    }
];

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
