import closeDialog from './../../common/closeDialog';

export const openAdditionalInformation = () => {
    $('[data-name="about"]').click();
};

export const getSummaryValue = valueName => {
    return $(`.case-summary-list [zs-tooltip="${valueName}"] .case-summary-list-item-label`).getText();
};

export const getAboutValue = valueName => {
    const aboutInformationNames = $$('.case-info-group .case-info-group-field');
    let result;
    let fieldValue;

    openAdditionalInformation();
    result = aboutInformationNames
        .filter(elm =>
            elm
                .$('.case-info-group-field-label')
                .getText()
                .then(text =>
                    text === `${valueName}`
                )
        )
    .first();

    fieldValue = result.$('.case-info-group-field-value').getText();
    closeDialog();

    return fieldValue;
};

export const updateConfidentiality = confidentiality => {
    const confidentialityButton = $('zs-case-summary [data-name="confidentiality"] button');
    const confidentialitySetting = $('.modal-confidentiality');
    let option;

    if ( confidentiality === 'vertrouwelijk' ) {
        option = 3;
    } else if ( confidentiality === 'intern' ) {
        option = 2;
    } else if ( confidentiality === 'openbaar' ) {
        option = 1;
    }

    confidentialityButton.click();
    confidentialitySetting
        .$(`li:nth-child(${option}) input`)
        .click();
};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
