export const createChecklistItem = itemText => {
    const checklistItemAdd = $('.checklist-item-add input');
    
    checklistItemAdd.sendKeys(itemText);
    checklistItemAdd.sendKeys(protractor.Key.ENTER);
};

export const deleteChecklistItem = itemNumber => {
    $(`.check-list li:nth-child(${itemNumber}) .sidebar-item-action button`).click();
};

export const toggleChecklistItem = itemNumber => {
    $(`.check-list li:nth-child(${itemNumber}) input`).click();
};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
