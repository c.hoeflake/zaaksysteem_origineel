#! /bin/bash

set -e

if [ $# -lt "1" ]; then
    echo "USAGE: $0 [LIST OF SQL-UPDATE FILES]"
    echo
    echo "Loads update scripts on test template, and dumps is back"
    exit
fi
GIT_ROOT=$(git rev-parse --show-toplevel)
if [ -z "$GIT_ROOT" ]; then
    echo "Please run this script from a Zaaksysteem git repository"
    exit 1
fi

cd "$GIT_ROOT"

if ! docker-compose ps database | grep -q 'Up'; then
    docker-compose up -d database
    sleep 5;
fi

PROCID=$$
DATABASE="zs_test_${USER}_${PROCID}"

dbexists=$(docker-compose exec database psql -U zaaksysteem -t -c "SELECT 1 FROM pg_database WHERE datname = '$DATABASE'")
dbexists=$(echo "$dbexists" | tr -d '[:space:]')
if [ "$dbexists" != "" ]; then
    echo "Temporary database $DATABASE already (still?) exists. Please try again."
    echo $dbexists | od -x
    exit
fi

BASE_DIR="."
# Running in zs-start
if [ ! -d "$BASE_DIR/db" ]; then
    BASE_DIR=zsnl-perl-api
fi

UPGRADE_FILE=$(mktemp $BASE_DIR/db/db_upgrade.XXXXXXXX)
TEMPLATE="$BASE_DIR/db/test-template.sql"
EMPTYTEMPLATE="$BASE_DIR/db/template.sql"

for args in $*
do
    cat $args >> $UPGRADE_FILE;
    git add $args
done

echo "Creating test database: $DATABASE"
docker-compose exec database psql -U zaaksysteem -c "CREATE DATABASE $DATABASE ENCODING 'UTF-8';"

echo "Loading '${TEMPLATE#zsnl-perl-api/}' into '$DATABASE'"
docker-compose exec database psql -U zaaksysteem -f "/opt/zaaksysteem/${TEMPLATE#zsnl-perl-api/}" "$DATABASE"

echo "Executing upgrades to $DATABASE"
docker-compose exec database psql -U zaaksysteem -f "/opt/zaaksysteem/${UPGRADE_FILE#zsnl-perl-api/}" "$DATABASE"

echo "Recreating DBIx::Class schema"
perlbackend=backend
if [ "$(docker-compose ps|awk '{ print $1; }'|grep 'perl-api')" ]; then
    perlbackend=perl-api
fi
docker-compose run --no-deps --rm $perlbackend /opt/zaaksysteem/dev-bin/db_redeploy.sh "dbi:Pg:dbname=${DATABASE};host=database" zaaksysteem zaaksysteem123

echo "Dumping database '$DATABASE' to '${TEMPLATE#zsnl-perl-api/}'"
docker-compose exec -T database pg_dump -U zaaksysteem --no-owner "$DATABASE" > "$TEMPLATE"

echo "Dumping database '$DATABASE' schema to '${EMPTYTEMPLATE#zsnl-perl-api/}'"
docker-compose exec -T database pg_dump -U zaaksysteem --no-owner --schema-only "$DATABASE" > "$EMPTYTEMPLATE"

echo "Cleaning up database: '$DATABASE'"
docker-compose exec database psql -U zaaksysteem -c "DROP DATABASE $DATABASE"

rm "${UPGRADE_FILE}"

if [ $BASE_DIR != '.' ]
then
    cd $BASE_DIR
fi

find . -user 0 -exec sudo chown $USER:$USER {} \;
for i in $(git status --porcelain | egrep '^\?\? lib/Zaaksysteem/Schema/' | awk '{print $NF}')
do

    cat <<OEF >> $i

__END__

$(cat drafts/license_block.pod)
OEF

    git add $i
done

git add db/template.sql db/test-template.sql

echo "Added your files to git - you can commit now if you want"
