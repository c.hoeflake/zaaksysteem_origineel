BEGIN;
    INSERT INTO config (parameter, value, definition_id) VALUES
        ('case_number_prefix', '', 'dbb124d7-9e96-4a8a-9252-fb367ef8562a');

    ALTER TABLE zaak ADD COLUMN prefix text not null default '';
    ALTER TABLE zaak ALTER COLUMN uuid DROP DEFAULT;
COMMIT;
