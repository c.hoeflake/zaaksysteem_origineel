BEGIN;

    ALTER TABLE logging ADD COLUMN loglevel CHARACTER VARYING(32);

    -- BEWARE: this operation will take a long ass time to complete.
    UPDATE logging SET loglevel = '2';

COMMIT;
