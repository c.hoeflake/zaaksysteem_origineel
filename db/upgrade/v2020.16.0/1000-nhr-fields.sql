BEGIN;

  ALTER TABLE bedrijf ADD COLUMN rsin bigint;
  ALTER TABLE bedrijf ADD COLUMN oin bigint;

  ALTER TABLE bedrijf ADD COLUMN main_activity jsonb default '{}' NOT NULL;
  ALTER TABLE bedrijf ADD COLUMN secondairy_activities jsonb default '[]' NOT NULL;

  ALTER TABLE bedrijf ADD COLUMN date_founded date;
  ALTER TABLE bedrijf ADD COLUMN date_registration date;
  ALTER TABLE bedrijf ADD COLUMN date_ceased date;

  ALTER TABLE bedrijf ADD COLUMN vestiging_latlong point;
  ALTER TABLE bedrijf ADD COLUMN vestiging_bag_id bigint;

  -- snapshots require the same data:

  ALTER TABLE gm_bedrijf ADD COLUMN rsin bigint;
  ALTER TABLE gm_bedrijf ADD COLUMN oin bigint;

  ALTER TABLE gm_bedrijf ADD COLUMN main_activity jsonb default '{}' NOT NULL;
  ALTER TABLE gm_bedrijf ADD COLUMN secondairy_activities jsonb default '[]' NOT NULL;

  ALTER TABLE gm_bedrijf ADD COLUMN date_founded date;
  ALTER TABLE gm_bedrijf ADD COLUMN date_registration date;
  ALTER TABLE gm_bedrijf ADD COLUMN date_ceased date;

  ALTER TABLE gm_bedrijf ADD COLUMN vestiging_latlong point;
  ALTER TABLE gm_bedrijf ADD COLUMN vestiging_bag_id bigint;

COMMIT;
