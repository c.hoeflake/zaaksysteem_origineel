
BEGIN;
  CREATE INDEX queue_data_gin_idx ON queue USING gin(cast(data as jsonb));
  CREATE INDEX queue_metadata_gin_idx ON queue USING gin(cast(metadata as jsonb));
  CREATE INDEX queue_status_pending_idx ON queue USING btree(status) WHERE status = 'pending';
  CREATE INDEX queue_status_postponed_idx ON queue USING btree(status) WHERE status = 'postponed';
COMMIT;

