BEGIN;

  ALTER TABLE zaaktype ADD COLUMN uuid UUID UNIQUE DEFAULT uuid_generate_v4();
COMMIT;

BEGIN;

  WITH cte AS (
    SELECT uuid, object_id FROM object_data where object_class = 'casetype'
  )
  UPDATE zaaktype
    SET
      uuid = cte.uuid
    FROM
      cte
    WHERE
      zaaktype.id = cte.object_id
  ;

COMMIT;
